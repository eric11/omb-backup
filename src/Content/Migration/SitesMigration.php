<?php

namespace OhMyBingo\Content\Migration;

use Contentful\Delivery\Query as BaseQuery;
use Contentful\Delivery\Resource\Entry;
use Contentful\Management\Resource\Asset;
use Contentful\Delivery\Resource\Asset as DeliveryAsset;
use Contentful\Core\Resource\ResourceArray;
use Josantonius\File\File;
use DateTime;
use OhMyBingo\Content\Migration\Parser\SitesParser;
use OhMyBingo\Content\ContentManagementService;
use OhMyBingo\Content\ContentService;
use Psr\Log\LoggerInterface as Logger;

class SitesMigration extends Abstract_Migration
{
    const CONTENT_TYPE = 'site';
    const CREATE_SITE_ENTRIES = true;
    // const CREATE_LOGOS = true;

    const DEFAULT_SITEMAP_CHANGE_FREQ = 'monthly';
    const DEFAULT_SITEMAP_PRIORITY = '0.8';

    protected $sitesParser;

    public function __construct(ContentManagementService $contentManagementService, ContentService $contentService, SitesParser $sitesParser, Logger $logger)
    {
        parent::__construct($contentManagementService, $contentService, $logger);
        $this->sitesParser = $sitesParser;
    }

    function run()
    {
        $sql = "SELECT * FROM sites WHERE id >= 0 ORDER BY ID ASC LIMIT 0, 100000";
//        $sql = "SELECT * FROM sites WHERE type = 'poker' ORDER BY ID ASC LIMIT 0, 100000";
//        $sql = "SELECT * FROM sites WHERE NAME = 'Casimba' ORDER BY ID ASC LIMIT 0, 100000";

//        $sql = "SELECT * FROM sites ORDER BY ID ASC LIMIT 0, 10000";
        $stmt = $this->legacyDatabase->query($sql);
        $i = 1;
        while ($site = $stmt->fetch()) {
            if ($i < 10000) {
                $siteSlug = strtolower(str_replace(' ', '-', $site['name']));
                $siteSlug = str_replace("'", '', $siteSlug);
                $this->logger->info('Importing site:', ['id' => $site['id'], 'name' => $site['name'], 'slug' => $siteSlug]);

                # Parse site data
                $newSite = [];
                $newSite['logo']               = '';
                $newSite['label']              = '';
                $newSite['affiliateLink']      = $site['url']; // $site['url'], $site['sponsor_url'], $site['newburl'],
//                if ($site['type'] != 'bingo' && $site['type'] !=  'casino' && $site['type'] != 'slots') {
//                    continue;
//                }
                // Classification
                $newSite['classification'] = ucwords($site['type']);
                if ($site['type'] === 'scratch' ) {
                    $newSite['classification'] = 'Scratchcards';
                }

                # Software
                $newSite['software'] = null;
                $softwares = ['Dragonfish', 'Jumpman', 'Virtue Fusion', 'Cozy Games', '15 Network'];
                foreach ($softwares as $software) {
                    if (strpos($site['network'], $software) !== false) {
                        $newSite['software'] = $software;
                    }
                }

                # Network
                $newSite['network'] = null;
                if ($newSite['software'] === 'Dragonfish') {
                    $newSite['network'] = '888';
                }
                if ($site['network'] === 'Wheel Of Slots Sites') {
                    $newSite['network'] = 'Wheel Of Slots';
                }

                // Slots classification
//                if (strpos($site['name'], 'Slot') !== false || $newSite['network'] = 'Wheel Of Slots') {
//                    $newSite['classification'] = 'Slots';
//                }
                if (strpos($site['name'], 'Slot') !== false) {
                    $newSite['classification'] = 'Slots';
                }

                // Status
                $newSite['status'] = null;
                if (strpos($site['first_deposit_other'], 'This site is now closed') !== false) {
                    $newSite['status'] = 'Closed';
                } else if (strpos($site['first_deposit_other'], 'Coming Soon!') !== false) {
                    $newSite['status'] = 'Coming Soon';
                }
                if ($site['new_site'] == 1) {
                    $newSite['status'] = 'New';
                }

                // Recommended
                if ($site['recommended_sites'] == 1) {
                    $newSite['recommended'] = false;
                } else {
                    $newSite['recommended'] = true;
                }

                // Featured
                if ($site['gold'] > 0) {
                    $newSite['featured'] = true;
                } else {
                    $newSite['featured'] = false;
                }


                // Site Features

//                if (!$free_money && !$first_deposit_bonus){
//                    $bonus=$first_deposit_other;
//                }else if (!$free_money && $first_deposit_bonus){
//                    $bonus=$first_deposit_bonus."% 1st DEPOSIT BONUS!";
//                }else{
//                    $bonus="&pound;".$free_money." FREE WHEN YOU JOIN!";
//                }
//                if($t_and_c_content=="") $t_and_c_content="18+. T&C Apply. New customers only. Wagering requirements and full T's&C's apply";
//
//                if (!$site['free_money'] && !$site['first_deposit_bonus']){
//
//                }

                # Terms And Conditions
                if (!empty($site['t_and_c'])) {
                    $newSite['termsAndConditions'] = $site['t_and_c'];
                } else {
                    $newSite['termsAndConditions'] = '18+. T&C Apply. New customers only. Wagering requirements and full T\'s&C\'s apply';
                }
                $newSite['termsAndConditionsUrl'] = (!empty($site['termsUrl'])) ? $site['termsUrl'] : $site['url'];

                # Site Features
                $newSite['siteFeatures'] = null;

                # Free Bingo
                if ($site['free_bingo'] > 1){
                    $newSite['siteFeatures'] = 'Free Bingo';
                    $newSite['freeBingo'] = true;
                }  else {
                    $newSite['freeBingo'] = false;
                }

                # No Deposit
                if ($site['free_bingo'] > 1){
                    $newSite['siteFeatures'] = 'No Deposit Bingo';
                    $newSite['noDeposit'] = true;
                }  else {
                    $newSite['noDeposit'] = false;
                }

                # Available Games
                $newSite['availableGames'] = null;
                if ($site['spin_wheel']){
                    $newSite['availableGames'] = 'Spin The Wheel';
                }


//                $siteStatus = $this->fetchTargetStatus($statusSlug);
//                $newSite['status'] = (!empty($siteStatus) ? $siteStatus->asLink() : null);

                $newSite['affiliationStatus'] = true;
                if (empty($site['first_deposit_other']) && empty($site['first_deposit_text'])) {
                    $newSite['affiliationStatus'] = false;
                }

                $hasReviewSections = $this->sitesParser->hasReviewSections($site['content']);
                $newSite['reviewTitle']        = ''; // not currently in use - not required
                if ($hasReviewSections) {
                    $newSite['reviewIntroduction'] = trim(strip_tags($this->sitesParser->parseReviewSection($site['content'], 'Introduction to '.$site['name'], 'Welcome Offer')));
                    $newSite['reviewWelcomeOffer'] = trim(strip_tags($this->sitesParser->parseReviewSection($site['content'], 'Welcome Offer', 'Promotions')));
                    $newSite['reviewPromotions']   = trim(strip_tags($this->sitesParser->parseReviewSection($site['content'], 'Promotions', 'Games')));
                    $newSite['reviewGames']        = trim(strip_tags($this->sitesParser->parseReviewSection($site['content'], 'Games', 'Our Final Thoughts')));
                    $newSite['reviewFinalThoughts'] = trim(strip_tags($this->sitesParser->parseReviewSection($site['content'], 'Our Final Thoughts', null)));
                } else {
                    $newSite['reviewIntroduction'] = $site['content'];//trim(strip_tags($site['content'])); // Put everything in introduction
                    $newSite['reviewWelcomeOffer'] = '';
                    $newSite['reviewPromotions']   = '';
                    $newSite['reviewGames']        = '';
                    $newSite['reviewFinalThoughts'] = '';
                }

                $datetime = new DateTime($site['review_date']);
                $newSite['reviewDate']         = $datetime->format(DateTime::ATOM); // ISO 8601
                $newSite['navigationText']     = '';
                $newSite['sitemapChangeFreq']  = self::DEFAULT_SITEMAP_CHANGE_FREQ;
                $newSite['sitemapPriority']    = self::DEFAULT_SITEMAP_PRIORITY;
                $newSite['slug']               = $siteSlug;
                // meta keywords
                $metaKeywords = explode(', ', $site['meta_keywords']);
                $newMetaKeywords = [];
                foreach ($metaKeywords as $metaKeyword) {
                    array_push($newMetaKeywords, htmlentities($metaKeyword));
                }
                $newSite['metaKeywords'] = $newMetaKeywords;

                # Fetch Target Entry if Exists
                $currentSiteEntry = $this->fetchTargetSite($siteSlug);

                # Fetch Current Site Logo
                $mediaLogo = $this->fetchLogoFromMedia($siteSlug);

                if (!$mediaLogo) {
                    $shouldCreateLogo = true;
                    $this->logger->info('Site logo does not exist in target media..');
                } else {
                    $shouldCreateLogo = false;
                    $this->logger->info('Site logo already exists in target media..');
                }

                if ($shouldCreateLogo) {
                    $logoTitle = $site['name'];
                    $logoDescription = $site['name'] . ' logo';
                    $currentSiteLogoUrl = $this->fetchImage($site['image']);
                    $logoFilename = $siteSlug.'.png';
                    $logoMimeType = 'image/png';
                    if ($currentSiteLogoUrl) {
                        # If no logo on current site then add it
                        if (!$currentSiteEntry) {
                            $createdLogoAsset = $this->createLogo($logoFilename, $logoMimeType, $logoTitle, $logoDescription, $currentSiteLogoUrl);
                            $newSite['logo'] = $createdLogoAsset->asLink();
                        } else {
                            if (!$currentSiteEntry->getLogo()) {
                                $createdLogoAsset = $this->createLogo($logoFilename, $logoMimeType, $logoTitle, $logoDescription, $currentSiteLogoUrl);
                                $newSite['logo'] = $createdLogoAsset->asLink();
                            } else {
                                $currentLogoLink = $currentSiteEntry->getLogo(); // Don't replace sites with valid logos
                                $newSite['logo'] = $currentLogoLink;
                                $this->logger->warning('The target site entry already has a logo image.');
                            }
                        }
                    } else {
                        $this->logger->error('The site to import does not have an logo image!');
                    }
                } else {
                    $newSite['logo'] = $mediaLogo->asLink();
                }

                $keyDetails = [];

                $hasKeyDetails = $this->sitesParser->hasKeyDetails($site['content']);

                if ($hasKeyDetails) {
                    $keyDetailsContent = explode('<h2>Key Details</h2>', $site['content'])[1];
                    $keyDetails['keyDetailsNoDepositOffer']    = trim(strip_tags($this->sitesParser->parseKeyDetail($keyDetailsContent, 'No Deposit Offer')));
                    $keyDetails['keyDetailsWelcomeBonus']      = trim(strip_tags($this->sitesParser->parseKeyDetail($keyDetailsContent, 'Welcome Bonus')));
                    $keyDetails['keyDetailsRedepositBonus']    = trim(strip_tags($this->sitesParser->parseKeyDetail($keyDetailsContent, 'Redeposit Bonus')));
                    $keyDetails['keyDetailsMinimumDeposit']    = trim(strip_tags($this->sitesParser->parseKeyDetail($keyDetailsContent, 'Minimum Deposit')));
                    $keyDetails['keyDetailsMinimumWithdrawal'] = trim(strip_tags($this->sitesParser->parseKeyDetail($keyDetailsContent, 'Minimum Withdrawal')));
                    $keyDetails['keyDetailsFreeBingo']         = trim(strip_tags($this->sitesParser->parseKeyDetail($keyDetailsContent, 'Free Bingo')));
                    $keyDetails['keyDetailsSoftware']          = trim(strip_tags($this->sitesParser->parseKeyDetail($keyDetailsContent, 'Bingo Software')));
                    $keyDetails['keyDetailsEstablished']       = trim(strip_tags($this->sitesParser->parseKeyDetail($keyDetailsContent, 'Established')));
                    $rawDepositMethods = $this->sitesParser->parseKeyDetail($keyDetailsContent, 'Deposit Methods');
                    if ($rawDepositMethods) {
                        $rawDepositMethodsArray = explode(', ', $rawDepositMethods);
                        $depositMethods = [];
                        foreach ($rawDepositMethodsArray as $depositMethod) {
                            array_push($depositMethods, htmlentities($depositMethod));
                        }
                    } else {
                        $depositMethods = [];
                    }
                    $keyDetails['keyDetailsDepositMethods']    = $depositMethods;
                    $keyDetails['keyDetailsEmailAddress']      = $this->sitesParser->parseKeyDetail($keyDetailsContent, 'Email Address');
                    $keyDetails['keyDetailsLiveChat']          = '';
                    $keyDetails['keyDetailsPhoneNumber']       = $this->sitesParser->parseKeyDetail($keyDetailsContent, 'Phone Number');
                    $ukGamblingNo = $this->sitesParser->parseKeyDetail($keyDetailsContent, 'UK Gambling Commission');
                    if ($ukGamblingNo) {
                        $ukGamblingNo = str_replace('Licence ', '', $ukGamblingNo);
                        $ukGamblingNo = trim(strip_tags($ukGamblingNo));
                        $this->logger->info('Parsed UK Gambling commission license', ['ukGamblingCommissionNo' => $ukGamblingNo]);
                    } else {
                        $ukGamblingNo = null;
                    }
                    $keyDetails['keyDetailsLicenseNumber']     = (int) $ukGamblingNo;
                } else {
                    $keyDetails['keyDetailsNoDepositOffer']    = '';
                    $keyDetails['keyDetailsWelcomeBonus']      = '';
                    $keyDetails['keyDetailsRedepositBonus']    = '';
                    $keyDetails['keyDetailsMinimumDeposit']    = '';
                    $keyDetails['keyDetailsMinimumWithdrawal'] = '';
                    $keyDetails['keyDetailsFreeBingo']         = '';
                    $keyDetails['keyDetailsSoftware']          = '';
                    $keyDetails['keyDetailsEstablished']       = '';
                    $keyDetails['keyDetailsDepositMethods']    = [];
                    $keyDetails['keyDetailsEmailAddress']      = '';
                    $keyDetails['keyDetailsLiveChat']          = '';
                    $keyDetails['keyDetailsPhoneNumber']       = '';
                    $keyDetails['keyDetailsLicenseNumber']     = null;
                }

                # Build site data
                $welcomeOfferLine1 = SitesParser::htmlEntities($site['first_deposit_other']);
                $welcomeOfferLine2 = SitesParser::htmlEntities($site['first_deposit_text']);

                $siteData = [
                    'name' => $site['name'],
                    'slug' => $siteSlug,
                    'classification' => $newSite['classification'],
                    'status'         => $newSite['status'],
                    'recommended'       => $newSite['recommended'],
                    'featured'       => $newSite['featured'],
                    'affiliationStatus' => $newSite['affiliationStatus'],
                    'affiliateLink'         => $newSite['affiliateLink'],
                    'logo'                  => $newSite['logo'],
                    'network'               => $newSite['network'],
                    'software'               => $newSite['software'],
                    'freeBingo'               => $newSite['freeBingo'],
                    'noDeposit'               => (bool) $newSite['noDeposit'],
                    'siteFeatures'               => $newSite['siteFeatures'],
                    'availableGames'               => $newSite['availableGames'],
                    'rating'                => (int) $site['site_rating'],
                    'welcomeOfferLine1'     => $welcomeOfferLine1,
                    'welcomeOfferLine2'     => $welcomeOfferLine2,
                    'reviewTitle'           => SitesParser::htmlEntities($newSite['reviewTitle']),
                    'reviewIntroduction'    => SitesParser::htmlEntities($newSite['reviewIntroduction']),
                    'reviewWelcomeOffer'    => SitesParser::htmlEntities($newSite['reviewWelcomeOffer']),
                    'reviewPromotions'      => SitesParser::htmlEntities($newSite['reviewPromotions']),
                    'reviewGames'           => SitesParser::htmlEntities($newSite['reviewGames']),
                    'reviewFinalThoughts'   => SitesParser::htmlEntities($newSite['reviewFinalThoughts']),
                    'reviewDate'            => $newSite['reviewDate'],
                    'keyDetailsNoDepositOffer'    =>  SitesParser::htmlEntities($keyDetails['keyDetailsNoDepositOffer']),
                    'keyDetailsWelcomeBonus'      =>  SitesParser::htmlEntities($keyDetails['keyDetailsWelcomeBonus']),
                    'keyDetailsRedepositBonus'    =>  SitesParser::htmlEntities($keyDetails['keyDetailsRedepositBonus']),
                    'keyDetailsMinimumDeposit'    =>  SitesParser::htmlEntities($keyDetails['keyDetailsMinimumDeposit']),
                    'keyDetailsMinimumWithdrawal' =>  SitesParser::htmlEntities($keyDetails['keyDetailsMinimumWithdrawal']),
                    'keyDetailsFreeBingo'         =>  SitesParser::htmlEntities($keyDetails['keyDetailsNoDepositOffer']),
                    'keyDetailsSoftware'          =>  SitesParser::htmlEntities($keyDetails['keyDetailsSoftware']),
                    'keyDetailsEstablished'       =>  SitesParser::htmlEntities($keyDetails['keyDetailsEstablished']),
                    'keyDetailsDepositMethods'    =>  $keyDetails['keyDetailsDepositMethods'],
                    'keyDetailsEmailAddress'      =>  SitesParser::htmlEntities($keyDetails['keyDetailsEmailAddress']),
                    'keyDetailsLiveChat'          =>  SitesParser::htmlEntities($keyDetails['keyDetailsLiveChat']),
                    'keyDetailsPhoneNumber'       =>  SitesParser::htmlEntities($keyDetails['keyDetailsPhoneNumber']),
                    'keyDetailsLicenseNumber'     =>  $keyDetails['keyDetailsLicenseNumber'],
                    'metaTitle'             => SitesParser::htmlEntities($site['meta_title']),
                    'metaDescription'       => SitesParser::htmlEntities($site['meta_description']),
                    'metaKeywords'          => $newSite['metaKeywords'],
                    'label'                 => null, // $newSite['label'],
                    'termsAndConditions'    => SitesParser::htmlEntities($newSite['termsAndConditions']),
                    'termsAndConditionsUrl' => SitesParser::htmlEntities($newSite['termsAndConditionsUrl']), // $site['terms_url'],
                    'navigationText'        => $newSite['navigationText'],
                    'sitemapChangeFreq'     => $newSite['sitemapChangeFreq'],
                    'sitemapPriority'       => (float) $newSite['sitemapPriority'],
                ];
                $this->logger->info('Constructed site data to import:', $siteData);

                if (self::CREATE_SITE_ENTRIES) {
                    if (!$currentSiteEntry) {
                        $this->logger->info('Creeating site entry...');
                        $siteEntry = $this->contentManagementService->createEntry(self::CONTENT_TYPE, $siteData, 'en-US');
                        $this->logger->info('Successfully created site entry!');
                    } else {
                        $this->logger->info('Updating site entry...');
                        $siteEntry = $this->contentManagementService->updateEntry(self::CONTENT_TYPE, $currentSiteEntry->getSystemProperties()->getId(), $siteData, 'en-US');
                        $this->logger->info('Successfully updated site entry. Not implemented');
                    }
                }

            }
            $i++;
        }

//        $sites = [];
//        foreach ($sites as $site) {
//            echo $site;
//        }
    }

    function fetchTargetSite(string $siteSlug): ?Entry
    {
        $contentQuery = new BaseQuery();
        $contentQuery->setContentType('site')->where('fields.slug.en-US', $siteSlug);
        $currentSiteEntry = $this->contentService->getEntry($contentQuery);
        if ($currentSiteEntry) {
            $this->logger->warning('Site to import already exists:', ['currentSiteEntry' => $currentSiteEntry->getSystemProperties()->getId()]);
        } else {
            $this->logger->info('Site to import does not exist:', ['slug' => $siteSlug]);
        }
        return $currentSiteEntry;
    }

    function fetchLogoFromMedia(string $siteSlug): ?DeliveryAsset
    {
        $assetFilename = $siteSlug.'.png';
        $contentQuery = new BaseQuery();
        $contentQuery->where('fields.file.fileName', $assetFilename);
        /** @var ResourceArray $mediaAssets */
        $mediaAssets = $this->contentService->getAssets($contentQuery);
        $this->logger->info('Fetched current site logo:', ['slug' => $siteSlug, 'count' => $mediaAssets->getTotal()]);


        if ($mediaAssets->getTotal() > 0) {
            return $mediaAssets->getItems()[0];
        } else {
            return null;
        }
    }


    function createLogo(string $filename, string $mimeType, string $logoTitle, string $logoDescription, string $currentSiteLogoUrl): ?Asset
    {
        $this->logger->info('Creating site logo:', ['currentSiteUrl' => $currentSiteLogoUrl]);
        $newSiteEntryLogoAsset = $this->contentManagementService->createAssetFromRemoteFile($filename, $mimeType, $logoTitle, $logoDescription, $currentSiteLogoUrl, 'en-US');
        $this->logger->info('Created new site logo for import target site entry.');
        return $newSiteEntryLogoAsset;
    }

    function fetchImage(string $siteImage): ?string
    {
        $imageUrl = "https://www.ohmybingo.com/images/sites/{$siteImage}";

        $fileExists = File::exists($imageUrl);
        if ($fileExists) {
            return $imageUrl;
        } else {
            return null;
        }

        // Checking if file exists

        /*
        $ch = curl_init($imageUrl);
        curl_setopt($ch, CURLOPT_NOBODY, true);
//        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($code == 200) {
//            $this->downloader->download($imageUrl, $imagesRoot, $fileName);
//            $savedImagePath = "$imagesRoot/$fileName";
//            return $savedImagePath;
            return $imageUrl;
        } else {
//            throw new \Exception('File for logo not found:'. $fileName);
            return null;
        }
        */
        
    }
    /*

    function fetchTargetNetwork(string $networkSlug): ?Entry
    {
        $contentQuery = new BaseQuery();
        $contentQuery->setContentType('network')->where('fields.slug.en-US', $networkSlug);
        $networkEntry = $this->contentService->getEntry($contentQuery);
        if ($networkEntry) {
            $this->logger->info('Network entry fetched:', ['networkEntry' => $networkEntry->getSystemProperties()->getId(), 'slug' => $networkSlug]);
        } else {
            $this->logger->error('Network entry not fetched:', ['slug' => $networkSlug]);
        }
        return $networkEntry;
    }

    function fetchTargetSoftware(string $softwareSlug): ?Entry
    {
        $contentQuery = new BaseQuery();
        $contentQuery->setContentType('software')->where('fields.slug.en-US', $softwareSlug);
        $softwareEntry = $this->contentService->getEntry($contentQuery);
        if ($softwareEntry) {
            $this->logger->info('Software entry fetched:', ['softwareEntry' => $softwareEntry->getSystemProperties()->getId(), 'slug' => $softwareSlug]);
        } else {
            $this->logger->error('Software entry not fetched:', ['slug' => $softwareSlug]);
        }
        return $softwareEntry;
    }

    function fetchTargetClassification(string $classificationSlug): ?Entry
    {
        $contentQuery = new BaseQuery();
        $contentQuery->setContentType('classification')->where('fields.slug.en-US', $classificationSlug);
        $classificationEntry = $this->contentService->getEntry($contentQuery);
        if ($classificationEntry) {
            $this->logger->info('Site classification entry fetched:', ['classificationEntry' => $classificationEntry->getSystemProperties()->getId(), 'slug' => $classificationSlug]);
        } else {
            $this->logger->error('Site classification entry not fetched:', ['slug' => $classificationSlug]);
        }
        return $classificationEntry;
    }
    */

    /*
    function fetchTargetStatus(?string $statusSlug): ?Entry
    {
        if (!$statusSlug) {
            return null;
        }
        $contentQuery = new BaseQuery();
        $contentQuery->setContentType('status')->where('fields.slug.en-US', $statusSlug);
        $statusEntry = $this->contentService->getEntry($contentQuery);
        if ($statusEntry) {
            $this->logger->info('Site status fetched:', ['statusEntry' => $statusEntry->getSystemProperties()->getId(), 'slug' => $statusSlug]);
        } else {
            $this->logger->error('Site status not fetched:', ['slug' => $statusSlug]);
        }
        return $statusEntry;
    }*/


}
