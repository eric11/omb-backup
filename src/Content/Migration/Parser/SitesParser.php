<?php

namespace OhMyBingo\Content\Migration\Parser;

use Psr\Log\LoggerInterface as Logger;

class SitesParser
{
    protected $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    function parseReviewTitle(string $reviewContent): string
    {
        return '';
    }

    /*
     * Review Sections
     *
     * @param string $content
     */
    function hasReviewSections(string $content): string
    {
        if (strpos($content, 'Introduction to ') !== false) {
            $this->logger->info('Review has sections - will parse review content');
            return true;
        } else {
            $this->logger->warning('Review has no sections - unable to parse review content - will set introduction as full review content');
            return false;
        }
    }

    function parseReviewSection(string $content, string $startString, ?string $endString): string
    {
        $reviewContent = trim($content);
        $reviewContent = str_replace("\r\n",'', $reviewContent);

        if ($endString) {        // Try H2
            $startNeedle = '<h2>'.$startString.'</h2>';
            $endNeedle = '<h2>'.$endString.'</h2>';
            if (strpos($reviewContent, $startNeedle) !== false && strpos($reviewContent, $startNeedle) !== false) {
                $sectionContent = $this->getStringBetween($reviewContent, $startNeedle, $endNeedle);
                $sectionContent = $this->removeEmptyPTags($sectionContent);
//            $contentExploded = explode("<h2>", $reviewContent);
//            $introExplode = explode("</h2>", $contentExploded[1]);
//            $introOnlyExplode = explode("<p>", $introExplode[1]);
//            $introPOnlyExplode = explode("</p>", $introOnlyExplode[0]);
//            $contentSection = trim(strip_tags($introOnlyExplode[1]));
                $this->logger->info('Found review section:', ['startNeedle', $startNeedle, 'sectionContent' => $sectionContent]);
                return $sectionContent;
            } else {
                $sectionContent = '';
                $this->logger->info('Did not find review section:', ['startNeedle', $startNeedle]);
            }

            // Try P
            $startNeedle = '<p>'.$startString.'</p>';
            $endNeedle = '<p>'.$endString.'</p>';
            if (strpos($reviewContent, $startNeedle) !== false && strpos($reviewContent, $startNeedle) !== false) {
                $sectionContent = $this->getStringBetween($reviewContent, $startNeedle, $endNeedle);
                $sectionContent = $this->removeEmptyPTags($sectionContent);
                $this->logger->info('Found review section:', ['startNeedle', $startNeedle, 'sectionContent' => $sectionContent]);
                return $sectionContent;
            } else {
                $sectionContent = '';
                $this->logger->info('Did not find review section:', ['startNeedle', $startNeedle]);
            }
        }

        // Try last section (h2) - Key Details
        $startNeedle = '<h2>'.$startString.'</h2>';
        $endNeedle = '<h2>Key Details</h2>';
        if (strpos($reviewContent, $startNeedle) !== false && strpos($reviewContent, $startNeedle) !== false) {
            $sectionContent = $this->getStringBetween($reviewContent, $startNeedle, $endNeedle);
            $sectionContent = $this->removeEmptyPTags($sectionContent);
            $this->logger->info('Found review section:', ['startNeedle', $startNeedle, 'sectionContent' => $sectionContent]);
            return $sectionContent;
        } else {
            $sectionContent = '';
            $this->logger->info('Did not find review section:', ['startNeedle', $startNeedle]);
        }

        // Try last section (p) - Key Details
        $startNeedle = '<p>'.$startString.'</p>';
        $endNeedle = '<h2>Key Details</h2>';
        if (strpos($reviewContent, $startNeedle) !== false && strpos($reviewContent, $startNeedle) !== false) {
            $sectionContent = $this->getStringBetween($reviewContent, $startNeedle, $endNeedle);
            $sectionContent = $this->removeEmptyPTags($sectionContent);
            $this->logger->info('Found review section:', ['startNeedle', $startNeedle, 'sectionContent' => $sectionContent]);
            return $sectionContent;
        } else {
            $sectionContent = '';
            $this->logger->info('Did not find review section:', ['startNeedle', $startNeedle]);
        }

        // Try last section (h2) - div
        $startNeedle = '<h2>'.$startString.'</h2>';
        $endNeedle = '</div>';
        if (strpos($reviewContent, $startNeedle) !== false && strpos($reviewContent, $startNeedle) !== false) {
            $sectionContent = $this->getStringBetween($reviewContent, $startNeedle, $endNeedle);
            $sectionContent = $this->removeEmptyPTags($sectionContent);
            $this->logger->info('Found review section:', ['startNeedle', $startNeedle, 'sectionContent' => $sectionContent]);
            return $sectionContent;
        } else {
            $sectionContent = '';
            $this->logger->info('Did not find review section:', ['startNeedle', $startNeedle]);
        }

        // Try last section (p) - div
        $startNeedle = '<p>'.$startString.'</p>';
        $endNeedle = '</div>';
        if (strpos($reviewContent, $startNeedle) !== false && strpos($reviewContent, $startNeedle) !== false) {
            $sectionContent = $this->getStringBetween($reviewContent, $startNeedle, $endNeedle);
            $sectionContent = $this->removeEmptyPTags($sectionContent);
            $this->logger->info('Found review section:', ['startNeedle', $startNeedle, 'sectionContent' => $sectionContent]);
            return $sectionContent;
        } else {
            $sectionContent = '';
            $this->logger->info('Did not find review section:', ['startNeedle', $startNeedle]);
        }


        return $sectionContent;
    }

    private function getStringBetween($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    private function removeEmptyPTags(string $str) {
        return preg_replace("/<p[^>]*>[\s|&nbsp;]*<\/p>/", '', $str);
    }

    /*
     * Key Details
     *
     * @param string $content
     */
    function hasKeyDetails(string $content): string
    {
        if (strpos($content, '<h2>Key Details</h2>') !== false) {
            $this->logger->info('Review has key details - will parse key details');
            return true;
        } else {
            $this->logger->warning('Review has no key details - unable to parse key details');
            return false;
        }
    }

    function parseKeyDetail(string $content, string $startString): string
    {
        $reviewContent = trim($content);
        $reviewContent = str_replace("\r\n",'', $reviewContent);
        $startNeedle = '<h2>Key Details</h2>';
        $endNeedle = '<h2>Key Details</h2>';
        $keyDetails = $this->getStringBetween($reviewContent, $startNeedle, $endNeedle);

        $startNeedle = '<strong>'.$startString.'</strong>';
        $endNeedle = '</tr>';
        if (strpos($reviewContent, $startNeedle) !== false && strpos($reviewContent, $startNeedle) !== false) {
            $keyDetail = $this->getStringBetween($reviewContent, $startString, $endNeedle);//trim(strip_tags($this->getStringBetween($reviewContent, $startString, $endString)));
            $keyDetail = trim(strip_tags($keyDetail));
            $this->logger->info('Found key details:', ['startNeedle', $startNeedle, 'keyDetail' => $keyDetail]);
            return $keyDetail;
        } else {
            $keyDetail = '';
            $this->logger->info('Did not find key detail:', ['startNeedle', $startNeedle]);
        }

        return $keyDetails;
    }


    public static function htmlEntities(string $str): string
    {
        return htmlentities(mb_convert_encoding($str, 'UTF-8', 'ASCII'), ENT_SUBSTITUTE, "UTF-8");
        // https://stackoverflow.com/questions/8440364/htmlentities-returning-empty-string
    }
}
