<?php

namespace OhMyBingo\Content\Migration;

use OhMyBingo\Content\ContentManagementService;
use OhMyBingo\Content\ContentService;
use Psr\Log\LoggerInterface as Logger;
//use Symfony\Component\Console\Logger\ConsoleLogger as Logger;


abstract class Abstract_Migration
{
    protected $contentManagementService;
    protected $contentService;
    protected $logger;
    protected $legacyDatabase;
    // protected $downloader;

    const CONTENT_TYPE = 'Site';

    public function __construct(ContentManagementService $contentManagementService, ContentService $contentService, Logger $logger)
    {
        $this->contentManagementService = $contentManagementService;
        $this->contentService           = $contentService;
        $this->logger                   = $logger;
        $this->legacyDatabase = \Doctrine\DBAL\DriverManager::getConnection(
            [
                'url' => getenv('LEGACY_DATABASE_URL'),
                'driver' => getenv('LEGACY_DATABASE_DRIVER')
            ],
            new \Doctrine\DBAL\Configuration()
        );
        /*
        $this->downloader = new \greeflas\tools\ImageDownloader([
            'class' => \greeflas\tools\validators\ImageValidator::class
        ]);*/
    }

    abstract function run();
}
