<?php

namespace OhMyBingo\Content;

use Contentful\Core\Api\Exception;
use Contentful\Delivery\Client as DeliveryClient;
use Contentful\Management\Client;
use Contentful\Management\Resource\Entry;
use Contentful\Core\File\RemoteUploadFile;
use Contentful\Management\Resource\Asset;
use Contentful\Core\File\UnprocessedFileInterface;
use Psr\Log\LoggerInterface as Logger;

class ContentManagementService
{
    private $client;
    private $deliveryClient;
    private $spaceId;
    private $environmentId;
    private $logger;

    public function __construct(Logger $logger)
    {
        $this->client = new Client(getenv('CONTENTFUL_MANAGEMENT_ACCESS_TOKEN'));
        $this->deliveryClient = new DeliveryClient(
            getenv('CONTENTFUL_ACCESS_TOKEN'),
            getenv('CONTENTFUL_SPACE_ID'),
            getenv('CONTENTFUL_ENVIRONMENT_ID')
        );
        $this->spaceId = getenv('CONTENTFUL_SPACE_ID');
        $this->environmentId = getenv('CONTENTFUL_ENVIRONMENT_ID');
        $this->logger = $logger;
    }

    public function createEntry($contentType, $fieldData, $locale): Entry
    {
        $this->logger->info('Attempting to create entry:', ['contentType' => $contentType, 'slug' => $fieldData['slug'], 'locale' => $locale]);

        /** @var Entry $entry */
        $entry = new Entry($contentType);
        foreach ($fieldData as $key => $value) {
            $entry->setField($key, $locale, $value);
        }

        /** @var $environmentProxy */
        $environmentProxy = $this->client->getEnvironmentProxy($this->spaceId, $this->environmentId);

        try {
            $environmentProxy->create($entry);
        } catch (Exception $exception) {
            $this->logger->error('Failed to create entry', ['exception' => $exception->getMessage(), 'details' => json_decode($exception->getResponse()->getBody())]);
        }

        $this->logger->info('Successfully created entry');

        try {
            $entry->publish();
        } catch (Exception $exception) {
            $this->logger->error('Failed to publish entry', ['exception' => $exception->getMessage(), 'details' => json_decode($exception->getResponse()->getBody())]);
        }

        $this->logger->info('Successfully published entry');

        return $entry;
    }

    /*
     * @param int $entryId
     */
    public function updateEntry($contentType, $entryId, $fieldData, $locale): ?Entry
    {
        $this->logger->info('Attempting to update entry:', ['contentType' => $contentType, 'entryId' => $entryId, 'slug' => $fieldData['slug'], 'locale' => $locale]);
        /** @var $environmentProxy */
        $environmentProxy = $this->client->getEnvironmentProxy($this->spaceId, $this->environmentId);
        $entry = $environmentProxy->getEntry($entryId);

        $entry->setField('classification', 'en-US', $fieldData['classification']);
        $entry->setField('status', 'en-US', $fieldData['status']);
        $entry->setField('affiliationStatus', 'en-US', $fieldData['affiliationStatus']);
        $entry->setField('recommended', 'en-US', $fieldData['recommended']);
        $entry->setField('featured', 'en-US', $fieldData['featured']);
        $entry->setField('status', 'en-US', $fieldData['status']);
        $entry->setField('network', 'en-US', $fieldData['network']);
        $entry->setField('software', 'en-US', $fieldData['software']);
        $entry->setField('freeBingo', 'en-US', $fieldData['freeBingo']);
        $entry->setField('noDeposit', 'en-US', $fieldData['noDeposit']);
        $entry->setField('termsAndConditions', 'en-US', $fieldData['termsAndConditions']);
        $entry->setField('termsAndConditionsUrl', 'en-US', $fieldData['termsAndConditionsUrl']);

        $entry->setField('siteFeatures', 'en-US', $fieldData['siteFeatures']);
        if ($fieldData['availableGames']) {
            $entry->setField('availableGames', 'en-US', $fieldData['availableGames']);
        }

        $entry->setField('welcomeOfferLine1', 'en-US', $fieldData['welcomeOfferLine1']);
        $entry->setField('welcomeOfferLine2', 'en-US', $fieldData['welcomeOfferLine2']);

        $entry->setField('reviewIntroduction', 'en-US', $fieldData['reviewIntroduction']);
        $entry->setField('reviewWelcomeOffer', 'en-US', $fieldData['reviewWelcomeOffer']);
        $entry->setField('reviewPromotions', 'en-US', $fieldData['reviewPromotions']);
        $entry->setField('reviewGames', 'en-US', $fieldData['reviewGames']);
        $entry->setField('reviewFinalThoughts', 'en-US', $fieldData['reviewFinalThoughts']);
        $entry->setField('reviewDate', 'en-US', $fieldData['reviewDate']);

        $entry->setField('keyDetailsNoDepositOffer', 'en-US', $fieldData['keyDetailsNoDepositOffer']);
        $entry->setField('keyDetailsWelcomeBonus', 'en-US', $fieldData['keyDetailsWelcomeBonus']);
        $entry->setField('keyDetailsRedepositBonus', 'en-US', $fieldData['keyDetailsRedepositBonus']);
        $entry->setField('keyDetailsMinimumDeposit', 'en-US', $fieldData['keyDetailsMinimumDeposit']);
        $entry->setField('keyDetailsMinimumWithdrawal', 'en-US', $fieldData['keyDetailsMinimumWithdrawal']);
        $entry->setField('keyDetailsFreeBingo', 'en-US', $fieldData['keyDetailsFreeBingo']);
        $entry->setField('keyDetailsSoftware', 'en-US', $fieldData['keyDetailsSoftware']);
        $entry->setField('keyDetailsEstablished', 'en-US', $fieldData['keyDetailsEstablished']);
        $entry->setField('keyDetailsDepositMethods', 'en-US', $fieldData['keyDetailsDepositMethods']);
        $entry->setField('keyDetailsEmailAddress', 'en-US', $fieldData['keyDetailsEmailAddress']);
        $entry->setField('keyDetailsLiveChat', 'en-US', $fieldData['keyDetailsLiveChat']);
        $entry->setField('keyDetailsPhoneNumber', 'en-US', $fieldData['keyDetailsPhoneNumber']);
        $entry->setField('keyDetailsLicenseNumber', 'en-US', $fieldData['keyDetailsLicenseNumber']);

        $entry->setField('navigationText', 'en-US', $fieldData['navigationText']);
        $entry->setField('sitemapChangeFreq', 'en-US', $fieldData['sitemapChangeFreq']);
        $entry->setField('sitemapPriority', 'en-US', $fieldData['sitemapPriority']);

        try {
            $entry->update();
        } catch (Exception $exception) {
            $this->logger->error('Failed to update entry', ['exception' => $exception->getMessage(), 'details' => json_decode($exception->getResponse()->getBody())]);
        }
        $this->logger->info('Successfully updated entry');

        try {
            $entry->publish();
        } catch (Exception $exception) {
            $this->logger->error('Failed to publish updated entry', ['exception' => $exception->getMessage(), 'details' => json_decode($exception->getResponse()->getBody())]);
        }

        $this->logger->info('Successfully published entry');
        return $entry;
    }

    function createAssetFromRemoteFile(string $filename, string $mimeType, string $title, string $description, string $fileUrl, string $locale = 'en-US')
    {
        /** @var $environmentProxy */
        $environmentProxy = $this->client->getEnvironmentProxy($this->spaceId, $this->environmentId);

        /** @var Asset $asset */
        $asset = new Asset();
        $asset->setTitle($locale, $title);
        $asset->setDescription($locale,  $description);

        /** @var RemoteUploadFile $file */
        $file = new RemoteUploadFile(
            $filename,
            $mimeType,
            $fileUrl
        );
        $asset->setFile($locale, $file);

        $this->logger->info('Attempting to create asset from remote file:', ['title' => $title, 'description' => $description, 'fileUrl' => $fileUrl, 'locale' => $locale]);

        try {
            $environmentProxy->create($asset);
            $assetId = $asset->getId();

            $asset->process();

            while ($asset->getFile('en-US') instanceof UnprocessedFileInterface) {
                $asset = $environmentProxy->getAsset($assetId);
            }
        } catch (Exception $exception) {
            $this->logger->error('Failed to create asset from remote file', ['exception' => $exception->getMessage(), 'details' => json_decode($exception->getResponse()->getBody())]);
            try {
                $asset->delete();
            } catch (Exception $exception) {
                $this->logger->error('Failed to delete corrupt asset', ['exception' => $exception->getMessage(), 'details' => json_decode($exception->getResponse()->getBody())]);
            }
            $this->logger->info('Deleted corrupt asset:', ['title' => $title, 'description' => $description, 'fileUrl' => $fileUrl, 'locale' => $locale]);
            return null;
        }

        $this->logger->info('Successfully created asset');

//        try {
//            $asset = $this->deliveryClient->getAsset($assetId);
//        } catch (Exception $exception) {
//            $this->logger->info('Failed to fetch created asset.', ['exception' => $exception->getMessage(), 'details' => json_decode($exception->getResponse()->getBody()), 'assetId' => $assetId]);
//        }
//
//        $this->logger->info('Successfully fetched created asset');

        try {
            $asset->publish();
        } catch (Exception $exception) {
            $this->logger->error('Failed to publish asset', ['exception' => $exception->getMessage(), 'details' => json_decode($exception->getResponse()->getBody())]);
        }

        $this->logger->info('Successfully published asset');

        return $asset;
    }
}
