<?php

namespace OhMyBingo\Content\Command;

use FirstLeads\Infrastructure\Logger\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use OhMyBingo\Content\Deployment\DeploymentService;
use Symfony\Component\Console\Input\InputArgument;


class DeployCommand extends Command
{
    protected static $defaultName = 'content:deploy';
    protected $deploymentService;
    protected $logger;

    public function __construct(DeploymentService $deploymentService, Logger $logger)
    {
        parent::__construct();
        $this->deploymentService = $deploymentService;
        $this->logger            = $logger;
    }

    protected function configure()
    {
        $this
            ->setDescription('Deploys the site.')
            ->setHelp('This command deploys the site...')
            ->addArgument('contentType', InputArgument::OPTIONAL, 'What content type do you want to deploy')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '======================',
            'Deploy: Images',
            '======================',
            '',
        ]);
        $output->writeln(sprintf('Deploying Application: %s', $_ENV['APP_NAME']));
        $output->writeln(sprintf("\tAPP_NAME: %s", getenv('APP_NAME')));
        $output->writeln(sprintf("\tAPP_ENV: %s", getenv('APP_ENV')));
        $output->writeln(sprintf("\tAPP_SECRET: %s", getenv('APP_SECRET')));
        $output->writeln(sprintf("\tDATABASE_URL: %s", getenv('DATABASE_URL')));
        $output->writeln(sprintf("\tLEGACY_DATABASE_URL: %s", getenv('LEGACY_DATABASE_URL')));
        $this->deploymentService->deploy('images');
        $output->writeln('Deployment complete!');
    }
}