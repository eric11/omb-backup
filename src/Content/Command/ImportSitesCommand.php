<?php

namespace OhMyBingo\Content\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use OhMyBingo\Content\Migration\SitesMigration;

class ImportSitesCommand extends Command
{
    protected static $defaultName = 'content:import-sites';
    protected $sitesMigration;


    public function __construct(SitesMigration $sitesMigration)
    {
        parent::__construct();
        $this->sitesMigration = $sitesMigration;
    }

    protected function configure()
    {
        $this
            ->setDescription('Imports site data from legacy system.')
            ->setHelp('This command imports site data from the legacy system...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '======================',
            'Content - Import Sites',
            '======================',
            '',
        ]);
        $output->writeln('Importing sites...');
        $this->sitesMigration->run();
        $output->writeln('Sites import complete!');
    }
}