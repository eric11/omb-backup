<?php

namespace OhMyBingo\Content\Image;

use Contentful\Core\File\ImageOptions;

class ImageProcessor
{
    public static function getImageWidth($asset)
    {
        $imageWidth= $asset->getTitle();
        return $imageWidth;
    }

    public static function toImage($asset, $width, $height, $format = 'png', $resizeFit = 'scale', $radius = null)
    {
        return [
            'title' => $asset->getTitle().PHP_EOL,
            '1x' => self::toImageUrl($asset, $width, $height, $format, $resizeFit, $radius),
            '2x' => self::toImageUrl($asset, ($width * 2), ($height * 2), $format, $resizeFit, $radius),
            '3x' => self::toImageUrl($asset, ($width * 3), ($height * 3), $format, $resizeFit, $radius),
            '4x' => self::toImageUrl($asset, ($width * 4), ($height * 4), $format, $resizeFit, $radius),
        ];
    }

    public static function toImageUrl($asset, $width = null, $height = null, $format = 'png', $resizeFit = 'scale', $radius = null)
    {
        $options = (new ImageOptions());
        if ($width) {
            $options->setWidth($width);
        }
        if ($height) {
            $options->setHeight($height);
        }
        $options->setFormat($format);
        $options->setResizeFit($resizeFit);
        if ($radius) {
            $options->setRadius($radius);
        }
        return 'https:' . $asset->getFile()->getUrl($options);
    }
}
