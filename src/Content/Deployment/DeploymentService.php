<?php

namespace OhMyBingo\Content\Deployment;

use Psr\Log\LoggerInterface as Logger;
use OhMyBingo\Content\ContentService;

class DeploymentService
{
    protected $contentService;
    protected $logger;

    public function __construct(ContentService $contentService,  Logger $logger)
    {
        $this->contentService = $contentService;
        $this->logger            = $logger;
    }

    public function deploy(string $contentType): array
    {
        return [];
    }
}
