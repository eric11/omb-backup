<?php

namespace OhMyBingo\Content\Query;

use Contentful\Delivery\Query as BaseQuery;

class Query
{
    public static function get(string $contentType, string $whereValue, string $whereField = 'fields.slug.en-US')
    {
        $query = new BaseQuery();
        $query->setContentType($contentType)->where($whereField, $whereValue);
        return $query;
    }

    public static function getAll(string $contentType, ?string $orderByField = null)
    {
        $query = new BaseQuery();
        $query->setContentType($contentType);
        if ($orderByField) {
            $query->orderBy($orderByField);
        }
        return $query;
    }
}
