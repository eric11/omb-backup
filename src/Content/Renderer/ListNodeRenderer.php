<?php

namespace OhMyBingo\Content\Renderer;

use Contentful\RichText\NodeRenderer\NodeRendererInterface;
use Contentful\RichText\Node\UnorderedList;
use Contentful\RichText\Node\OrderedList;
use Contentful\Delivery\Resource\Asset;
use Contentful\RichText\Node\NodeInterface;
use Contentful\RichText\RendererInterface;
use OhMyBingo\Content\Image\ImageProcessor;
use Contentful\RichText\Renderer;
use Exception;

class ListNodeRenderer implements NodeRendererInterface
{
    public function __construct()
    {

    }

    public function supports(NodeInterface $node): bool
    {
        if (!$node instanceof UnorderedList && !$node instanceof OrderedList) {
            return false;
        }
        return true;
    }

    /**
     * This method is supposed to transform a node object into a string
     *
     * @param RendererInterface $renderer
     * @param NodeInterface     $node
     * @param array             $context
     * @return string
     *
     * @throws Exception
     */
    public function render(RendererInterface $renderer, NodeInterface $node, array $context = []): string
    {
        if ($node instanceof UnorderedList) {
            $element = 'ul';
        } else {
            $element = 'ol';
        }
        $renderer = new Renderer();
        $list = '<'.$element.' class="content-list"/>';
        foreach ($node->getContent() as $listItem) {
            $list .= $renderer->render($listItem);
        }
        $list .= '<'.$element.'>';
        return $list;
    }
}
