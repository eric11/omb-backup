<?php

namespace OhMyBingo\Content\Renderer;

use Contentful\RichText\NodeRenderer\NodeRendererInterface;
use Contentful\RichText\Node\EmbeddedAssetBlock;
use Contentful\RichText\Node\EmbeddedAssetInline;
use Contentful\Delivery\Resource\Asset;
use Contentful\RichText\Node\NodeInterface;
use Contentful\RichText\RendererInterface;
use OhMyBingo\Content\Image\ImageProcessor;
use Exception;

class AssetNodeRenderer implements NodeRendererInterface
{
    public function __construct()
    {

    }

    public function supports(NodeInterface $node): bool
    {
        if (!$node instanceof EmbeddedAssetBlock && !$node instanceof EmbeddedAssetInline) {
            return false;
        }
        return true;
    }

    /**
     * This method is supposed to transform a node object into a string
     *
     * @param RendererInterface $renderer
     * @param NodeInterface     $node
     * @param array             $context
     * @return string
     *
     * @throws Exception
     */
    public function render(RendererInterface $renderer, NodeInterface $node, array $context = []): string
    {
        /** @var Asset $asset */
        $asset = $node->getAsset();
        $assetFile = $asset->getFile();
        $assetMimeType = $assetFile->getContentType();
        $imageMimeTypes = [
            'image/png',
            'image/jpeg',
            'image/svg+xml',
        ];
        if (!in_array($assetMimeType, $imageMimeTypes)) {
            throw new Exception(sprintf('Asset node renderer attempted to render unsupported mime type: %s', $assetMimeType));
        }
        $imageUrl = ImageProcessor::toImageUrl($asset);
        return \sprintf(
            '<img class="content-asset--image" alt="%s" src="%s"/>',
            $asset->getDescription(),
            $imageUrl
        );
    }
}
