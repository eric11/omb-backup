<?php

namespace OhMyBingo\Content;

use Contentful\Delivery\Query as BaseQuery;
use Contentful\Delivery\Client;
use Psr\Log\LoggerInterface as Logger;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
//use Contentful\Core\Exception\InvalidQueryException;

class ContentService
{
    private $client;
    private $logger;

    public function __construct(Logger $logger)
    {
        $this->client = new Client(
            getenv('CONTENTFUL_ACCESS_TOKEN'),
            getenv('CONTENTFUL_SPACE_ID'),
            getenv('CONTENTFUL_ENVIRONMENT_ID')
        );
        $this->logger = $logger;
    }

    public function getEntry(BaseQuery $query)
    {
        $entries = $this->client->getEntries($query)->getItems();
        return (!empty($entries) ? $entries[0] : null);
    }

    public function getEntries(BaseQuery $query)
    {
        $entries = $this->client->getEntries($query)->getItems();
        return (!empty($entries) ? $entries : null);
    }

    public function getAsset(string $id, string $locale)
    {
        return $this->client->getAsset($id, $locale);
    }

    public function getAssets(BaseQuery $query)
    {
        try {
            /** @var $environmentProxy */
            return $this->client->getAssets($query);
        } catch (\Exception $exception) {
            $this->logger->error('Failed to get assets', ['query' => $query->getQueryData()]);
        }
    }

    public function parseJson(string $json)
    {
        return $this->client->parseJson($json);
    }
}
