<?php

namespace OhMyBingo\FeatureFlag;

use OhMyBingo\Search\SearchService;

class FeatureFlagService
{
    private $hostname;
    private $urlSet;

    public function __construct(SearchService $searchService, $hostname)
    {
        $featureFlagSlug = 'notificationBell';
        $featureFlags = $searchService->search('featureFlag', 'featureFlag', 'slug', $featureFlagSlug);
        dd($featureFlags);
    }

    public function addUrl(string $loc, string $lastMod, string $changeFreq, float $priority)
    {
        array_push($this->urlSet, (object) [
            'loc'        => $loc,
            'lastMod'    => $lastMod,
            'changeFreq' => $changeFreq,
            'priority'   => $priority
        ]);
    }

    public function getUrlSet()
    {
        return $this->urlSet;
    }
}
