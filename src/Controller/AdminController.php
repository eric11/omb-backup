<?php

namespace OhMyBingo\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", defaults={"path"=null}, name="admin", methods={"GET"})
     *
     * @param $request     Request
     * @param $path
     * @return Response
     */
    public function index(Request $request, $path): Response
    {
        return $this->render('pages/admin/admin.html.twig', [
            'admin' => true,
        ]);
    }

    public function adminDashboard()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        return new Response('Well hi there '.$user->getFirstName());
    }
}
