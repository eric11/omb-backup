<?php

namespace OhMyBingo\Controller;

use OhMyBingo\Page\PageService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AboutController extends AbstractController
{
    /**
     * @Route("/about", name="about", methods={"GET"})
     *
     * @param $pageService PageService
     * @param $request     Request
     * @return Response
     */
    public function index(PageService $pageService, Request $request): Response
    {
        $path = ltrim($request->getPathInfo(), '/');
        $page = $pageService->getLegalPage($path);
        if (!$page) {
            throw new NotFoundHttpException('Sorry, about page not found matching the path: ' . $path);
        }
        return $this->render('pages/about/about.html.twig', [
            'page' => $page,
            'amp'  => ($request->get('amp') !== null),
            'hideSidebar' => true,
        ]);
    }
}
