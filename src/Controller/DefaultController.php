<?php

namespace OhMyBingo\Controller;

use FirstLeads\Presentation\Pagination\PaginationService;
use OhMyBingo\Page\Component\SiteListings\SiteListings;
use OhMyBingo\Page\DefaultPage;
use OhMyBingo\Page\PageService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use OhMyBingo\Page\PageInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends AbstractController
{
    /**
     * @Route("/{path}", defaults={"path"=null}, name="defaultCatchAll", methods={"GET"})
     *
     * @param $pageService       PageService
     * @param $paginationService PaginationService
     * @param $request           Request
     * @param $path
     * @return Response
     */
    public function index(PageService $pageService, PaginationService $paginationService, Request $request, string $path): Response
    {
        try {
            /** @var DefaultPage $page */
            $page = $pageService->get($path);

            # Paginate results
            $pageNumber = $request->get('page');
            if (!$pageNumber) {
                $pageNumber = 1;
            }

            /** @var SiteListings $siteListings */
            $siteListings = $page->getSiteListings();
            $siteListingsSites = $siteListings->getSites();
            $pagination = $paginationService->getPagination($siteListingsSites, $pageNumber);
            $siteListings->setPagination($pagination);

            return $this->render('pages/default.html.twig', [
                'page'         => $page,
                'path'         => $path,
                'siteListings' => $siteListings,
                'amp'          => ($request->get('amp') !== null),
            ]);
        } catch (NotFoundHttpException $exception) {
            /** @var PageInterface $page */
            $page = $pageService->get('site-review', $siteSlug = $path, 'site');
            return $this->render('pages/site-review/entry.html.twig', [
                'page' => $page,
                'amp'  => ($request->get('amp') !== null),
            ]);
        }
    }
}
