<?php

namespace OhMyBingo\Controller;

use OhMyBingo\ContentPreview\ContentPreviewService;
use OhMyBingo\Page\Component\ComponentBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use RuntimeException;

/**
 * Play controller.
 * @Route("/", name="api_content_preview")
 */
class ContentPreviewController extends FOSRestController
{
    protected $contentPreviewService;
    protected $componentBuilder;

    /**
     * Content Preview.
     *
     * @Route("content-preview/{contentType}/{entrySlug}", defaults={"contentType"=null, "entrySlug"=null}, name="content-preview", methods={"GET"})
     *
     * @param $contentPreviewService
     * @param $componentBuilder
     * @param $request
     * @param $contentType
     * @param $entrySlug
     * @return Response
     */
    public function getContentPreviewAction(ContentPreviewService $contentPreviewService, ComponentBuilder $componentBuilder, Request $request, ?string $contentType, ?string $entrySlug): Response
    {
//        $entrySlug = $request->get('entrySlug');
        $this->contentPreviewService = $contentPreviewService;
        $this->componentBuilder      = $componentBuilder;
        if (!$contentType && $entrySlug) {
            throw new RuntimeException('Content preview requires content type and entry slug to be defined');
        } else {
            $componentEntry = $this->contentPreviewService->getContentPreview($contentType, $entrySlug);
            // leaderboard
//            dd($entry);
            $componentClassName = ucwords($contentType);
            $component = $this->componentBuilder->buildFromEntry($componentClassName, $componentEntry);
             $leaderBoard = $component;
//             dd($leaderBoard);
//            $page = $pageService->get('home');
            return $this->render('components/widgets/leader-board.html.twig', [
                'leaderBoard'    => $leaderBoard,
                'contentPreview' => true,
            ]);
        }
    }
}