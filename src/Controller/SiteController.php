<?php

namespace OhMyBingo\Controller;

use FirstLeads\Presentation\Pagination\PaginationService;
use OhMyBingo\Page\Component\SiteListings\SiteListings;
use OhMyBingo\Page\PageService;
use OhMyBingo\Page\PageInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SiteController extends AbstractController
{
    /**
     * @Route("/online-bingo-reviews/{siteSlug}", defaults={"siteSlug"=null}, name="online-bingo-reviews", methods={"GET"})
     * @Route("/online-slot-reviews/{siteSlug}", defaults={"siteSlug"=null}, name="online-slot-reviews", methods={"GET"})
     * @Route("/online-casino-reviews/{siteSlug}", defaults={"siteSlug"=null}, name="online-casino-reviews", methods={"GET"})
     *
     * @param $pageService       PageService
     * @param $paginationService PaginationService
     * @param $request
     * @param $siteSlug
     * @return Response
     */
    function index(PageService $pageService, PaginationService $paginationService, Request $request, ?string $siteSlug): Response
    {
        if ($siteSlug) {
            /** @var PageInterface $page */
            $page = $pageService->get('site-review', $siteSlug, 'site');
            return $this->render('pages/site-review/entry.html.twig', [
                'page' => $page,
                'amp'  => ($request->get('amp') !== null),
            ]);
        }
        $pathInfo = ltrim($request->getPathInfo(), '/');
        if (!in_array($pathInfo, ['online-bingo-reviews', 'online-slot-reviews', 'online-casino-reviews'])) {
            throw new BadRequestHttpException('Allowed paths: `online-bingo-reviews`, `online-slot-reviews`, `online-casino-reviews`');
        }
        $page = $pageService->get($pathInfo);

        # Paginate results
        $pageNumber = $request->get('page');
        if (!$pageNumber) {
            $ajax = false;
            $pageNumber = 1;
        } else {
            $ajax = true;
        }
        /** @var array $siteListingsSites */
        $siteListings = $page->getSiteListings();
        $siteListingsSites = $siteListings->getSites();
        $pagination = $paginationService->getPagination($siteListingsSites, $pageNumber);
        $siteListings->setPagination($pagination);
        return $this->render('pages/site-review/index.html.twig', [
            'page'         => $page,
            'path'         => $pathInfo,
            'siteListings' => $siteListings,
            'amp'          => ($request->get('amp') !== null),
        ]);
    }
}