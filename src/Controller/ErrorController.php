<?php

namespace OhMyBingo\Controller;

use OhMyBingo\Page\ExceptionPage;
use OhMyBingo\Page\PageService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;

class ErrorController extends AbstractController
{
    protected $debug; // this is passed as a parameter from services.yaml
    protected $code;  // 404, 500, etc.
    protected $pageService;

    public function __construct(PageService $pageService, bool $debug)
    {

        $this->debug       = $debug;
        $this->pageService = $pageService;

    }

    public function showException(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null): Response
    {
        // dd($exception); // uncomment me to see the exception
        /** @var ExceptionPage $page */
        $page = $this->pageService->getExceptionPage($exception);
        $statusCode = (int) $exception->getStatusCode();

        /** @var array $errorTemplates */
        $errorTemplates = [401, 403, 404, 500];
        if (in_array($statusCode, $errorTemplates)) {
            $template = 'errors/' . $exception->getStatusCode() . '.error.html.twig';
        } else {
            $template = 'errors/error.html.twig';
        }
        return $this->render(
            $template,
            array_merge(
                [
                    'page'        => $page,
                    'errorCode'   => $statusCode,
                    'hideSidebar' => true,
                    'noIndex'     => true,
                ]
            )
        );
    }
}
