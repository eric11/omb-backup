<?php

namespace OhMyBingo\Controller;

use OhMyBingo\Page\DefaultPage;
use OhMyBingo\Page\NewsArticlePage;
use OhMyBingo\Page\PageService;
use OhMyBingo\Page\PageInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class NewsController extends AbstractController
{
    /**
     * @Route("/bingo-news/{newsArticleSlug}", defaults={"newsArticleSlug"=null}, name="news", methods={"GET"})
     *
     * @param $pageService PageService
     * @param $request
     * @param $newsArticleSlug
     * @return Response
     */
    function index(PageService $pageService, Request $request, ?string $newsArticleSlug): Response
    {
        $path = NewsArticlePage::ROUTE_NAME;
        if ($newsArticleSlug) {
            /** @var DefaultPage $page */
            $page = $pageService->get($path, $newsArticleSlug, $path);
            return $this->render('pages/news/entry.html.twig', [
                'page' => $page,
                'amp'  => ($request->get('amp') !== null),
            ]);
        }
        $page = $pageService->get($path);
        return $this->render('pages/'.$path.'/index.html.twig', [
            'page'         => $page,
            'path'         => $path,
            'newsListings' => $page->getNewsListings() ? $page->getNewsListings()->getNewsArticles() : [],
            'amp'          => ($request->get('amp') !== null),
        ]);
    }
}