<?php

namespace OhMyBingo\Controller;

use FirstLeads\Presentation\Play\PlayService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Play controller.
 * @Route("/", name="api_play")
 */
class PlayController extends FOSRestController
{
    protected $playService;

    /**
     * Lists all play links.
     *
     * @Route("play/{componentSlug}/{siteSlug}/{affiliateCode}", defaults={"componentSlug"=null, "siteSlug"=null, "affiliateCode"=null}, name="play", methods={"GET"})
     *
     * @param $playService
     * @param $request
     * @param $componentSlug
     * @param $siteSlug
     * @return Response
     */
    public function getPlayAction(PlayService $playService, Request $request, string $componentSlug, string $siteSlug): Response
    {
        $this->playService = $playService;
        $redirectUrl = $this->playService->getRedirectUrl($componentSlug, $siteSlug);
        if (!$redirectUrl) {
            throw new NotFoundHttpException('Failed to redirect to play link for this site: ' . $siteSlug);
        }
        return $this->redirect($redirectUrl);
    }
}