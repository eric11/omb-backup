<?php

namespace OhMyBingo\Controller;

use OhMyBingo\Page\DefaultPage;
use OhMyBingo\Page\PageService;
use OhMyBingo\Page\PageInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ArticlesController extends AbstractController
{
    /**
     * @Route("/bingo-articles/{articleSlug}", defaults={"articleSlug"=null}, name="articles", methods={"GET"})
     *
     * @param $pageService PageService
     * @param $request
     * @param $articleSlug
     * @return Response
     */
    function index(PageService $pageService, Request $request, ?string $articleSlug): Response
    {
        $path = 'articles';
        if ($articleSlug) {
            /** @var DefaultPage $page */
            $page = $pageService->get('article', $articleSlug, 'article');
            return $this->render('pages/'.$path.'/entry.html.twig', [
                'page'            => $page,
                'articleListings' => $page->getArticleListings() ? $page->getArticleListings()->getArticles() : [],
                'amp'             => ($request->get('amp') !== null),
            ]);
        }
        $page = $pageService->get($path);
        return $this->render('pages/'.$path.'/index.html.twig', [
            'page'            => $page,
            'amp'             => ($request->get('amp') !== null),
            'path'            => $path,
            'articleListings' => $page->getArticleListings() ? $page->getArticleListings()->getArticles() : [],
        ]);
    }
}

