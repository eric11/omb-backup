<?php

namespace OhMyBingo\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use OhMyBingo\Entity\Comment;
use OhMyBingo\Form\CommentType;
use DateTime;

/**
 * Movie controller.
 * @Route("/api", name="api_comments")
 */
class CommentController extends FOSRestController
{

    /**
     * Lists all Comments.
     * @Rest\Get("/comments")
     *
     * @return Response
     */
    public function getCommentsAction()
    {
        $repository = $this->getDoctrine()->getRepository(Comment::class);
        $comments = $repository->findAll();
        return $this->json($comments);
    }

    /**
     * Create Comment.
     * @Rest\Post("/comments")
     *
     * @param Request $request
     * @return Response
     */
    public function postCommentAction(Request $request)
    {
        $data = [
            'name' => $request->get('name'),
            'message' => $request->get('message'),
            'siteId' => $request->get('siteId')
        ];
        $comment = new Comment();
        $comment->setName($data['name']);
        $datetime = new DateTime("now");
        $comment->setDatetime($datetime);
        $comment->setMessage($data['message']);
        $comment->setSiteId($data['siteId']);

        $form = $this->createForm(CommentType::class, $comment);

//        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }

        return $this->handleView($this->view($form->getErrors()));
    }

}