<?php

namespace OhMyBingo\Controller;

use OhMyBingo\AutoSuggestions\AutoSuggestionsService;
use OhMyBingo\AutoSuggestions\Exception\NoAutoSuggestionsException;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Auto-Suggestions controller.
 * @Route("/api", name="api_autoSuggestions")
 */
class AutoSuggestionsController extends FOSRestController
{
    const ERROR_CODE = AutoSuggestionsController::class;

    /**
     * Lists all Comments.
     * @Rest\Get("/auto-suggestions")
     *
     * @return Response
     * @param $autoSuggestionsService
     */
    public function getAutoSuggestionsAction(AutoSuggestionsService $autoSuggestionsService)
    {
        try {
            return $this->json($autoSuggestionsService->get());
        } catch (NoAutoSuggestionsException $exception) {
            return $this->json([
                'status' => 404,
                'errorMessage' => $exception->getMessage(),
                'error' => [
                    'code'    => self::ERROR_CODE,
                    'message' => $exception->getMessage(),
                ]
            ]);
        }
    }
}