<?php

namespace OhMyBingo\Controller;

use FirstLeads\Presentation\Pagination\Pagination;
use FirstLeads\Presentation\Pagination\PaginationService;
use OhMyBingo\Page\Component\SiteListings\SiteListings;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;
use OhMyBingo\Search\SearchService;
use OhMyBingo\Page\PageService;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search", methods={"GET"})
     *
     * @param $pageService PageService
     * @param $searchService
     * @param $paginationService
     * @param $logger
     * @param $request
     * @return Response
     */
    function index(PageService $pageService, SearchService $searchService, PaginationService $paginationService, LoggerInterface $logger, Request $request): Response
    {
//        $logger->info('I just got the logger');
//        $indexResult = $searchService->index('test_index', 'test_type', 'test_id', ['name' => 'Eric']);
        $page = $pageService->get('search');
        $searchQuery = $request->query->get('q'); // query
        // $autoComplete = $request->query->get('s'); // auto-suggest
//        dd($searchQuery);

        $siteSearchResults = $searchService->search('site', 'site', 'name', $searchQuery);

        # Paginate results
        $pageNumber = $request->get('page');
        if (!$pageNumber) {
            $pageNumber = 1;
        }

        /** @var Pagination $pagination */
        $pagination = $paginationService->getPagination($siteSearchResults->getEntries(), $pageNumber);
        $siteListings = new SiteListings('somePath', $siteSearchResults->getEntries());
        $siteListings->setPagination($pagination);

        $searchResults = [
            'query'        => $searchQuery,
            'siteListings' => $siteListings
//            'pages'    => $searchService->search('page', 'page', 'title', $searchQuery),
//            'articles' => $searchService->search('article', 'article', 'title', $searchQuery),
//            'news'     => $searchService->search('news', 'news', 'title', $searchQuery),
        ];

        if (!empty($siteListings->getSites())) {
            $page = $pageService->fetchAndSetModal($page, 'no-affiliation-bingo');
        }

        // $page->getArticle()->replaceInCopy('{{ query }}', $searchQuery);

        return $this->render('pages/search/search-results.html.twig', [
            'page'          => $page,
            'path'          => 'search',
            'searchResults' => $searchResults,
            'amp'           => ($request->get('amp') !== null),
        ]);
    }
}