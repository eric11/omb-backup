<?php

namespace OhMyBingo\Controller;

use FirstLeads\Presentation\TermsAndConditions\TermsAndConditionsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * TermsAndConditions controller.
 * @Route("/", name="api_terms_and_conditions")
 */
class TermsAndConditionsController extends FOSRestController
{
    protected $termsAndConditionsService;

    /**
     * Lists all terms and conditions links.
     *
     * @Route("terms/{componentSlug}/{siteSlug}/{affiliateCode}", defaults={"componentSlug"=null, "siteSlug"=null, "affiliateCode"=null}, name="termsAndConditions", methods={"GET"})
     *
     * @param $termsAndConditionsService
     * @param $request
     * @param $componentSlug
     * @param $siteSlug
     * @return Response
     */
    public function getTermsAndConditionsAction(TermsAndConditionsService $termsAndConditionsService, Request $request, string $componentSlug, string $siteSlug): Response
    {
        $this->termsAndConditionsService = $termsAndConditionsService;
        return $this->termsAndConditions($componentSlug, $siteSlug);
    }

    private function termsAndConditions(string $componentSlug, $siteSlug): Response
    {
        return $this->redirect($this->termsAndConditionsService->getRedirectUrl($componentSlug, $siteSlug));
    }
}