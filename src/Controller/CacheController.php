<?php

namespace OhMyBingo\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface as Logger;
use OhMyBingo\Page\PageService;
use FirstLeads\Infrastructure\Cache\CacheService;

class CacheController extends AbstractController
{
    /**
     * @Route("/cache/get/{key}", defaults={"_format"="json", "key"=null}, name="cache-get", methods={"GET"})
     *
     * @param $cacheService CacheService
     * @param $logger       Logger
     * @param $request      Request
     * @param $key          string
     * @return Response     Request
     */
    function index(CacheService $cacheService, Logger $logger, Request $request, ?string $key): Response
    {
        # Get all values
        if (!$key) {
            $values = $cacheService->all();
            return $this->json($values);
        }

        # Get a single value
        $value = $cacheService->get($key);
        return $this->json($value);
    }


    /**
     * @Route("/cache/set", defaults={"_format"="json", "key"=null}, name="cache-set", methods={"GET"})
     *
     * @param $cacheService CacheService
     * @param $pageService  PageService
     * @param $logger       Logger
     * @param $request      Request
     * @param $key          string
     * @return Response     Request
     */
    function save(CacheService $cacheService, PageService $pageService, Logger $logger, Request $request, ?string $key): Response
    {
        # Fetch index of all pages
        $index = $pageService->index();

        // Index all pages
        $result = [
            'pages' => 3,
            'success' => true
        ];
        return $this->json($result);
    }
}