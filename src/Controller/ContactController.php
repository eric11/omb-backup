<?php

namespace OhMyBingo\Controller;

use OhMyBingo\Page\PageService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", defaults={"path"=null}, name="contact", methods={"GET"})
     *
     * @param $pageService PageService
     * @param $request     Request
     * @return Response
     */
    public function index(PageService $pageService, Request $request): Response
    {
        $path = ltrim($request->getPathInfo(), '/');
        $page = $pageService->getLegalPage($path);
        if (!$page) {
            throw new NotFoundHttpException('Sorry, contact page not found matching the path: ' . $path);
        }
        return $this->render('pages/contact/contact.html.twig', [
            'page' => $page,
            'hideSidebar' => true,
            'amp'   => ($request-x>get('amp') !== null),
        ]);
    }
}
