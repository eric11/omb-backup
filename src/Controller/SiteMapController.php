<?php

namespace OhMyBingo\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface as Logger;
use  OhMyBingo\Search\SearchService;
use OhMyBingo\Page\PageService;
use OhMyBingo\SiteMap\SiteMapService;

class SiteMapController extends AbstractController
{
    /**
     * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"}, methods={"GET"})
     *
     * @param $request
     * @param PageService    $pageService
     * @param SiteMapService $siteMapService
     * @param Logger $logger
     * @return Response
     */
    function index(PageService $pageService, SiteMapService $siteMapService, Logger $logger, Request $request): Response
    {
//        $hostname = $request->getSchemeAndHttpHost();

        $loc = 'bingo-articles';
        $lastMod = '2005-01-01';
        $changeFreq = 'monthly';
        $priority = '0.8';
//        $siteMapService->addUrl($loc, $lastMod, $changeFreq, $priority);

        return $this->render('pages/sitemap/sitemap.xml.twig', [
            'urlset' => $siteMapService->getUrlSet(),
        ]);
    }
}