<?php

namespace OhMyBingo\Controller;

use OhMyBingo\Page\PageService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LegalController extends AbstractController
{
    /**
     * @Route("/terms-and-conditions", defaults={"path"=null}, name="terms-and-conditions", methods={"GET"})
     * @Route("/privacy-policy", defaults={"path"=null}, name="privacy-policy", methods={"GET"})
     * @Route("/responsible-gaming", defaults={"path"=null}, name="responsible-gaming", methods={"GET"})
     *
     * @param $pageService PageService
     * @param $request     Request
     * @return Response
     */
    public function index(PageService $pageService, Request $request): Response
    {
        $path = ltrim($request->getPathInfo(), '/');
        $page = $pageService->getLegalPage($path);
        if (!$page) {
            throw new NotFoundHttpException('Sorry, legal page not found matching the path: ' . $path);
        }
        return $this->render('pages/legal/legal.html.twig', [
            'page' => $page,
            'amp'  => ($request->get('amp') !== null),
            'hideSidebar' => true,
        ]);
    }
}
