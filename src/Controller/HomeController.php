<?php

namespace OhMyBingo\Controller;

use OhMyBingo\Page\PageService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController implements PageControllerInterface
{
    /**
     * @Route("/", name="home", methods={"GET", "POST"})
     *
     * @param $pageService PageService
     * @param $request     Request
     * @return Response
     */
    public function index(PageService $pageService, Request $request): Response
    {
        $page = $pageService->get('home');
        return $this->render('pages/home.html.twig', [
            'page' => $page,
            'amp'  => ($request->get('amp') !== null),
        ]);
    }
}
