<?php

namespace OhMyBingo\SiteMap;

use OhMyBingo\Content\ContentService;
use OhMyBingo\Content\Query\Query;
use OhMyBingo\Page\deprecated;
use OhMyBingo\Page\LegalPage;
use OhMyBingo\Page\DefaultPage;
use OhMyBingo\Page\NewsArticlePage;
use OhMyBingo\Page\SitePage;
use OhMyBingo\Search\SearchService;
use DateTime;

class SiteMapService
{
    protected $contentService;
    protected $searchService;
    private $hostname;
    private $urlSet = [];

    public function __construct(ContentService $contentService, SearchService $searchService)
    {
        $this->contentService = $contentService;
        $this->searchService  = $searchService;
        $this->hostname = 'http://ohmybingo.com'; //getenv('APP_HOSTNAME');
        $this->addPages();
//        dd($pages);

//        $pageEntries = $searchService->search('legalPage', 'legalPage')->getEntries();
//        dd($pageEntries);
        # Sites
//        $sites = $searchService->search('site', 'site', 'name', $searchQuery);
//        $urls = [];
//        foreach ($urls as $url) {
//            $this->addUrl($url);
//        }
    }

    public function addUrl(string $loc, string $lastMod, string $changeFreq, float $priority)
    {
        array_push($this->urlSet, (object) [
            'loc'        => $loc,
            'lastMod'    => $lastMod,
            'changeFreq' => $changeFreq,
            'priority'   => $priority
        ]);
    }

    public function getUrlSet()
    {
        return $this->urlSet;
    }

    private function addPages(): void
    {
        # Home
        $defaultPageEntries  = $this->contentService->getEntries(Query::getAll(DefaultPage::CONTENT_TYPE, 'fields.sitemapPosition.en-US'));
        $this->addPageEntries($defaultPageEntries, null,  'home');
        # Legal Pages
        $legalPageEntries  = $this->contentService->getEntries(Query::getAll(LegalPage::CONTENT_TYPE, 'fields.sitemapPosition'));
        $this->addPageEntries($legalPageEntries, 'home');
        # Other Pages (Listings)
        $this->addPageEntries($defaultPageEntries, 'home');
        # Sites
        $siteEntries  = $this->contentService->getEntries(Query::getAll(SitePage::CONTENT_TYPE, 'fields.sitemapPosition,fields.name'));
        $this->addSiteEntries($siteEntries);
        # News
        $newsEntries  = $this->contentService->getEntries(Query::getAll(NewsArticlePage::CONTENT_TYPE, 'fields.sitemapPosition,fields.date'));
        $this->addNewsEntries($newsEntries);
        # Articles
        $articleEntries  = $this->contentService->getEntries(Query::getAll(NewsArticlePage::CONTENT_TYPE, 'fields.sitemapPosition,fields.date'));
        $this->addArticleEntries($articleEntries);

    }

    private function addPageEntries($pageEntries, ?string $exclude = null, ?string $include = null): void
    {
        foreach ($pageEntries as $pageEntry) {
            $lastMod = (new DateTime($pageEntry->getSystemProperties()->getUpdatedAt()))->format('Y-m-d');
            if ($exclude && ($pageEntry->getSlug() !== $exclude) || $include && ($pageEntry->getSlug() === $include)) {
                $this->addUrl(
                    $this->toLoc($pageEntry->getSlug()),
                    $lastMod,
                    $pageEntry->getSitemapChangeFreq()['en-US'] ?? 'monthly',
                    $pageEntry->getSitemapPriority()['en-US'] ?? '0.8'
                );
            }
        }
    }

    private function addSiteEntries($siteEntries): void
    {
        foreach ($siteEntries as $siteEntry) {
            $lastMod = (new DateTime($siteEntry->getSystemProperties()->getUpdatedAt()))->format('Y-m-d');
            $this->addUrl(
                $this->toLoc($siteEntry->getSlug()),
                $lastMod,
                $siteEntry->getSitemapChangeFreq()['en-US'] ?? 'monthly',
                $siteEntry->getSitemapPriority()['en-US'] ?? '0.8'
            );
        }
    }

    private function addNewsEntries($newsEntries): void
    {
        foreach ($newsEntries as $newsEntry) {
            $lastMod = (new DateTime($newsEntry->getSystemProperties()->getUpdatedAt()))->format('Y-m-d');
            $this->addUrl(
                $this->toLoc('news'.'/'.$newsEntry->getSlug()),
                $lastMod,
                $newsEntry->getSitemapChangeFreq()['en-US'] ?? 'monthly',
                $newsEntry->getSitemapPriority()['en-US'] ?? '0.8'
            );
        }
    }

    private function addArticleEntries($articleEntries): void
    {
        foreach ($articleEntries as $articleEntry) {
            $lastMod = (new DateTime($articleEntry->getSystemProperties()->getUpdatedAt()))->format('Y-m-d');
            $this->addUrl(
                $this->toLoc('articles'.'/'.$articleEntry->getSlug()),
                $lastMod,
                $articleEntry->getSitemapChangeFreq()['en-US'] ?? 'monthly',
                $articleEntry->getSitemapPriority()['en-US'] ?? '0.8'
            );
        }
    }

    private function toLoc($slug): string
    {
        if ($slug === 'home') {
            return $this->hostname . '/';
        } else {
            return $this->hostname . '/' . $slug;
        }
    }
}
