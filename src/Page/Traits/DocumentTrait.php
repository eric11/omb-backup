<?php

namespace OhMyBingo\Page\Traits;

use OhMyBingo\Page\Attributes\Article;

trait DocumentTrait
{
    protected $article;

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(Article $article): void
    {
        $this->article = $article;
    }
}