<?php

namespace OhMyBingo\Page\Traits;


trait PathTrait
{
    protected $path;

    public function getPath(): ?string
    {
        return $this->path;
    }

    protected function setPath(string $path): void
    {
        $this->path = $path;
    }
}