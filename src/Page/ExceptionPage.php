<?php

namespace OhMyBingo\Page;

use OhMyBingo\Page\Attributes\Slug;
use OhMyBingo\Page\Attributes\Meta;
use OhMyBingo\Page\Attributes\Article;
use OhMyBingo\Page\Traits\DocumentTrait;
use Symfony\Component\Debug\Exception\FlattenException;

class ExceptionPage extends AbstractPage implements PageInterface
{
    use DocumentTrait;

    const CONTENT_TYPE = 'errorPage';

//    protected $article;
    private $exception = [];

    public function __construct(
        Slug $slug,
        Meta $meta,
        Article $article,
        FlattenException $exception
    ) {
        parent::__construct($slug, $meta);
        $this->setArticle($article);
        $this->exception = json_encode($exception, true);
    }

    public function getException(): array
    {
        return json_decode($this->exception);
    }
}
