<?php

namespace OhMyBingo\Page\Attributes\Picture;

trait PictureTrait
{
    protected $picture;

    public function getPicture()
    {
        return $this->picture;
    }

    public function getPictureUrl()
    {
        return $this->picture['1x'];
    }
}
