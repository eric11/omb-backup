<?php

namespace OhMyBingo\Page\Attributes\News;

use OhMyBingo\Content\Image\ImageProcessor;
use DateTime;
use OhMyBingo\Page\Attributes\Picture\PictureTrait;
use OhMyBingo\Page\Attributes\RichSnippet\RichSnippetTrait;

class NewsArticle
{
    use RichSnippetTrait;
    use PictureTrait;

    protected $slug;
    protected $title;
    protected $copy;
    protected $site;
    protected $date;

    public function __construct(string $slug, string $title, string $copy, $picture, $siteEntry, string $date)
    {
        $this->slug        = $slug;
        $this->title       = $title;
        $this->copy        = $copy;
        if ($picture) {
            $this->picture = ImageProcessor::toImage($picture, 1200, 1200, 'png', 'pad');
        }
        if (!empty($siteEntry)) {
            $siteLogo = ImageProcessor::toImage($siteEntry->logo, 220, 138, 'png', 'pad');
            $siteData = [
                'name' => $siteEntry->name,
                'logo' => $siteLogo,
                'slug' => $siteEntry->slug
            ];
            $this->site = $siteData;
        }
        $this->date        = (new DateTime($date))->format('d F y'); // convert 2019-03-21T00:00:00Z to 23 May 19
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getDescription(): ?string
    {
        return $this->copy;
    }

    public function getCopy(): ?string
    {
        return $this->copy;
    }

    public function getSite()
    {
        return $this->site;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }
}
