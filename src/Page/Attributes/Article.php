<?php

namespace OhMyBingo\Page\Attributes;

class Article
{
    private $title;
    private $copy;

    public function __construct(?string $title, ?string $copy)
    {
        $this->title = $title;
        $this->copy  = $copy;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getCopy(): ?string
    {
        return $this->copy;
    }

//    public function replaceInCopy($search, $replace): void
//    {
//        $this->copy = str_replace($search, $replace, $this->copy);
//    }
}
