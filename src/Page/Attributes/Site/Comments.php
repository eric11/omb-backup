<?php

namespace OhMyBingo\Page\Attributes\Site;

use DateTime;
use Westsworld\TimeAgo;

class Comments
{
    private $comments;

    public function __construct(array $rawComments) {
        $comments = [];
        foreach ($rawComments as $rawComment) {
            $timeAgo = (new TimeAgo())->inWords($rawComment->getDatetime());
            $timeAgo = str_replace(' years', 'yrs', $timeAgo);
            $timeAgo = str_replace(' months', 'mo', $timeAgo);
            $timeAgo = str_replace(' days', 'd', $timeAgo);
            $timeAgo = str_replace(' hours', 'h', $timeAgo);
            $timeAgo = str_replace(' minutes', 'm', $timeAgo);
            $timeAgo = str_replace(' seconds', 's', $timeAgo);
            array_push($comments,
                [
                    'name'    => $rawComment->getName(),
                    'time'    => $timeAgo,
                    'message' => $rawComment->getMessage(),
                ]);
        }
        $this->comments = $comments;
    }

    public function get(): ?array
    {
        return $this->comments;
    }
}
