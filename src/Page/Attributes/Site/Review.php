<?php

namespace OhMyBingo\Page\Attributes\Site;

use DateTime;
use OhMyBingo\Page\Attributes\RichSnippet\RichSnippetInterface;
use OhMyBingo\Page\Attributes\RichSnippet\RichSnippetTrait;

class Review
{
    use RichSnippetTrait;
    
    private $title;
    private $introduction;
    private $welcomeOffer;
    private $promotions;
    private $games;
    private $finalThoughts;
    private $date;
    private $hasContents;

    public function __construct(
        ?string $title,
        ?string $introduction,
        ?string $welcomeOffer,
        ?string $promotions,
        ?string $games,
        ?string $finalThoughts,
        ?string $date
    ) {
        $this->title         = html_entity_decode($title);
        $this->introduction  = html_entity_decode($introduction);
        $this->welcomeOffer  = html_entity_decode($welcomeOffer);
        $this->promotions    = html_entity_decode($promotions);
        $this->games         = html_entity_decode($games);
        $this->finalThoughts = html_entity_decode($finalThoughts);

        // convert 2019-03-21T00:00:00Z to 23 May 19
        $dateOnly            = new DateTime($date);
        $this->date          = $dateOnly->format('d F y');
        $this->hasContents   = false;
        if (
            $welcomeOffer
            || $promotions
            || $games
            || $finalThoughts
        ) {
            $this->hasContents = true;
        }
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getIntroduction(): ?string
    {
        return $this->introduction;
    }

    public function getWelcomeOffer(): ?string
    {
        return $this->introduction;
    }

    public function getPromotions(): ?string
    {
        return $this->promotions;
    }

    public function getGames(): ?string
    {
        return $this->games;
    }

    public function getFinalThoughts(): ?string
    {
        return $this->finalThoughts;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function hasContents(): bool
    {
        return $this->hasContents;
    }
}
