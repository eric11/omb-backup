<?php

namespace OhMyBingo\Page\Attributes\Site;

class CommentForm
{
    private $data =  [];

    public function __construct($commentFormEntry) {
        if ($commentFormEntry) {
            $this->data = json_encode($commentFormEntry);
        }
    }

    public function getData(): ?array
    {
        return $this->data;
    }
}
