<?php

namespace OhMyBingo\Page\Attributes\Site;

class Site
{
    private $id;
    private $slug;
    private $name;
    private $affiliateLink;
    private $logo;
    private $welcomeOfferLine1;
    private $welcomeOfferLine2;
    private $termsAndConditions;
    private $termsAndConditionsUrl;
    public $software;
    public $network;
    public $freeBingo;
    public $noDeposit;
    public $recommended;
    public $featured;
    public $siteFeatures;
    public $availableGames;
    public $status;
    public $rating;

    public function __construct(
        string $id,
        string $slug,
        string $name,
        ?string $affiliateLink,
        ?array $logo,
        ?string $welcomeOfferLine1,
        ?string $welcomeOfferLine2,
        ?string $termsAndConditions,
        ?string $termsAndConditionsUrl,
        ?string $software,
        ?string $network,
        ?string $freeBingo,
        ?string $noDeposit,
        ?string $recommended,
        ?string $featured,
        ?string $siteFeatures,
        ?string $availableGames,
        ?string $status,
        ?int $rating
    ) {
        $this->id                    = $id;
        $this->slug                  = $slug;
        $this->name                  = $name;
        $this->affiliateLink         = $affiliateLink;
        $this->logo                  = $logo;
        $this->welcomeOfferLine1     = html_entity_decode($welcomeOfferLine1);
        $this->welcomeOfferLine2     = html_entity_decode($welcomeOfferLine2);
        $this->termsAndConditions    = html_entity_decode($termsAndConditions);
        $this->termsAndConditionsUrl = html_entity_decode($termsAndConditionsUrl);
        $this->software = $software;
        $this->network = $network;
        $this->freeBingo = $freeBingo;
        $this->noDeposit = $noDeposit;
        $this->recommended = $recommended;
        $this->featured = $featured;
        $this->siteFeatures = $siteFeatures;
        $this->availableGames = $availableGames;
        $this->status = $status;
        $this->rating = $rating;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAffiliateLink(): ?string
    {
        return $this->affiliateLink;
    }

    public function getLogo(): ?array
    {
        return $this->logo;
    }

    public function getWelcomeOfferLine1(): ?string
    {
        return $this->welcomeOfferLine1;
    }

    public function getWelcomeOfferLine2(): ?string
    {
        return $this->welcomeOfferLine2;
    }

    public function getTermsAndConditions(): ?string
    {
        return $this->termsAndConditions;
    }

    public function getTermsAndConditionsUrl(): ?string
    {
        return $this->termsAndConditionsUrl;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }
}
