<?php

namespace OhMyBingo\Page\Attributes\Site;

class KeyDetails
{
    private $noDepositOffer;
    private $welcomeBonus;
    private $redepositBonus;
    private $minimumDeposit;
    private $minimumWithdrawal;
    private $freeBingo;
    private $software;
    private $established;
    private $depositMethods = [];
    private $emailAddress;
    private $liveChat;
    private $phoneNumber;
    private $licenseNumber;
    private $hasDetails;

    public function __construct(
        ?string $noDepositOffer,
        ?string $welcomeBonus,
        ?string $redepositBonus,
        ?string $minimumDeposit,
        ?string $minimumWithdrawal,
        ?string $freeBingo,
        ?string $software,
        ?string $established,
        ?array $depositMethods,
        ?string $emailAddress,
        ?string $liveChat,
        ?string $phoneNumber,
        ?string $licenseNumber // Key Details - UK Gambling Commission License No.
    ) {
        $this->noDepositOffer    = html_entity_decode($noDepositOffer);
        $this->welcomeBonus      = html_entity_decode($welcomeBonus);
        $this->redepositBonus    = html_entity_decode($redepositBonus);
        $this->minimumDeposit    = html_entity_decode($minimumDeposit);
        $this->minimumWithdrawal = html_entity_decode($minimumWithdrawal);
        $this->freeBingo         = html_entity_decode($freeBingo);
        $this->software          = html_entity_decode($software);
        $this->established       = html_entity_decode($established);
        $this->depositMethods    = [];
        foreach ($depositMethods as $depositMethod) {
            array_push($this->depositMethods, html_entity_decode($depositMethod));
        }
        $this->emailAddress      = html_entity_decode($emailAddress);
        $this->liveChat          = html_entity_decode($liveChat);
        $this->phoneNumber       = html_entity_decode($phoneNumber);
        $this->licenseNumber     = $licenseNumber;
        $this->hasDetails = false;
        if (
            $noDepositOffer
            || $welcomeBonus
            || $redepositBonus
            || $minimumDeposit
            || $minimumWithdrawal
        ) {
            $this->hasDetails = true;
        }
    }

    public function getNoDepositOffer(): ?string
    {
        return $this->noDepositOffer;
    }

    public function getWelcomeBonus(): ?string
    {
        return $this->welcomeBonus;
    }

    public function getRedepositBonus(): ?string
    {
        return $this->redepositBonus;
    }

    public function getMinimumDeposit(): ?string
    {
        return $this->minimumDeposit;
    }

    public function getMinimumWithdrawal(): ?string
    {
        return $this->minimumWithdrawal;
    }

    public function getFreeBingo(): ?string
    {
        return $this->freeBingo;
    }

    public function getSoftware(): ?string
    {
        return $this->software;
    }

    public function getEstablished(): ?string
    {
        return $this->established;
    }

    public function getDepositMethods(): ?array
    {
        return $this->depositMethods;;
    }

    public function getEmailAddress(): ?string
    {
        return $this->emailAddress;
    }

    public function getLiveChat(): ?string
    {
        return $this->liveChat;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function getLicenseNumber(): ?string
    {
        return $this->licenseNumber;
    }

    public function hasDetails(): bool
    {
        return $this->hasDetails;
    }
}
