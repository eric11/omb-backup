<?php

namespace OhMyBingo\Page\Attributes;

class Meta
{
    private $title;
    private $description;
    private $keywords = [];

    public function __construct(?string $title, ?string $description, ?array $keywords = [])
    {
        $this->title       = str_replace('{date.month}', date('F'), $title);
        $this->description = $description;
        $this->keywords    = $keywords;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getKeywords(): ?array
    {
        return $this->keywords;
    }
}
