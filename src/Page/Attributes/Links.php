<?php

namespace OhMyBingo\Page\Attributes;

use Spatie\Url\Url;

class Links
{
    private $canonicalLink;
    private $alternateLink;
    private $ampLink;

    const AMP_SUFFIX = '/amp';
    const HOST = 'ohmybingo.com';

    public function __construct(string $canonicalLink, ?string $alternateLink = null)
    {
        if (substr($canonicalLink, 0, 1) !== '/') {
            $canonicalLink = ($canonicalLink !== 'home') ? '/' . $canonicalLink : '';
        }
        $canonicalLinkUrl = Url::fromString($canonicalLink);
        $trustedCanonicalLink = $canonicalLinkUrl->withScheme('https')->withHost(self::HOST)->__toString();
        $this->setCanonicalLink($trustedCanonicalLink);
        $this->alternateLink = $alternateLink;
        $this->ampLink       = $canonicalLink . self::AMP_SUFFIX;
    }

    public function setCanonicalLink(?string $canonicalLink): void
    {
        if ($canonicalLink === 'home') {
            $canonicalLink = '/';
        }
        $this->canonicalLink = $canonicalLink;
    }

    public function getCanonicalLink(): string
    {
        return $this->canonicalLink;
    }

    public function getAlternateLink(): ?string
    {
        return $this->alternateLink;
    }

    public function getAmpLink(): string
    {
        return $this->ampLink;
    }
}
