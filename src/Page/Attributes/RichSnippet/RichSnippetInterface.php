<?php

namespace OhMyBingo\Page\Attributes\RichSnippet;

interface RichSnippetInterface
{
    public function getJson(): string;
}
