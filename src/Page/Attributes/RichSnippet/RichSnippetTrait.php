<?php

namespace OhMyBingo\Page\Attributes\RichSnippet;

trait RichSnippetTrait
{
    private $richSnippet;

    public function setRichSnippet(RichSnippetInterface $richSnippet): void
    {
        $this->richSnippet = $richSnippet;
    }

    public function getRichSnippet(): ?string
    {
        if (empty($this->richSnippet)) {
            return null;
        }
        return $this->richSnippet->getJson();
    }
}
