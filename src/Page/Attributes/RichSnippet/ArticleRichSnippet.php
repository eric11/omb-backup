<?php

namespace OhMyBingo\Page\Attributes\RichSnippet;

use DateTime;

class ArticleRichSnippet implements RichSnippetInterface
{
    private $data = [];

    public function __construct(
        string $articleSection,
        string $headline,
        string $url,
        ?string $imageUrl,
        DateTime $datePublished,
        DateTime $dateCreated,
        DateTime $dateModified,
        string $articleBody,
        string $publisherName,
        string $publisherLogoUrl,
        string $mainEntityOfPage,
        string $genre,
        string $keywords
    ) {
        $wordCount = str_word_count($articleBody, 0);
        $this->data = [
            '@context'       => 'http://schema.org/',
            '@type'          => $articleSection,
            'headline'       => $headline,
            'alternativeHeadline' => '',
            'articleSection' => 'News',
            'url'            => $url,
            'genre'          => $genre,
            'keywords'       => $keywords,
            'wordcount'      => $wordCount,
            'datePublished'  => $datePublished->format('Y-m-d'),
            'dateCreated'    => $dateCreated->format('Y-m-d'),
            'dateModified'   => $dateModified->format('Y-m-d'),
            'publisher'  => [
                '@type' => 'Organization',
                'name'  => $publisherName,
                'logo'  => [
                    '@type' => 'imageObject',
                    'url'   => $publisherLogoUrl,
                ],
            ],
            'articleBody' => $articleBody,
            'mainEntityOfPage' => $mainEntityOfPage
        ];
        if ($imageUrl) {
            $this->data['image'] = $imageUrl;
        }
        if (!empty($imageUrl)) {
            $this->data['author']['logo'] = [
                '@type' => 'imageObject',
                'url'   => $imageUrl,
            ];
        }
    }

    public function getJson(): string
    {
        return json_encode($this->data, JSON_UNESCAPED_SLASHES);
    }

}
