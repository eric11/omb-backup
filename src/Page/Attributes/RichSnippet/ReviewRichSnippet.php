<?php

namespace OhMyBingo\Page\Attributes\RichSnippet;

use DateTime;

class ReviewRichSnippet implements RichSnippetInterface
{
    private $data = [];

    public function __construct(
        string $name,
        DateTime $datePublished,
        DateTime $dateModified,
        string $image,
        string $reviewBody,
        int $rating
    ) {
        $this->data = [
            '@context'      => 'http://schema.org/',
            '@type'         => 'Review',
            'datePublished' => $datePublished->format('Y-m-d'),
            'dateModified'  => $dateModified->format('Y-m-d'),
            'itemReviewed'  => [
                '@type' => 'Thing',
                'name'  => $name,
                'image' => $image,
            ],
            'author'  => [
                '@type' => 'Organization',
                'name'  => 'https://www.ohmybingo.com',
            ],
            'reviewBody' => $reviewBody,
            'reviewRating'  => [
                '@type' => 'Rating',
                'bestRating'  => '5',
                'worstRating' => '0',
                'ratingValue' => $rating
            ],
        ];
    }

    public function getJson(): string
    {
        return json_encode($this->data, JSON_UNESCAPED_SLASHES);
    }

}
