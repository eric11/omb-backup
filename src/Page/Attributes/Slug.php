<?php

namespace OhMyBingo\Page\Attributes;

class Slug
{
    private $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function get(): string
    {
        return $this->value;
    }
}
