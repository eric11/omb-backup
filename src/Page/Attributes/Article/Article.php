<?php

namespace OhMyBingo\Page\Attributes\Article;

use DateTime;
use OhMyBingo\Content\Image\ImageProcessor;
use OhMyBingo\Page\Attributes\Picture\PictureTrait;
use OhMyBingo\Page\Attributes\RichSnippet\RichSnippetTrait;

class Article
{
    use RichSnippetTrait;
    use PictureTrait;

    protected $slug;
    protected $title;
    protected $description;
    protected $copy;
    protected $date;

    public function __construct(string $slug, string $title, string $description, string $copy, $picture, string $date)
    {
        $this->slug        = $slug;
        $this->title       = $title;
        $this->copy        = $copy;
        if ($picture) {
            $this->picture = ImageProcessor::toImage($picture, 1200, 1200, 'png', 'pad');
        }
        $this->description = $description;
        $this->date        = (new DateTime($date))->format('d F y'); // convert 2019-03-21T00:00:00Z to 23 May 19
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getDescription(): ?string
    {
        return $this->copy;
    }

    public function getCopy(): ?string
    {
        return $this->copy;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }
}
