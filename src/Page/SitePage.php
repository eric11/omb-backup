<?php

namespace OhMyBingo\Page;

use OhMyBingo\Page\Attributes\Links;
use OhMyBingo\Page\Attributes\Site\CommentForm;
use OhMyBingo\Page\Attributes\Slug;
use OhMyBingo\Page\Attributes\Meta;
use OhMyBingo\Page\Attributes\Site\Site;
use OhMyBingo\Page\Attributes\Site\Review;
use OhMyBingo\Page\Attributes\Site\KeyDetails;
use OhMyBingo\Page\Attributes\Site\Comments;

class SitePage extends AbstractPage implements PageInterface
{
    const CONTENT_TYPE = 'site';

    private $site;
    private $review;
    private $keyDetails;
    private $comments;
    private $commentForm;

    public function __construct(
        Slug $slug,
        Meta $meta,
        Links $links,
        Site $site,
        Review $review,
        KeyDetails $keyDetails,
        Comments $comments,
        CommentForm $commentForm
    ) {
        parent::__construct($slug, $meta, $links);
        $this->site         = $site;
        $this->review      = $review;
        $this->keyDetails  = $keyDetails;
        $this->comments    = $comments;
        $this->commentForm = $commentForm;

    }

    public function getSite(): Site
    {
        return $this->site;
    }

    public function getReview(): Review
    {
        return $this->review;
    }

    public function getKeyDetails(): KeyDetails
    {
        return $this->keyDetails;
    }

    public function getComments(): Comments
    {
        return $this->comments;
    }
}
