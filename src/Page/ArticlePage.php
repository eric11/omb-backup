<?php

namespace OhMyBingo\Page;

use OhMyBingo\Page\Attributes\Links;
use OhMyBingo\Page\Attributes\Slug;
use OhMyBingo\Page\Attributes\Meta;
use OhMyBingo\Page\Attributes\Article\Article;
use OhMyBingo\Page\Component\ArticleListings\ArticleListings;
use OhMyBingo\Page\Traits\DocumentTrait;

class ArticlePage extends AbstractPage implements PageInterface
{
    use DocumentTrait;

    const CONTENT_TYPE = 'article';
    const ROUTE = 'bingo-articles';
    const ROUTE_NAME = 'articles';
    const PUBLISHER_NAME = 'OhMyBingo';
    const PUBLISHER_LOGO_URL = 'http://127.0.0.1:8000/img/logo/logo@2x.png';

    protected $article;
    protected $articleListings;

    public function __construct(
        Slug $slug,
        Meta $meta,
        Links $links,
        Article $article,
        ArticleListings $articleListings
    ) {
        parent::__construct($slug, $meta, $links);
        $this->article = $article;
        $this->articleListings = $articleListings;
    }

    public function getArticle(): Article
    {
        return $this->article;
    }

    public function getArticleListings(): ?ArticleListings
    {
        return $this->articleListings;
    }
}
