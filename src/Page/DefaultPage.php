<?php

namespace OhMyBingo\Page;

use OhMyBingo\Page\Attributes\Links;
use OhMyBingo\Page\Attributes\Slug;
use OhMyBingo\Page\Attributes\Meta;
use OhMyBingo\Page\Attributes\Article;
use OhMyBingo\Page\Component\SiteListings\SiteListings;
use OhMyBingo\Page\Component\NewsListings\NewsListings;
use OhMyBingo\Page\Component\ArticleListings\ArticleListings;
use OhMyBingo\Page\Component\SiteListings\SiteListingsTrait;
use OhMyBingo\Page\Traits\DocumentTrait;

class DefaultPage extends AbstractPage implements PageInterface
{
    use DocumentTrait;
    use SiteListingsTrait;

    const CONTENT_TYPE = 'page'; // Does not exist in content management system | defaultPage.virtual

    private $newsListings;
    private $articleListings;

    public function __construct(
        Slug $slug,
        Meta $meta,
        Links $links,
        ?string $navigationText = null,
        ?Article $article = null,
        SiteListings $siteListings = null,
        NewsListings $newsListings = null,
        ArticleListings $articleListings = null
    ) {
        parent::__construct($slug, $meta, $links, $navigationText);
        $this->article      = $article;
        $this->siteListings = $siteListings;
        $this->newsListings = $newsListings;
        $this->articleListings = $articleListings;
    }

    public function getNewsListings(): ?NewsListings
    {
        return $this->newsListings;
    }

    public function getArticleListings(): ?ArticleListings
    {
        return $this->articleListings;
    }
}
