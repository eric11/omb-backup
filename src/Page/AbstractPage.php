<?php

namespace OhMyBingo\Page;

use OhMyBingo\Page\Attributes\Links;
use OhMyBingo\Page\Attributes\Slug;
use OhMyBingo\Page\Attributes\Meta;
use OhMyBingo\Page\Component\ComponentInterface;
use OhMyBingo\Page\Component\ComponentCollection;

abstract class AbstractPage implements PageInterface
{
    private $slug;
    private $meta;
    private $links;
    private $components;
    private $modal;
    private $navigationText;

    public function __construct(Slug $slug, Meta $meta, ?Links $links = null, ?string $navigationText = '') // ?ComponentCollection $components = null
    {
        $this->slug     = $slug;
        $this->meta     = $meta;
        $this->setLinks($links, $slug);
        $this->navigationText = $navigationText;
    }

    public function setLinks(Links $links, Slug $slug): void
    {
        $this->links = (!$links->getCanonicalLink() && !$links->getAlternateLink()) ? new Links($slug->get()) : $links;
    }

    public function getSlug(): Slug
    {
        return $this->slug;
    }

    public function getMeta(): Meta
    {
        return $this->meta;
    }

    public function getLinks(): Links
    {
        return $this->links;
    }

    public function getNavigationText(): string
    {
        return $this->navigationText;
    }

    public function setComponents(ComponentCollection $components): void
    {
        $this->components = $components;
    }

    public function getComponent($name): ?ComponentInterface
    {
        return $this->components->get($name);
    }

    public function setModal(ComponentInterface $modal): void
    {
        $this->modal = $modal;
    }

    public function getModal(): ?ComponentInterface
    {
        return $this->modal;
    }
}
