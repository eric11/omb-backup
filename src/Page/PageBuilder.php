<?php

namespace OhMyBingo\Page;

use OhMyBingo\Page\Attributes\Links;
use OhMyBingo\Page\Attributes\RichSnippet\ArticleRichSnippet;
use OhMyBingo\Page\Attributes\RichSnippet\NewsArticleRichSnippet;
use OhMyBingo\Page\Attributes\RichSnippet\ReviewRichSnippet;
use OhMyBingo\Page\Attributes\Site\CommentForm;
use OhMyBingo\Page\Attributes\Slug;
use OhMyBingo\Page\Attributes\Meta;
use OhMyBingo\Page\Component\SiteListings\SiteListings;
use OhMyBingo\Page\Component\NewsListings\NewsListings;
use OhMyBingo\Page\Component\ArticleListings\ArticleListings;
use OhMyBingo\Page\Attributes\Article as PageArticle;
use OhMyBingo\Page\Attributes\Article\Article;
use OhMyBingo\Page\Attributes\News\NewsArticle;
use OhMyBingo\Page\Attributes\Site\Site;
use OhMyBingo\Page\Attributes\Site\Review as SiteReview;
use OhMyBingo\Page\Attributes\Site\KeyDetails as SiteKeyDetails;
use OhMyBingo\Page\Attributes\Site\Comments as SiteComments;
use Contentful\Core\Resource\ResourceInterface;
use Contentful\RichText\Renderer;
use OhMyBingo\Content\Image\ImageProcessor;
use OhMyBingo\Content\Renderer\AssetNodeRenderer;
use OhMyBingo\Content\Renderer\ListNodeRenderer;
use OhMyBingo\Page\Component\SiteModal\SiteModal;
use OhMyBingo\Page\Component\Button\Button;
use OhMyBingo\Repository\CommentRepository;
use Symfony\Component\Debug\Exception\FlattenException;
use DateTime;
use Stringy\Stringy as S;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\RequestContext;

class PageBuilder
{
    private $contentRenderer;
    private $commentRepository;

    public function __construct(CommentRepository $commentRepository)
    {
        $this->contentRenderer = new Renderer();
        $this->commentRepository = $commentRepository;
    }

    public function buildFromPage(string $path, ResourceInterface $pageEntry, $siteListingEntries, $newsListingEntries, $articleListingEntries): PageInterface
    {
        $pageSlug     = new Slug($pageEntry->getSlug());
        $pageMeta     = new Meta($pageEntry->getMetaTitle(), $pageEntry->getMetaDescription(), $pageEntry->getMetaKeywords());
        $pageCopy     = (!empty($pageEntry->getCopy()) ? $this->contentRenderer->render($pageEntry->getCopy()) : null);
        $article      = new PageArticle($pageEntry->getTitle(), $pageCopy);
        $siteListings = new SiteListings($path, $siteListingEntries);
        $newsListings = new NewsListings($path, $newsListingEntries);
        $articleListings = new ArticleListings($path, $articleListingEntries);
        $pageLinks    = new Links($pageEntry->getLinkCanonical() ?? $path);
        return new DefaultPage($pageSlug, $pageMeta, $pageLinks, $pageEntry->getNavigationText(), $article, $siteListings, $newsListings, $articleListings);
    }

    public function buildFromLegalPage(ResourceInterface $pageEntry): PageInterface
    {
        $pageSlug     = new Slug($pageEntry->getSlug());
        $pageMeta     = new Meta($pageEntry->getMetaTitle(), $pageEntry->getMetaDescription());
        $pageCopy     = (!empty($pageEntry->getCopy()) ? $this->contentRenderer->render($pageEntry->getCopy()) : null);
        $article      = new PageArticle($pageEntry->getTitle(), $pageCopy);
        $pageLinks    = new Links($pageEntry->getLinkCanonical() ?? $pageEntry->getSlug());
        return new LegalPage($pageSlug, $pageMeta, $pageLinks, $article);
    }

    public function buildFromExceptionPage(ResourceInterface $pageEntry, FlattenException $exception): PageInterface
    {
        $pageSlug     = new Slug($pageEntry->getSlug());
        $pageMeta     = new Meta($pageEntry->getMetaTitle(), $pageEntry->getMetaDescription());
        $pageCopy     = (!empty($pageEntry->getCopy()) ? $this->contentRenderer->render($pageEntry->getCopy()) : null);
        $article      = new PageArticle($pageEntry->getTitle(), $pageCopy);
        return new ExceptionPage($pageSlug, $pageMeta,  $article, $exception);
    }

    public function buildFromSite(ResourceInterface $siteEntry, ?ResourceInterface $commentFormEntry): PageInterface
    {
        /** @var Slug $pageSlug */
        $pageSlug = new Slug($siteEntry->getSlug());

        /** @var Meta $pageMeta */
        $pageMeta = new Meta($siteEntry->getMetaTitle(), $siteEntry->getMetaDescription(), $siteEntry->getMetaKeywords());

        /** @var Site $site */
        $site = new Site(
            $siteEntry->getSystemProperties()->getId(),
            $siteEntry->getSlug(),
            $siteEntry->getName(),
            $siteEntry->getAffiliateLink(),
            ImageProcessor::toImage($siteEntry->getLogo(), 255, 131, 'png', 'pad'),
            $siteEntry->getWelcomeOfferLine1(),
            $siteEntry->getWelcomeOfferLine2(),
            $siteEntry->getTermsAndConditions(),
            $siteEntry->getTermsAndConditionsUrl(),
            $siteEntry->getSoftware(),
            $siteEntry->getNetwork(),
            $siteEntry->getFreeBingo(),
            $siteEntry->getNoDeposit(),
            $siteEntry->getRecommended(),
            $siteEntry->getFeatured(),
            $siteEntry->getSiteFeatures(),
            $siteEntry->getAvailableGames(),
            $siteEntry->getStatus(),
            $siteEntry->getRating()
        );

        /** @var SiteReview $siteReview */
        $siteReview = new SiteReview(
            !empty($siteEntry->getReviewTitle()) ? $siteEntry->getReviewTitle() : 'Our review of ' . $siteEntry->getName(),
            $siteEntry->getReviewIntroduction(),
            $siteEntry->getReviewWelcomeOffer(),
            $siteEntry->getReviewPromotions(),
            $siteEntry->getReviewGames(),
            $siteEntry->getReviewFinalThoughts(),
            $siteEntry->getReviewDate()
        );

        /** @var SiteKeyDetails $siteKeyDetails */
        $siteKeyDetails = new SiteKeyDetails(
            $siteEntry->getKeyDetailsNoDepositOffer(),
            $siteEntry->getKeyDetailsWelcomeBonus(),
            $siteEntry->getKeyDetailsRedepositBonus(),
            $siteEntry->getKeyDetailsMinimumDeposit(),
            $siteEntry->getKeyDetailsMinimumWithdrawal(),
            $siteEntry->getKeyDetailsFreeBingo(),
            $siteEntry->getKeyDetailsSoftware(),
            $siteEntry->getKeyDetailsEstablished(),
            $siteEntry->getKeyDetailsDepositMethods(),
            $siteEntry->getKeyDetailsEmailAddress(),
            $siteEntry->getKeyDetailsLiveChat(),
            $siteEntry->getKeyDetailsPhoneNumber(),
            $siteEntry->getKeyDetailsLicenseNumber()
        );

        /** @var RichSnippet $googleRichSnippet */
        if (!empty($site->getLogo()) && array_key_exists('1x', $site->getLogo())) {
            $richSnippetImage = $site->getLogo()['1x'];
        } else {
            $richSnippetImage = null;
        }
        $richSnippetReviewBody = S::create($siteReview->getIntroduction())->safeTruncate(160, '...');
        $richSnippetReviewBody = strip_tags($richSnippetReviewBody);
        $richSnippetReviewBody = trim($richSnippetReviewBody);
        if (!empty($richSnippetImage) && !empty($siteReview->getIntroduction()) && !empty($site->getRating())) {
            $richSnippet = new ReviewRichSnippet(
                $site->getName(),
                (new DateTime($siteReview->getDate())),
                (new DateTime($siteEntry->getSystemProperties()->getUpdatedAt())),
                $richSnippetImage,
                $richSnippetReviewBody,
                (int) $site->getRating()
            );
            $siteReview->setRichSnippet($richSnippet);
        }

        /** @var SiteComments $siteComments */
        $comments = $this->commentRepository->findBySite($siteEntry->getSystemProperties()->getId());
        $siteComments = new SiteComments($comments);
        $commentForm = new CommentForm($commentFormEntry);
        $pageLinks = new Links($siteEntry->getLinkCanonical() ?? $site->getSlug()->get());
        return new SitePage($pageSlug, $pageMeta, $pageLinks, $site, $siteReview, $siteKeyDetails, $siteComments, $commentForm);
    }

    public function buildFromArticle(ResourceInterface $articleEntry): PageInterface
    {
        /** @var Slug $pageSlug */
        $pageSlug = new Slug($articleEntry->getSlug());
        /** @var Meta $pageMeta */
        $pageMeta = new Meta($articleEntry->getMetaTitle(), $articleEntry->getMetaDescription(), $articleEntry->getMetaKeywords());

        $articleContentRenderer = new Renderer();
        $articleContentRenderer->pushNodeRenderer(new AssetNodeRenderer());
        $articleContentRenderer->pushNodeRenderer(new ListNodeRenderer());

        $articleCopy = (!empty($articleEntry->getCopy()) ? $articleContentRenderer->render($articleEntry->getCopy()) : null);

        /** @var Article $article */
        $article = new Article(
            $articleEntry->getSlug(),
            $articleEntry->getTitle(),
            $articleEntry->getDescription(),
            $articleCopy,
            $articleEntry->getPicture(),
            $articleEntry->getDate()
        );

        /** @var string $newsArticleUrl */
        $articleUrl = ArticlePage::ROUTE . '/' . $articleEntry->getSlug();

        /** @var ArticleRichSnippet $googleRichSnippet */
        if (!empty($articleCopy)) {
            $articleBody = S::create($articleCopy)->safeTruncate(160, '...');
            $articleBody = strip_tags($articleBody);
            $articleBody = trim($articleBody);
            $mainEntityOfPage = 'http://ohmybingo.com';
            $newsArticleGenre = 'Bingo, Slot, and Casino sites';
            $newsArticleKeywords = 'bingo sites, slots sites, casino sites';
            $richSnippet = new ArticleRichSnippet(
                'Articles',
                $articleEntry->getTitle(),
                $articleUrl,
                $article->getPictureUrl(),
                (new DateTime($articleEntry->getDate())),
                (new DateTime($articleEntry->getSystemProperties()->getCreatedAt())),
                (new DateTime($articleEntry->getSystemProperties()->getUpdatedAt())),
                $articleBody,
                NewsArticlePage::PUBLISHER_NAME,
                NewsArticlePage::PUBLISHER_LOGO_URL,
                $mainEntityOfPage,
                $newsArticleGenre,
                $newsArticleKeywords
            );
            $article->setRichSnippet($richSnippet);
        }

        $pageLinks = new Links(ArticlePage::ROUTE . '/' . $pageSlug->get());
        return new ArticlePage($pageSlug, $pageMeta, $pageLinks, $article, new ArticleListings($pageSlug->get(), $article));
    }

    public function buildFromNewsArticle(ResourceInterface $newsArticleEntry): PageInterface
    {
        /** @var Slug $pageSlug */
        $pageSlug = new Slug($newsArticleEntry->getSlug());
        /** @var Meta $pageMeta */
        $pageMeta = new Meta($newsArticleEntry->getMetaTitle(), $newsArticleEntry->getMetaDescription(), $newsArticleEntry->getMetaKeywords());

        $newsContentRenderer = new Renderer();
        $newsContentRenderer->pushNodeRenderer(new AssetNodeRenderer());
        $newsContentRenderer->pushNodeRenderer(new ListNodeRenderer());
        $newsArticleCopy = (!empty($newsArticleEntry->getCopy()) ? $newsContentRenderer->render($newsArticleEntry->getCopy()) : null);

        /** @var NewsArticle $article */
        $newsArticle = new NewsArticle(
            $newsArticleEntry->getSlug(),
            $newsArticleEntry->getTitle(),
            $newsArticleCopy,
            $newsArticleEntry->getPicture(),
            $newsArticleEntry->getSite(),
            $newsArticleEntry->getDate()
        );

        /** @var string $newsArticleUrl */
        $newsArticleUrl = NewsArticlePage::ROUTE . '/' . $newsArticleEntry->getSlug();

        /** @var NewsArticleRichSnippet $googleRichSnippet */
        if (!empty($newsArticleCopy)) {
            $newsArticleBody = S::create($newsArticleCopy)->safeTruncate(160, '...');
            $newsArticleBody = strip_tags($newsArticleBody);
            $newsArticleBody = trim($newsArticleBody);
            $mainEntityOfPage = 'http://ohmybingo.com';
            $newsArticleGenre = 'Bingo, Slot, and Casino sites';
            $newsArticleKeywords = 'bingo sites, slots sites, casino sites';
            $richSnippet = new ArticleRichSnippet(
                'News',
                $newsArticleEntry->getTitle(),
                $newsArticleUrl,
                $newsArticle->getPictureUrl(),
                (new DateTime($newsArticleEntry->getDate())),
                (new DateTime($newsArticleEntry->getSystemProperties()->getCreatedAt())),
                (new DateTime($newsArticleEntry->getSystemProperties()->getUpdatedAt())),
                $newsArticleBody,
                NewsArticlePage::PUBLISHER_NAME,
                NewsArticlePage::PUBLISHER_LOGO_URL,
                $mainEntityOfPage,
                $newsArticleGenre,
                $newsArticleKeywords
            );
            $newsArticle->setRichSnippet($richSnippet);
        }

        $pageLinks = new Links('https://ohmybingo.com/'.$newsArticleUrl);
        return new NewsArticlePage($pageSlug, $pageMeta, $pageLinks, $newsArticle);
    }
}
