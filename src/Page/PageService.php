<?php

namespace OhMyBingo\Page;

use FirstLeads\Infrastructure\Cache\CacheKey\PageCacheKey;
use OhMyBingo\Content\ContentService;
use OhMyBingo\Page\Attributes\Site\CommentForm;
use OhMyBingo\Page\Component\ComponentCollection;
use OhMyBingo\Page\Component\ComponentService;
use OhMyBingo\Content\Query\Query;
use OhMyBingo\Page\Component\SiteModal\SiteModal;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Contentful\Core\Resource\ResourceInterface;
use FirstLeads\Infrastructure\Cache\CacheService;
use FirstLeads\Infrastructure\Cache\CacheKey\CacheKey;
use FirstLeads\Infrastructure\Cache\Exception\CacheNotFoundException;
use OhMyBingo\Search\SearchService;
use Exception;
use RuntimeException;
use Contentful\Delivery\Resource\Entry;

class PageService
{
    protected $contentService;
    protected $pageBuilder;
    protected $componentService;
    protected $cacheService;
    protected $searchService;

    public function __construct(ContentService $contentService, PageBuilder $pageBuilder, ComponentService $componentService, CacheService $cacheService, SearchService $searchService)
    {
        $this->contentService   = $contentService;
        $this->pageBuilder      = $pageBuilder;
        $this->componentService = $componentService;
        $this->cacheService     = $cacheService;
        $this->searchService    = $searchService;
    }

    public function getLegalPage(string $path)
    {
        /** @var PageCacheKey $cacheKey */
        $cacheKey = new PageCacheKey(LegalPage::CONTENT_TYPE, $path);

        try {
            $page = $this->cacheService->get($cacheKey);
        } catch (CacheNotFoundException $exception) {
            /** @var Entry $pageEntry */
            $legalPageEntry = $this->contentService->getEntry(Query::get(LegalPage::CONTENT_TYPE, $path));
            if (!$legalPageEntry) {
                throw new NotFoundHttpException(sprintf('No page found matching path: %s', $path));
            }
            /** @var ExceptionPage $page */
            $page = $this->pageBuilder->buildFromLegalPage($legalPageEntry);
            $this->cacheService->set($cacheKey, $page);
        }
        /** @var ComponentCollection $pageComponents */
        $pageComponents = $this->componentService->get($path);
        $page->setComponents($pageComponents);
        return $page;
    }

    public function getExceptionPage(FlattenException $exception): ExceptionPage
    {
        /** @var string $path */
        $path = 'error-'.$exception->getStatusCode();

        /** @var Entry $pageEntry */
        $exceptionPageEntry = $this->contentService->getEntry(Query::get(ExceptionPage::CONTENT_TYPE, $path));
        if (!$exceptionPageEntry) {
            throw new NotFoundHttpException(sprintf('No page found matching path: %s', $path));
        }

        /** @var ExceptionPage $page */
        $page = $this->pageBuilder->buildFromExceptionPage($exceptionPageEntry, $exception);

        /** @var ComponentCollection $pageComponents */
        $pageComponents = $this->componentService->get($path);
        $page->setComponents($pageComponents);
        return $page;
    }

    public function get(string $path, string $entrySlug = null, string $contentType = 'page')
    {
        /** @var PageInterface $page */
        $page = null;
        # Page
        if ($contentType === 'page') {
            /** @var CacheKey $cacheKey */
            $cacheKey = new PageCacheKey($contentType, $path);
//            dd($cacheKey);
            try {
                $page = $this->cacheService->get($cacheKey);
                throw new CacheNotFoundException('nocache');
            } catch (CacheNotFoundException $exception) {
                /** @var Entry $pageEntry */
                $pageEntry = $this->contentService->getEntry(Query::get($contentType, $path));

                if (!$pageEntry) {
                    throw new NotFoundHttpException(sprintf('No page found matching path: %s', $path));
                }
                // Search for site listing entries
                if ($path === 'news') {
                    $newsListingEntries = $this->searchService->search('news', 'news')->getEntries();
                } else {
                    $newsListingEntries = [];
                }
//                dd($newsListingEntries);
                if ($path === 'articles') {
                    $articleListingEntries = $this->searchService->search('article', 'article')->getEntries();
                } else{
                    $articleListingEntries = [];
                }
                $siteListingEntries = $this->fetchSiteListingEntries($pageEntry);
//                if (strpos($url, 'Dragonfish') == false) {
//                    $siteListingEntries = $this->searchService->search('site', 'site', $contentType, 'Dragonsi')->getEntries();
//                }

//                dd($siteListingEntries);
                /** @var AbstractPage $page */
                $page = $this->pageBuilder->buildFromPage($path, $pageEntry, $siteListingEntries, $newsListingEntries, $articleListingEntries);
                if (!empty($siteListingEntries)) {
                    $page = $this->fetchAndSetModal($page, 'no-affiliation-bingo');
                }

                $this->cacheService->set($cacheKey, $page);
            }
        # Site
        } elseif ($contentType === 'site') {
            /** @var CacheKey $cacheKey */
            $cacheKey = new PageCacheKey($contentType, $entrySlug);
            try {
                $page = $this->cacheService->get($cacheKey);
            } catch (CacheNotFoundException $exception) {

                /** @var ResourceInterface $siteEntry */
                $siteEntry = $this->contentService->getEntry(Query::get($contentType, $entrySlug));
                if (!$siteEntry) {
                    throw new NotFoundHttpException(sprintf('No site found matching slug: %s', $entrySlug));
                }

                /** CommentForm */
                $commentFormEntry = null;//$this->contentService->getEntry(Query::get('widget', 'comment-form'));

                /** @var AbstractPage $page */
                $page = $this->pageBuilder->buildFromSite($siteEntry, $commentFormEntry);

                if ($page->getSite()->status === 'Closed') {
                    $page = $this->fetchAndSetModal($page, 'site-closed');
                }
            }
        # Article
        } elseif ($contentType === 'article') {
            /** @var CacheKey $cacheKey */
            $cacheKey = new PageCacheKey($contentType, $entrySlug);
            try {
                $page = $this->cacheService->get($cacheKey);
            } catch (CacheNotFoundException $exception) {
                /** @var ResourceInterface $articleEntry */
                $articleEntry = $this->contentService->getEntry(Query::get($contentType, $entrySlug));
                if (!$articleEntry) {
                    throw new NotFoundHttpException(sprintf('No article found matching slug: %s', $entrySlug));
                }
                /** @var AbstractPage $page */
                $page = $this->pageBuilder->buildFromArticle($articleEntry);
            }
        # News
        } elseif ($contentType === 'news') {
            /** @var CacheKey $cacheKey */
            $cacheKey = new PageCacheKey($contentType, $entrySlug);
            try {
                $page = $this->cacheService->get($cacheKey);
            } catch (CacheNotFoundException $exception) {
                /** @var ResourceInterface $newsEntry */
                $newsEntry = $this->contentService->getEntry(Query::get($contentType, $entrySlug));
                if (!$newsEntry) {
                    throw new NotFoundHttpException(sprintf('No news articles found matching slug: %s', $entrySlug));
                }
                /** @var AbstractPage $page */
                $page = $this->pageBuilder->buildFromNewsArticle($newsEntry);
            }
        }
        /** @var ComponentCollection $pageComponents */
        $pageComponents = $this->componentService->get($path);
        $page->setComponents($pageComponents);
        return $page;
    }

    private function fetchSiteListingEntries(Entry $page)
    {
        /** @var string $siteListingsConfig */
        $siteListingsConfig = $page->getSiteListingsConfig();
        /*
        if ($page->getSiteListings()) {
            $siteListingEntries = $page->getSiteListings();
        } else
        */
        if ($siteListingsConfig) {
            // $siteListingEntries = $page->getSiteListings();
            if (!is_string($siteListingsConfig)) {
                throw new RuntimeException(sprintf('Invalid site listings configuration provided:', $siteListingsConfig));
            }

            //
            if (strpos($siteListingsConfig, '&') !== false) {
                $siteListingConfigParts = explode('&', $siteListingsConfig);
                $configContentTypes = [];
                $configValues = [];
                foreach ($siteListingConfigParts as $siteListingConfigPart) {
                    $configParts = explode(':', $siteListingConfigPart);
                    if (empty($configParts[0]) || empty($configParts[1])) {
                        throw new RuntimeException(sprintf('Invalid site listings configuration provided:', $siteListingConfigPart));
                    }
                    $configContentTypes[] = $configParts[0];
                    $configValues[] = $configParts[1];
                }
                $siteListingEntries = $this->searchService->search('site', 'site', $configContentTypes, $configValues)->getEntries();
            } else {
                $configParts = explode(':', $siteListingsConfig);
                if (empty($configParts[0]) || empty($configParts[1])) {
                    throw new RuntimeException(sprintf('Invalid site listings configuration provided:', $siteListingsConfig));
                }
                $configContentType = $configParts[0];
                $configValue = $configParts[1];
                $siteListingEntries = $this->searchService->search('site', 'site', $configContentType, $configValue)->getEntries();
            }



//            $configValue = '5IIvkZMrPdSB39cSrRw2yd'; // if a software/network link..



//            $siteListingEntries = $this->searchService->search('site', 'site', 'software.fields.name', '5IIvkZMrPdSB39cSrRw2yd')->getEntries();
//            dd($siteListingEntries);
//            fields.software.fields.name/**/[all]=Dragonfih
        } else {
            $siteListingEntries = [];
        }
        return $siteListingEntries;
    }


    public function fetchAndSetModal(PageInterface $page, $siteModalSlug = 'no-affiliation-bingo')
    {
        /** @var Entry $pageEntry */
        $siteModalEntry = $this->contentService->getEntry(Query::get(SiteModal::CONTENT_TYPE, $siteModalSlug));
        if (!$siteModalEntry) {
            throw new NotFoundHttpException(sprintf('No site modal found matching slug: %s', $siteModalSlug));
        }
        $siteModal = new SiteModal($siteModalEntry);
        $page->setModal($siteModal);
        return $page;
    }
}
