<?php

namespace OhMyBingo\Page\Component\QuickLinks;

use Contentful\RichText\Renderer;
use OhMyBingo\Page\Component\ComponentInterface;
use OhMyBingo\Content\Image\ImageProcessor;

class QuickLinks implements ComponentInterface
{
    const CONTENT_TYPE = 'quickLink';

    private $quickLinks = [];

    public function __construct(array $entries)
    {
        $contentRenderer = new Renderer();
        foreach ($entries as $entry) {
            if (empty($entry->copy)) {
                $entryCopy = $entry->name;
            } else {
                $entryCopy = $contentRenderer->render($entry->copy);
                if (strpos($entryCopy, '<p>') !== false) {
                    $entryCopy = str_replace('</p>', '<br/>', $entryCopy);
                    $entryCopy = str_replace('<p>', '', $entryCopy);
                    $entryCopy = str_replace('</p>', '', $entryCopy);
                    $entryCopy = rtrim($entryCopy, '<br/>');
                }
            }
//            dd($entry->icon);
            array_push(
                $this->quickLinks,
                (object) [
                    'name' => $entry->name,
                    'copy' => $entryCopy,
                    'link'  => $entry->page->slug,
                    'icon'  => ImageProcessor::toImage($entry->icon, 90, 90, 'png', 'pad'),
                ]
            );
        }
        $this->quickLinks;
    }

    public function getQuickLinks()
    {
        return $this->quickLinks;
    }
}
