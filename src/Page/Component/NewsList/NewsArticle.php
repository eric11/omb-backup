<?php

namespace OhMyBingo\Page\Component\NewsList;

class NewsArticle
{
    private $slug;
    private $title;
    private $date;

    public function __construct(string $slug, string $title, ?string $date)
    {
        $this->slug  = $slug;
        $this->title = $title;
        $this->date  = $date;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }
}
