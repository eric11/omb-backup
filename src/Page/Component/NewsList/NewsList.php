<?php

namespace OhMyBingo\Page\Component\NewsList;

use OhMyBingo\Page\Component\ComponentInterface;
use DateTime;

class NewsList implements ComponentInterface
{
    const CONTENT_TYPE = 'newsList';

    private $title;
    private $newsArticles = [];
    private $viewAllButtonText;

    public function __construct($newsListEntry)
    {
        $this->title = $newsListEntry->title;
        $this->newsArticles = [];
        foreach ($newsListEntry->newsArticles as $newsArticleEntry) {
            array_push(
                $this->newsArticles,
                new NewsArticle(
                    $newsArticleEntry->slug,
                    $newsArticleEntry->title,
                    (new DateTime($newsArticleEntry->date))->format('d F y') // convert 2019-03-21T00:00:00Z to 23 May 19
                )
            );
        }
        $this->viewAllButtonText = $newsListEntry->viewAllButtonText;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getNewsArticles(): array
    {
        return $this->newsArticles;
    }

    public function getViewAllButtonText(): string
    {
        return $this->viewAllButtonText;
    }
}
