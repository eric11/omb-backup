<?php

namespace OhMyBingo\Page\Component\SiteModal;

class Site
{
    private $slug;
    private $name;
    private $affiliateLink;
    private $logo;
    private $rating;
    private $welcomeOfferLine1;
    private $welcomeOfferLine2;

    public function __construct(string $slug, string $name, ?string $affiliateLink, ?array $logo, int $rating, ?string $welcomeOfferLine1, ?string $welcomeOfferLine2)
    {
        $this->slug              = $slug;
        $this->name              = $name;
        $this->affiliateLink     = $affiliateLink;
        $this->logo              = $logo;
        $this->rating            = $rating;
        $this->welcomeOfferLine1 = $welcomeOfferLine1;
        $this->welcomeOfferLine2 = $welcomeOfferLine2;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAffiliateLink(): ?string
    {
        return $this->affiliateLink;
    }

    public function getLogo(): ?array
    {
        return $this->logo;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function getWelcomeOfferLine1(): ?string
    {
        return $this->welcomeOfferLine1;
    }

    public function getWelcomeOfferLine2(): ?string
    {
        return $this->welcomeOfferLine2;
    }
}
