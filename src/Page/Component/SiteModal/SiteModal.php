<?php

namespace OhMyBingo\Page\Component\SiteModal;

use Contentful\RichText\Renderer;
use OhMyBingo\Page\Component\Button\Button;
use OhMyBingo\Page\Component\ComponentInterface;
use OhMyBingo\Content\Image\ImageProcessor;

class SiteModal implements ComponentInterface
{
    const CONTENT_TYPE = 'modal';

    private $name;
    private $copy;
    private $sites = [];
    private $playButton;
    private $goButton;
    private $cancelButton;

    public function __construct($siteModalEntry)
    {
        $contentRenderer = new Renderer();
        $this->name = $siteModalEntry->name;
        $this->copy = (!empty($siteModalEntry->getCopy()) ? $contentRenderer->render($siteModalEntry->getCopy()) : null);
        $this->sites = [];
        foreach ($siteModalEntry->sites as $siteEntry) {
            if (empty($siteEntry->affiliateLink)) {
                continue;
            }
            array_push(
                $this->sites,
                new Site(
                    $siteEntry->slug,
                    $siteEntry->name,
                    $siteEntry->affiliateLink,
                    ImageProcessor::toImage($siteEntry->logo, 145, null, 'png', 'pad'),
                    $siteEntry->rating,
                    $siteEntry->welcomeOfferLine1,
                    $siteEntry->welcomeOfferLine2
                )
            );
        }
        $this->playButton   = new Button($siteModalEntry->playButton);
        $this->goButton     = new Button($siteModalEntry->goButton);
        if ($siteModalEntry->cancelButton) {
            $this->cancelButton = new Button($siteModalEntry->cancelButton);
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCopy(): string
    {
        return $this->copy;
    }

    public function getSites(): array
    {
        return $this->sites;
    }

    public function getPlayButton()
    {
        return $this->playButton;
    }

    public function getGoButton()
    {
        return $this->goButton;
    }

    public function getCancelButton()
    {
        return $this->cancelButton;
    }
}
