<?php

namespace OhMyBingo\Page\Component\SiteListings;

use OhMyBingo\Page\Attributes\Slug;
use OhMyBingo\Page\Attributes\Meta;

class SiteListing
{
    private $name;
    private $logo;
    private $welcomeOfferLine1;
    private $welcomeOfferLine2;
    private $termsAndConditions;
    private $termsAndConditionsUrl;
    private $affiliateLink;
    private $label;
    private $status;
    private $featured;
    private $recommended;
    private $slug;

    public function __construct(
        string $name,
        array $logo,
        ?string $welcomeOfferLine1,
        ?string $welcomeOfferLine2,
        ?string $termsAndConditions,
        ?string $termsAndConditionsUrl,
        ?string $affiliateLink,
        ?string $label,
        ?string $status,
        ?string $featured,
        ?string $recommended,
        string $slug
    ) {
        $this->name                  = $name;
        $this->logo                  = $logo;
        $this->welcomeOfferLine1     = $welcomeOfferLine1;
        $this->welcomeOfferLine2     = $welcomeOfferLine2;
        $this->termsAndConditions    = $termsAndConditions;
        $this->termsAndConditionsUrl = $termsAndConditionsUrl;
        $this->affiliateLink         = $affiliateLink;
        $this->label                 = $label;
        $this->status                = $status;
        $this->featured              = $featured;
        $this->recommended           = $recommended;
        $this->slug                  = $slug;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLogo(): array
    {
        return $this->logo;
    }

    public function getWelcomeOfferLine1(): ?string
    {
        return $this->welcomeOfferLine1;
    }

    public function getWelcomeOfferLine2(): ?string
    {
        return $this->welcomeOfferLine2;
    }

    public function getTermsAndConditions(): ?string
    {
        return $this->termsAndConditions;
    }

    public function getTermsAndConditionsUrl(): ?string
    {
        return $this->termsAndConditionsUrl;
    }

    public function getAffiliateLink(): string
    {
        return $this->affiliateLink;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function getFeatured(): ?string
    {
        return $this->featured;
    }

    public function getRecommended(): ?bool
    {
        return (bool) $this->recommended;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }
}
