<?php

namespace OhMyBingo\Page\Component\SiteListings;

use OhMyBingo\Page\Component\ComponentInterface;
use OhMyBingo\Page\Traits\PathTrait;

class SiteListings implements ComponentInterface
{
    use PathTrait;
    use SiteListingsTrait;

    const CONTENT_TYPE = 'siteListings';

    protected $path;
    private $sites = [];

    public function __construct(string $path, $siteListingEntries)
    {
        $this->setPath($path);
        $this->sites = [];
        foreach ($siteListingEntries as $siteEntry) {
            if ($siteEntry->getLabel()) {
//                $siteLabel = $siteEntry->label->getLabel();
                $siteLabel = $siteEntry->getLabel();
            } else {
                $siteLabel = null;
            }
//            $logo = $siteEntry->getLogo();
            array_push(
                $this->sites,
                new SiteListing(
                    $siteEntry->getName(),
                    $siteEntry->getLogo(), // ImageProcessor::toImage($siteEntry->getLogo(), 137, 137, 'png', 'pad'),
                    $siteEntry->getWelcomeOfferLine1(),
                    $siteEntry->getWelcomeOfferLine2(),
                    $siteEntry->getTermsAndConditions(),
                    $siteEntry->getTermsAndConditionsUrl(),
                    $siteEntry->getAffiliateLink(),
                    $siteLabel,
                    $siteEntry->getStatus(),
                    $siteEntry->getFeatured(),
                    $siteEntry->getRecommended(),
                    $siteEntry->getSlug()
                )
            );
        }
    }

    public function getSites(): array
    {
        return $this->sites;
    }

//    public function hasSites(): bool
//    {
//        return (count($this->sites) > 1) ? true : false;
//    }
}
