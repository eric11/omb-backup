<?php

namespace OhMyBingo\Page\Component\SiteListings;

use FirstLeads\Presentation\Pagination\Pagination;

trait SiteListingsTrait
{
    protected $siteListings;
    protected $pagination;

    public function getSiteListings(): ?SiteListings
    {
        return $this->siteListings;
    }

    public function getSiteListingsSites(): ?array
    {
        return ($this->siteListings) ? ($this->siteListings->getSites()) : [];
    }

    public function setPagination(Pagination $pagination): void
    {
        $this->pagination   = $pagination;
    }

    public function getPagination(): Pagination
    {
        return $this->pagination;
    }
}