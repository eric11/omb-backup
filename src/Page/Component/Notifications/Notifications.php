<?php

namespace OhMyBingo\Page\Component\Notifications;

use OhMyBingo\Page\Component\ComponentInterface;
use OhMyBingo\Content\Image\ImageProcessor;

class Notifications implements ComponentInterface
{
    const CONTENT_TYPE = 'notifications';

    protected $title;
    protected $sites = [];
    protected $termsAndConditions;

    public function __construct($notificationsEntry)
    {
        $this->title = $notificationsEntry->title;
        $this->sites = [];
        foreach ($notificationsEntry->sites as $siteEntry) {
            array_push(
                $this->sites,
                new Site(
                    $siteEntry->slug,
                    $siteEntry->name,
                    $siteEntry->affiliateLink,
                    ImageProcessor::toImage($siteEntry->logo, 60, 60, 'png', 'pad'),
                    $siteEntry->welcomeOfferLine1,
                    $siteEntry->welcomeOfferLine2
                )
            );
        }
        $this->termsAndConditions = $notificationsEntry->termsAndConditions;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSites(): array
    {
        return $this->sites;
    }

    public function getTermsAndConditions(): string
    {
        return $this->termsAndConditions;
    }
}
