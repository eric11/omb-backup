<?php

namespace OhMyBingo\Page\Component\ArticleList;

use OhMyBingo\Page\Component\ComponentInterface;
use DateTime;

class ArticleList implements ComponentInterface
{
    const CONTENT_TYPE = 'articleList';

    private $title;
    private $articles = [];
    private $viewAllButtonText;

    public function __construct($articleListEntry)
    {
        $this->title = $articleListEntry->title;
        $this->articles = [];
        foreach ($articleListEntry->articles as $articleEntry) {
            array_push(
                $this->articles,
                new Article(
                    $articleEntry->slug,
                    $articleEntry->title,
                    (new DateTime($articleEntry->date))->format('d F y') // convert 2019-03-21T00:00:00Z to 23 May 19
                )
            );
        }
        $this->viewAllButtonText = $articleListEntry->viewAllButtonText;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getArticles(): array
    {
        return $this->articles;
    }

    public function getViewAllButtonText(): string
    {
        return $this->viewAllButtonText;
    }
}
