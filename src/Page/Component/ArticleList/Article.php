<?php

namespace OhMyBingo\Page\Component\ArticleList;

class Article
{
    private $slug;
    private $title;
    private $description;
    private $copy;
    private $date;

    public function __construct(string $slug, string $title, ?string $date)
    {
        $this->slug        = $slug;
        $this->title       = $title;
        $this->date        = $date;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getCopy(): ?string
    {
        return $this->copy;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }
}
