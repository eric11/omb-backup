<?php

namespace OhMyBingo\Page\Component\Button;

use OhMyBingo\Page\Component\ComponentInterface;

class Button implements ComponentInterface
{
    const CONTENT_TYPE = 'button';

    private $name;
    private $title;
    private $textColour;
    private $backgroundColour;
    private $borderColour;
    private $page;

    public function __construct($buttonEntry)
    {
        $this->name             = $buttonEntry->name;
        $this->title            = $buttonEntry->title;
        $this->textColour       = $buttonEntry->textColour;
        $this->backgroundColour = $buttonEntry->backgroundColour;
        $this->borderColour     = $buttonEntry->borderColour;
        $this->page             = $buttonEntry->page;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getTextColor(): string
    {
        return $this->textColour;
    }

    public function getBackgroundColor(): string
    {
        return $this->backgroundColour;
    }

    public function getBorderColor(): string
    {
        return $this->borderColour;
    }

    public function getPage(): string
    {
        return $this->page;
    }
}
