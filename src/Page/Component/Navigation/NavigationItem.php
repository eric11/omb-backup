<?php

namespace OhMyBingo\Page\Component\Navigation;

use OhMyBingo\Page\Component\ComponentInterface;

class NavigationItem implements ComponentInterface
{
    const CONTENT_TYPE = 'navigationItem';
    const MENU_CONTENT_TYPE = 'menu';
    const PAGE_CONTENT_TYPE = 'page';

    private $contentType;
    private $title;
    private $slug;
    private $children = [];
    private $navigationText;

    public function __construct($navigationItemEntry)
    {
        $this->contentType = $navigationItemEntry->getContentType()->getId();
        $this->title = $navigationItemEntry->title;
        $this->slug  = $navigationItemEntry->slug;
        if ($this->contentType === self::MENU_CONTENT_TYPE) {
            foreach ($navigationItemEntry->children as $navigationItemEntry) {
                array_push($this->children, new NavigationItem($navigationItemEntry));
            }
        }
        if ($this->contentType === self::PAGE_CONTENT_TYPE) {
            $this->navigationText = $navigationItemEntry->navigationText;
        }
        $this->children;
    }

    public function getContentType(): string
    {
        return $this->contentType;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getChildren(): array
    {
        return $this->children;
    }

    public function getNavigationText(): ?string
    {
        return $this->navigationText;
    }
}
