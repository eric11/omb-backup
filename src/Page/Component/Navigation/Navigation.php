<?php

namespace OhMyBingo\Page\Component\Navigation;

use OhMyBingo\Page\Component\ComponentInterface;

class Navigation implements ComponentInterface
{
    const CONTENT_TYPE = 'navigation';

    private $children = [];

    public function __construct($navigationEntry)
    {
        foreach ($navigationEntry->children as $navigationItemEntry) {
            array_push($this->children, new NavigationItem($navigationItemEntry));
        }
        $this->children;
    }

    public function getChildren(): array
    {
        return $this->children;
    }
}
