<?php

namespace OhMyBingo\Page\Component\SitesOfTheMonth;

use OhMyBingo\Page\Component\ComponentInterface;
use OhMyBingo\Content\Image\ImageProcessor;

class SitesOfTheMonth implements ComponentInterface
{
    const CONTENT_TYPE = 'sitesOfTheMonth';

    private $title;
    private $leaders;

    public function __construct($entry)
    {
        $this->title = $entry->title;
        $this->leaders = [
            new Site(
                'bingo',
                $entry->bingoAward,
                $entry->bingoSite->name,
                html_entity_decode($entry->bingoSite->welcomeOfferLine1),
                $entry->bingoAffiliateCode,
                $entry->bingoPlayButtonText,
                ImageProcessor::toImage($entry->bingoSite->logo, 220, 138, 'png', 'pad'),
                $entry->bingoSite->slug,
                $entry->bingoSite->affiliateLink
            ),
            new Site(
                'slots',
                $entry->slotsAward,
                $entry->slotsSite->name,
                html_entity_decode($entry->slotsSite->welcomeOfferLine1),
                $entry->slotsAffiliateCode,
                $entry->slotsPlayButtonText,
                ImageProcessor::toImage($entry->slotsSite->logo, 220, 138, 'png', 'pad'),
                $entry->slotsSite->slug,
                $entry->slotsSite->affiliateLink
            ),
            new Site(
                'casino',
                $entry->casinoAward,
                $entry->casinoSite->name,
                html_entity_decode($entry->casinoSite->welcomeOfferLine1),
                $entry->casinoAffiliateCode,
                $entry->casinoPlayButtonText,
                ImageProcessor::toImage($entry->casinoSite->logo, 220, 138, 'png', 'pad'),
                $entry->casinoSite->slug,
                $entry->casinoSite->affiliateLink
            ),
        ];
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getLeaders(): array
    {
        return $this->leaders;
    }
}
