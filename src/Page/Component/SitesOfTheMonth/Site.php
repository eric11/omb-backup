<?php

namespace OhMyBingo\Page\Component\SitesOfTheMonth;

class Site
{
    private $type;
    private $award;
    private $name;
    private $copy;
    private $affiliateCode;
    private $playButtonText;
    private $logo;
    private $slug;
    private $affiliateLink;

    public function __construct(
        string $type,
        string $award,
        string $name,
        string $copy,
        ?string $affiliateCode,
        string $playButtonText,
        array $logo,
        string $slug,
        string $affiliateLink
    ) {
        $this->type           = $type;
        $this->award          = $award;
        $this->name           = $name;
        $this->copy           = $copy;
        $this->affiliateCode  = $affiliateCode;
        $this->playButtonText = $playButtonText;
        $this->logo           = $logo;
        $this->slug           = $slug;
        // If affiliate code - then format affiliate link
        if ($affiliateCode) {
            $this->affiliateLink  = $affiliateLink; // append affiliate code to the end
        } else {
            $this->affiliateLink  = $affiliateLink;
        }
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getAward(): string
    {
        return $this->award;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCopy(): string
    {
        return $this->copy;
    }

    public function getAffiliateCode(): ?string
    {
        return $this->affiliateCode;
    }

    public function getPlayButtonText(): string
    {
        return $this->playButtonText;
    }

    public function getLogo(): array
    {
        return $this->logo;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getAffiliateLink(): string
    {
        return $this->affiliateLink;
    }
}
