<?php

namespace OhMyBingo\Page\Component\ArticleListings;

use OhMyBingo\Page\Component\ComponentInterface;
use OhMyBingo\Page\Traits\PathTrait;

class ArticleListings implements ComponentInterface
{
    use PathTrait;

    const CONTENT_TYPE = 'articleListings';

    protected $path;
    private $articles = [];

    public function __construct(string $path, $articleListingEntries  = null)
    {
        $this->setPath($path);
        if ($articleListingEntries) {
            foreach ($articleListingEntries as $articleEntry) {
                array_push(
                    $this->articles,
                    new ArticleListing(
                        $articleEntry->getTitle(),
                        $articleEntry->getShortDescription(),
                        $articleEntry->getDate(),
                        $articleEntry->getSlug()
                    )
                );
            }
        }
    }

    public function getArticles(): array
    {
        return $this->articles;
    }
}
