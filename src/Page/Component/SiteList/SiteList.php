<?php

namespace OhMyBingo\Page\Component\SiteList;

use OhMyBingo\Page\Component\ComponentInterface;
use OhMyBingo\Content\Image\ImageProcessor;

class SiteList implements ComponentInterface
{
    const CONTENT_TYPE = 'siteList';

    private $title;
    private $sites = [];
    private $playButtonText;

    public function __construct($siteListEntry)
    {
        $this->title = $siteListEntry->title;
        $this->sites = [];
        foreach ($siteListEntry->sites as $siteEntry) {
            array_push(
                $this->sites,
                new Site(
                    $siteEntry->slug,
                    $siteEntry->name,
                    $siteEntry->affiliateLink,
                    ImageProcessor::toImage($siteEntry->logo, 44, 44, 'png', 'pad'),
                    $siteEntry->welcomeOfferLine1,
                    $siteEntry->welcomeOfferLine2
                )
            );
        }
        $this->playButtonText = $siteListEntry->playButtonText;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSites(): array
    {
        return $this->sites;
    }

    public function getPlayButtonText(): string
    {
        return $this->playButtonText;
    }
}
