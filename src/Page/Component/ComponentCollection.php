<?php

namespace OhMyBingo\Page\Component;

class ComponentCollection
{
    private $items;

    public function __construct(array $items = [])
    {
        $this->items = $items;
    }

    public function add(string $componentSlug, ComponentInterface $component)
    {
        $this->items[$componentSlug] = $component;
        return $this;
    }

    public function get(string $componentSlug)
    {
        return $this->items[$componentSlug];
    }

    public function items()
    {
        return $this->items;
    }
}
