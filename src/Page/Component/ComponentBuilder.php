<?php

namespace OhMyBingo\Page\Component;

use Contentful\Core\Resource\ResourceInterface;

class ComponentBuilder
{
    public function buildFromEntry(string $componentName, ResourceInterface $componentEntry): ComponentInterface
    {
        $class = "\\OhMyBingo\\Page\\Component\\$componentName\\$componentName";
        return new $class($componentEntry);
    }

    public function buildFromEntries(string $componentName, array $componentEntries): ComponentInterface
    {
        $class = "\\OhMyBingo\\Page\\Component\\$componentName\\$componentName";
        return new $class($componentEntries);
    }
}
