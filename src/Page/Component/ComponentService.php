<?php

namespace OhMyBingo\Page\Component;

use OhMyBingo\Content\ContentService;
use OhMyBingo\Page\Component\ComponentBuilder;
use OhMyBingo\Page\Component\Navigation\Navigation;
use OhMyBingo\Page\Component\QuickLinks\QuickLinks;
use OhMyBingo\Page\Component\SitesOfTheMonth\SitesOfTheMonth;
use OhMyBingo\Page\Component\SiteList\SiteList;
use OhMyBingo\Page\Component\NewsList\NewsList;
use OhMyBingo\Page\Component\ArticleList\ArticleList;
use OhMyBingo\Page\Component\Notifications\Notifications;
use OhMyBingo\Page\Component\Poster\Poster;
use OhMyBingo\Content\Query\Query;
use FirstLeads\Infrastructure\Cache\CacheService;
use FirstLeads\Infrastructure\Cache\CacheKey\CacheKey;
use FirstLeads\Infrastructure\Cache\Exception\CacheNotFoundException;
use OhMyBingo\Page\Component\Exception\ComponentNotFoundException;

class ComponentService
{
    protected $componentBuilder;
    protected $contentService;
    protected $cacheService;

    public function __construct(ContentService $contentService, ComponentBuilder $componentBuilder, CacheService $cacheService)
    {
        $this->contentService   = $contentService;
        $this->componentBuilder = $componentBuilder;
        $this->cacheService     = $cacheService;
    }

    public function get(string $path): ComponentCollection
    {
        /** @var ComponentCollection $componentCollection */
        $componentCollection = new ComponentCollection();
        $componentCollection = $this->addGlobal($componentCollection);
        switch ($path) {
            case 'home':
                $componentCollection = $this->addHome($componentCollection);
                break;
            case 'site-review':
                $componentCollection = $this->addSiteReview($componentCollection);
                break;
            case 'site':
                break;
            default:
                break;
        }
        return $componentCollection;
    }

    private function add(ComponentCollection &$componentCollection, string $componentType, string $componentSlug, string $componentReference, bool $cacheResource = false): void
    {
        if ($cacheResource === true) {
            /** @var CacheKey $cacheKey */
            $cacheKey = new CacheKey('component.resource', $componentReference);
            try {
                $componentEntryJson = $this->cacheService->get($cacheKey);
                $componentEntry = $this->contentService->parseJson($componentEntryJson);
            } catch (CacheNotFoundException $exception) {
                $componentEntry = $this->contentService->getEntry(Query::get($componentType, $componentSlug));
                if (!$componentEntry) {
                    throw new ComponentNotFoundException(json_encode(['componentType' => $componentType, 'componentSlug' => $componentSlug]));
                }
                $componentEntryJson = json_encode($componentEntry->jsonSerialize());
                $this->cacheService->set($cacheKey, $componentEntryJson);
            }
            $componentClassName = ucwords($componentType);
            $component = $this->componentBuilder->buildFromEntry($componentClassName, $componentEntry);
            $componentCollection->add($componentReference, $component);
        } else {
            /** @var CacheKey $cacheKey */
            $cacheKey = new CacheKey('component.object', $componentReference);
            try {
                $component = $this->cacheService->get($cacheKey);
            } catch (CacheNotFoundException $exception) {
                $componentEntry = $this->contentService->getEntry(Query::get($componentType, $componentSlug));
                $componentClassName = ucwords($componentType);
                $component = $this->componentBuilder->buildFromEntry($componentClassName, $componentEntry);
                $this->cacheService->set($cacheKey, $component);
            }
            $componentCollection->add($componentReference, $component);
        }
    }

    private function addGlobal(ComponentCollection $componentCollection): ComponentCollection
    {
        $this->add($componentCollection, Navigation::CONTENT_TYPE, 'header-navigation', 'headerNavigation');
        $this->add($componentCollection, Navigation::CONTENT_TYPE, 'footer-navigation', 'footerNavigation');
        $this->add($componentCollection, SiteList::CONTENT_TYPE, 'editors-choice-bingo', 'editorsChoiceBingo');
        $this->add($componentCollection, SiteList::CONTENT_TYPE, 'editors-choice-slots', 'editorsChoiceSlots');
        $this->add($componentCollection, NewsList::CONTENT_TYPE, 'editors-choice', 'editorsChoiceNews');
        $this->add($componentCollection, ArticleList::CONTENT_TYPE, 'editors-choice', 'editorsChoiceArticles');
        $this->add($componentCollection, Notifications::CONTENT_TYPE, 'notifications', 'notifications', true);
        return $componentCollection;
    }

    private function addHome(ComponentCollection $componentCollection): ComponentCollection
    {
        $this->add($componentCollection, SitesOfTheMonth::CONTENT_TYPE, 'sites-of-the-month', 'sitesOfTheMonth'); // leaderBoard

        $quickLinksEntries  = $this->contentService->getEntries(Query::get(QuickLinks::CONTENT_TYPE, true, 'fields.active'));
        $quickLinks       = $this->componentBuilder->buildFromEntries('QuickLinks', $quickLinksEntries);
        $componentCollection->add('quickLinks', $quickLinks);
        return $componentCollection;
    }

    private function addSiteReview(ComponentCollection $componentCollection): ComponentCollection
    {
//        $posterEntry = $this->contentService->getEntry(Query::get(Poster::CONTENT_TYPE, 'site-review'));
//        $poster      = $this->componentBuilder->buildFromEntry('Poster', $posterEntry);
//        $componentCollection->add('Poster', $poster);
        return $componentCollection;
    }
}
