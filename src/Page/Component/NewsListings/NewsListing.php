<?php

namespace OhMyBingo\Page\Component\NewsListings;

class NewsListing
{
    private $title;
    private $shortDescription;
    private $date;
    private $slug;

    public function __construct(
        string $title,
        string $shortDescription,
        string $date,
        string $slug

    ) {
        $this->title             = $title;
        $this->shortDescription  = $shortDescription;
        $this->date              = $date;
        $this->slug              = $slug;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }
}
