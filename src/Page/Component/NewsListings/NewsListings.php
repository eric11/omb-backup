<?php

namespace OhMyBingo\Page\Component\NewsListings;

use OhMyBingo\Page\Component\ComponentInterface;
use OhMyBingo\Page\Traits\PathTrait;

class NewsListings implements ComponentInterface
{
    use PathTrait;

    const CONTENT_TYPE = 'newsListings';

    protected $path;
    private $newsArticles = [];

    public function __construct(string $path, $newsListingEntries)
    {
        $this->setPath($path);
        $this->newsArticles = [];
        foreach ($newsListingEntries as $newsArticleEntry) {
            array_push(
                $this->newsArticles,
                new NewsListing(
                    $newsArticleEntry->getTitle(),
                    $newsArticleEntry->getShortDescription(),
                    $newsArticleEntry->getDate(),
                    $newsArticleEntry->getSlug()
                )
            );
        }
    }

    public function getNewsArticles(): array
    {
        return $this->newsArticles;
    }
}
