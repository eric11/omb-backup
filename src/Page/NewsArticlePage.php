<?php

namespace OhMyBingo\Page;

use OhMyBingo\Page\Attributes\Links;
use OhMyBingo\Page\Attributes\Slug;
use OhMyBingo\Page\Attributes\Meta;
use OhMyBingo\Page\Attributes\News\NewsArticle;

class NewsArticlePage extends AbstractPage implements PageInterface
{
    const CONTENT_TYPE = 'news';
    const ROUTE = 'bingo-news';
    const ROUTE_NAME = 'news';
    const PUBLISHER_NAME = 'OhMyBingo';
    const PUBLISHER_LOGO_URL = 'http://127.0.0.1:8000/img/logo/logo@2x.png';

    private $newsArticle;

    public function __construct(
        Slug $slug,
        Meta $meta,
        Links $links,
        NewsArticle $newsArticle
    ) {
        parent::__construct($slug, $meta, $links);
        $this->newsArticle = $newsArticle;
    }

    public function getNewsArticle(): NewsArticle
    {
        return $this->newsArticle;
    }
}
