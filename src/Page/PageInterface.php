<?php

namespace OhMyBingo\Page;

use OhMyBingo\Page\Attributes\Slug;
use OhMyBingo\Page\Attributes\Meta;
use OhMyBingo\Page\Component\ComponentInterface;

interface PageInterface
{
    public function getSlug():  Slug;
    public function getMeta():  Meta;
    public function setModal(ComponentInterface $modal): void;
    public function getModal(): ?ComponentInterface;
}
