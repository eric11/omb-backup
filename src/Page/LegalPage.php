<?php

namespace OhMyBingo\Page;

use OhMyBingo\Page\Attributes\Links;
use OhMyBingo\Page\Attributes\Slug;
use OhMyBingo\Page\Attributes\Meta;
use OhMyBingo\Page\Attributes\Article;
use OhMyBingo\Page\Traits\DocumentTrait;

class LegalPage extends AbstractPage implements PageInterface
{
    use DocumentTrait;

    const CONTENT_TYPE = 'legalPage';

    protected $article;
    private $exception = [];

    public function __construct(
        Slug $slug,
        Meta $meta,
        Links $links,
        Article $article
    ) {
        parent::__construct($slug, $meta, $links);
        $this->setArticle($article);
    }
}
