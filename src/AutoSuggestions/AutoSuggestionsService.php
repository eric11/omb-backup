<?php

namespace OhMyBingo\AutoSuggestions;

use Psr\Log\LoggerInterface as Logger;
use OhMyBingo\Search\SearchService;
use FirstLeads\Infrastructure\Cache\CacheKey\CacheKey;
use FirstLeads\Infrastructure\Cache\CacheService;
use FirstLeads\Infrastructure\Cache\Exception\CacheNotFoundException;
use OhMyBingo\AutoSuggestions\Exception\NoAutoSuggestionsException;

class AutoSuggestionsService
{
    private $searchService;
    protected $cacheService;
    private $logger;

    public function __construct(SearchService $searchService, CacheService $cacheService,  Logger $logger)
    {
        $this->searchService = $searchService;
        $this->cacheService     = $cacheService;
        $this->logger        = $logger;
    }

    /*
     * Gets auto-suggestions
     *
     * @throws NoAutoSuggestionsException
     */
    public function get(): array
    {

        /** @var CacheKey $cacheKey */
        $cacheKey = new CacheKey('autoSuggestions');

        try {
            /** @var array $autoSuggestions */
            $autoSuggestions = $this->cacheService->get($cacheKey);
        } catch (CacheNotFoundException $exception) {
            /** @var array $autoSuggestions */
            $autoSuggestions = $this->fetchAutoSuggestions();
            if (!$autoSuggestions) {
                throw new NoAutoSuggestionsException(sprintf('No auto-suggestions found'));
            }
            $this->cacheService->set($cacheKey, $autoSuggestions);
        }
        return $autoSuggestions;
    }

    /*
     * Gets auto-suggestions
     */
    public function fetchAutoSuggestions(): array
    {
        $autoSuggestions = [];

        # Sites
        $sites = $this->searchService->search('site', 'site');
        foreach ($sites->getEntries() as $site) {
            $autoSuggestions[] = [
                'value'    => $site->getName(),
                'category' => 'Site',
            ];
        }

        # News
        $news = $this->searchService->search('news', 'news');
        $newsArticles = $news->getEntries();
        foreach ($newsArticles as $newsArticle) {
            $autoSuggestions[] = [
                'value'    => $newsArticle->getTitle(),
                'category' => 'News',
            ];
        }

        # Articles
        $news = $this->searchService->search('article', 'article');
        $articles = $news->getEntries();
        foreach ($articles as $article) {
            $autoSuggestions[] = [
                'value'    => $article->getTitle(),
                'category' => 'Article',
            ];
        }
        return $autoSuggestions;
    }
}
