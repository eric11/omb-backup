<?php

namespace OhMyBingo\Search\Entry;

class Site
{
    const TYPE = 'site';
    const DEFAULT_CLASSIFICATION = 'Bingo';

    private $id;
    private $name;
    private $logo;
    private $welcomeOfferLine1;
    private $welcomeOfferLine2;
    private $termsAndConditions;
    private $termsAndConditionsUrl;
    private $affiliateLink;
    private $slug;
    private $classification;
    private $status;
    private $software;
    private $network;
    private $siteFeatures;
    private $availableGames;
    private $recommended;
    private $featured;
    private $label;

    public function __construct(
        string $name,
        array $logo,
        string $welcomeOfferLine1,
        string $welcomeOfferLine2,
        string $termsAndConditions,
        string $termsAndConditionsUrl,
        string $affiliateLink,
        ?string $label,
        string $slug,
        string $classification,
        ?string $status,
        ?string $software,
        ?string $network,
        ?string $siteFeatures,
        ?string $availableGames,
        ?string $recommended,
        ?string $featured
    ) {
        $this->id                    = $slug;
        $this->name                  = $name;
        $this->logo                  = $logo;
        $this->welcomeOfferLine1     = $welcomeOfferLine1;
        $this->welcomeOfferLine2     = $welcomeOfferLine2;
        $this->termsAndConditions    = html_entity_decode($termsAndConditions);
        $this->termsAndConditionsUrl = html_entity_decode($termsAndConditionsUrl);
        $this->affiliateLink         = $affiliateLink;
        $this->label                 = $label;
        $this->classification        = $classification;
        $this->status                = $status;
        $this->software              = $software;
        $this->network               = $network;
        $this->siteFeatures          = $siteFeatures;
        $this->availableGames        = $availableGames;
        $this->recommended           = $recommended;
        $this->featured              = $featured;
        $this->slug                  = $slug;
    }

    public function getData()
    {
        return [
            'name'                   => $this->name,
            'logo'                  => $this->logo,
            'welcomeOfferLine1'     => $this->welcomeOfferLine1,
            'welcomeOfferLine2'     => $this->welcomeOfferLine2,
            'termsAndConditions'    => $this->termsAndConditions,
            'termsAndConditionsUrl' => $this->termsAndConditionsUrl,
            'affiliateLink'         => $this->affiliateLink,
            'label'                 => $this->label,
            'classification'        => $this->classification,
            'status'                => $this->status,
            'software'               => $this->software,
            'network'                => $this->network,
            'siteFeatures'          => $this->siteFeatures,
            'availableGames'        => $this->availableGames,
            'recommended'           => $this->recommended,
            'featured'              => $this->featured,
            'slug'                  => $this->slug,
        ];
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLogo(): array
    {
        return $this->logo;
    }

    public function getWelcomeOfferLine1(): string
    {
        return $this->welcomeOfferLine1;
    }

    public function getWelcomeOfferLine2(): string
    {
        return $this->welcomeOfferLine2;
    }

    public function getTermsAndConditions(): string
    {
        return $this->termsAndConditions;
    }

    public function getTermsAndConditionsUrl(): string
    {
        return $this->termsAndConditionsUrl;
    }

    public function getAffiliateLink(): string
    {
        return $this->affiliateLink;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getClassification(): string
    {
        return $this->classification;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function getRecommended(): ?bool
    {
        return (bool) $this->recommended;
    }

    public function getFeatured(): ?bool
    {
        return (bool) $this->featured;
    }

    public function getSiteFeatures(): ?string
    {
        return $this->siteFeatures;
    }

    public function getAvailableGames(): ?string
    {
        return $this->availableGames;
    }

    public function getSoftware(): ?string
    {
        return $this->software;
    }

    public function getNetwork(): ?string
    {
        return $this->network;
    }

    public function getPrintId(): string
    {
        return self::TYPE . '-' . $this->name."\n";
    }
}
