<?php

namespace OhMyBingo\Search\Entry;

class Page
{
    const TYPE = 'page';

    private $id;
    private $title;
    private $slug;

    public function __construct(string $title, string $slug)
    {
        $this->id    = $slug;
        $this->title = $title;
        $this->slug  = $slug;
    }

    public function getData(): array
    {
        return [
            'title' => $this->title,
            'slug'  => $this->slug,
        ];
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getPrintId(): string
    {
        return self::TYPE . '-' . $this->title."\n";
    }
}
