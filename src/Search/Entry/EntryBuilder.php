<?php

namespace OhMyBingo\Search\Entry;

use OhMyBingo\Search\Exception\NotSearchableException;
use OhMyBingo\Content\Image\ImageProcessor;
use ErrorException;

class EntryBuilder
{
    /*
     * @throws NotSearchableException Unknown entry class.
     * @throws ErrorException
     */
    public static function build(array $data, string $type)
    {
        switch ($type) {
            case 'page':
                return new Page($data['title'], $data['slug']);
            case 'site':
                return new Site(
                    $data['name'],
                    $data['logo'],
                    $data['welcomeOfferLine1'],
                    $data['welcomeOfferLine2'],
                    $data['termsAndConditions'],
                    $data['termsAndConditionsUrl'],
                    $data['affiliateLink'],
                    $data['label'],
                    $data['slug'],
                    $data['classification'],
                    $data['status'],
                    $data['software'],
                    $data['network'],
                    $data['siteFeatures'],
                    $data['availableGames'],
                    $data['recommended'],
                    $data['featured']
                );
            case 'network':
                return new Network($data['name'], $data['slug']);
            case 'software':
                return new Software($data['name'], $data['slug']);
            case 'article':
                return new Article($data['title'], $data['shortDescription'], $data['date'], $data['slug']);
            case 'news':
                return new News($data['title'], $data['shortDescription'], $data['date'], $data['slug']);
            default:
                throw new NotSearchableException('Unknown entity type: '.$type);
        }
    }
}
