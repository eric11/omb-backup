<?php

namespace OhMyBingo\Search\Entry;

use Psr\Log\LoggerInterface;
use OhMyBingo\Content\ContentService;
use OhMyBingo\Search\Exception\NotSearchableException;
use OhMyBingo\Content\Image\ImageProcessor;
use OhMyBingo\Content\Query\Query;
use Contentful\RichText\Renderer;
use Stringy\Stringy as S;

class EntryImporter
{
    private $contentService;
    private $logger;
    private $contentRenderer;

    public function __construct(ContentService $contentService, LoggerInterface $logger)
    {
        $this->contentService = $contentService;
        $this->logger         = $logger;
        $this->contentRenderer = new Renderer();
    }

    public function import(array $data)
    {
        $locale = getenv('CONTENTFUL_DEFAULT_LOCALE');
        $type   = $data['sys']['contentType']['sys']['id'];
        $fields = $data['fields'];
        switch ($type) {
            case 'page':
                $pageEntry = new Page($fields['title'][$locale], $fields['slug'][$locale]);
                $this->logger->info("Imported Page:", ['pageEntry' => $pageEntry->getData()]);
                return $pageEntry;
            case 'site':
                $logoAsset = $this->contentService->getAsset($fields['logo'][$locale]['sys']['id'], 'en-US');
                $siteLogo = ImageProcessor::toImage($logoAsset, 137, 137, 'png', 'pad');
                $siteLabelId = $fields['label'][$locale]['sys']['id'] ?? null;
                if (!$siteLabelId) {
                    $siteLabel = null;
                } else {
                    $siteLabel = ($this->contentService->getEntry(Query::get('siteLabel', $fields['label'][$locale]['sys']['id'], 'sys.id')))->getLabel();
                }
                if (!empty($fields['classification']) && !empty($fields['classification'])) {
                    $siteClassification = $fields['classification'][$locale];
                } else {
                    $siteClassification = Site::DEFAULT_CLASSIFICATION;
                }
                $siteEntry = new Site(
                    $fields['name'][$locale],
                    $siteLogo,
                    $fields['welcomeOfferLine1'][$locale] ?? '',
                    $fields['welcomeOfferLine2'][$locale] ?? '',
                    $fields['termsAndConditions'][$locale] ?? '',
                    $fields['termsAndConditionsUrl'][$locale] ?? '',
                    $fields['affiliateLink'][$locale] ?? '',
                    $siteLabel,
                    $fields['slug'][$locale],
                    $siteClassification,
                    $fields['status'][$locale] ?? null,
                    $fields['software'][$locale] ?? null,
                    $fields['network'][$locale] ?? null,
                    $fields['siteFeatures'][$locale] ?? null,
                    $fields['availableGames'][$locale] ?? null,
                    $fields['recommended'][$locale] ?? null,
                    $fields['featured'][$locale] ?? null
                );
                $this->logger->info("Imported Site:", ['siteEntry' => $siteEntry->getData()]);
                return $siteEntry;
            case 'news':
                $newsNode = $this->contentService->parseJson(json_encode($data));
                $newsShortDescription = (!empty($fields['copy'][$locale]) ? $this->contentRenderer->render($newsNode->getCopy()) : null);
                $newsShortDescription = S::create($newsShortDescription)->safeTruncate(160, '...');
                $newsEntry = new News($fields['title'][$locale], $newsShortDescription, $fields['date'][$locale], $fields['slug'][$locale]);
                $this->logger->info("Imported News:", ['newsEntry' => $newsEntry->getData()]);
                return $newsEntry;
            case 'article':
                $articleNode = $this->contentService->parseJson(json_encode($data));
                $articleShortDescription = (!empty($fields['copy'][$locale]) ? $this->contentRenderer->render($articleNode->getCopy()) : null);
                $articleShortDescription = S::create($articleShortDescription)->safeTruncate(160, '...');
                $articleEntry = new Article($fields['title'][$locale], $articleShortDescription, $fields['date'][$locale], $fields['slug'][$locale]);
                $this->logger->info("Imported Article:", ['articleEntry' => $articleEntry->getData()]);
                return $articleEntry;
            case 'network':
                $networkEntry = new Network($fields['name'][$locale], $fields['slug'][$locale]);
                $this->logger->info("Imported Network:", ['networkEntry' => $networkEntry->getData()]);
                return $networkEntry;
            case 'software':
                $softwareEntry = new Software($fields['name'][$locale], $fields['slug'][$locale]);
                $this->logger->info("Imported Software:", ['softwareEntry' => $softwareEntry->getData()]);
                return $softwareEntry;
            default:
                throw new NotSearchableException('Unknown entity type: '.$type);
        }
    }
}
