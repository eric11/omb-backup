<?php

namespace OhMyBingo\Search\Entry;

class News
{
    const TYPE = 'news';

    private $id;
    private $title;
    private $shortDescription;
    private $date;
    private $slug;

    public function __construct(string $title, string $shortDescription, string $date, string $slug)
    {
        $this->id                = $slug;
        $this->title             = $title;
        $this->shortDescription  = $shortDescription;
        $this->date              = $date;
        $this->slug              = $slug;
    }

    public function getData(): array
    {
        return [
            'title'             => $this->title,
            'shortDescription'  => $this->shortDescription,
            'date'              => $this->date,
            'slug'              => $this->slug,
        ];
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getPrintId(): string
    {
        return self::TYPE . '-' . $this->slug."\n";
    }
}
