<?php

namespace OhMyBingo\Search\Entry;

class Network
{
    const TYPE = 'network';

    private $id;
    private $name;
    private $slug;

    public function __construct(string $name, string $slug)
    {
        $this->id   = $slug;
        $this->name = $name;
        $this->slug = $slug;
    }

    public function getData(): array
    {
        return [
            'name' => $this->name,
            'slug' => $this->slug,
        ];
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getPrintId(): string
    {
        return self::TYPE . '-' . $this->name."\n";
    }
}
