<?php

namespace OhMyBingo\Search\Command;

use Exception;
use OhMyBingo\Search\Entry\EntryImporter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use OhMyBingo\Search\SearchService;
use OhMyBingo\Search\Entry\Page as PageEntry;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use OhMyBingo\Search\Entry\EntryBuilder;
use Bcn\Component\Json\Reader;
use OhMyBingo\Search\Exception\NotSearchableException;
use Psr\Log\LoggerInterface;

class IndexCommand extends Command
{
    protected static $defaultName = 'search:index';

    private $searchService;
    private $entryImporter;
    private $contentDatabaseFilePath;
    private $logger;

    public function __construct(SearchService $searchService, ParameterBagInterface $params, EntryImporter $entryImporter, LoggerInterface $logger)
    {
        $this->searchService           = $searchService;
        $this->logger                  = $logger;
        $this->contentDatabaseFilePath = $params->get('content.database_file_path');
        $this->entryImporter           = $entryImporter;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Indexes search data.')
            ->setHelp('This indexes data in Elasticsearch for use by search engines...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Bulk indexing search data. Please wait...');
        $fh = fopen($this->contentDatabaseFilePath, "r");
        $reader = new Reader($fh);
        while($data = $reader->read()) {
            $i = 0;
            foreach ($data['entries'] as $entryData) {
                try {
                    $entry = $this->entryImporter->import($entryData);
                } catch (NotSearchableException $e) {
                    $this->logger->warning($e->getMessage());
                    continue;
                }
                if (!$entry) {
                    continue;
                }
                try {
                    $shouldIndex = true;
                    if (getenv('ELASTICSEARCH_INDEX_SITES') != '1') {
                        $shouldIndex = false;
                    }
                    if (getenv('ELASTICSEARCH_INDEX_NEWS') != '1') {
                        $shouldIndex = false;
                    }
                    if (getenv('ELASTICSEARCH_INDEX_ARTICLES') != '1') {
                        $shouldIndex = false;
                    }
                    if (!$shouldIndex) {
                        continue;
                    }
                    $this->searchService->indexOrUpdate($entry::TYPE, $entry::TYPE, $entry->getId(), $entry->getData());
                    $this->logger->info(sprintf('%s) %s', $i, $entry->getPrintId()));
                } catch (Exception $e) {
                    $this->logger->error(sprintf('Failed to index entry: %s', $i), ['exception' => $e, 'exceptionMessage' => $e->getMessage()]);
                    continue;
                }
                $i++;
            }
        }
        fclose($fh);
    }
}
