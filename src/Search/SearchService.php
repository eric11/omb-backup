<?php

namespace OhMyBingo\Search;

use Contentful\Core\Resource\ResourceInterface;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use FirstLeads\Infrastructure\Cache\CacheKey\SearchCacheKey;
use FirstLeads\Infrastructure\Cache\CacheService;
use FirstLeads\Infrastructure\Cache\Exception\CacheNotEnabledException;
use FirstLeads\Infrastructure\Cache\Exception\CacheNotFoundException;
use FirstLeads\Infrastructure\Cache\CacheTrait;
use FirstLeads\Infrastructure\Logger\LoggerTrait;
use OhMyBingo\Search\Results\Results;
use Psr\Log\LoggerInterface as Logger;
use Exception;

class SearchService
{
    const DEFAULT_LIMIT = 10000;

    /** @var Client $client */
    protected $client;
    /** @var CacheService $cacheService */
    protected $cacheService;

    use LoggerTrait;
    use CacheTrait;

    public function __construct(CacheService $cacheService, Logger $logger)
    {
//        $hosts = [
//            'host'   => getenv('ELASTICSEARCH_HOST'),
//            'port'   => getenv('ELASTICSEARCH_PORT'),
//            'scheme' => getenv('ELASTICSEARCH_SCHEME'),
//            'user'   => getenv('ELASTICSEARCH_USER'),
//            'pass'   => getenv('ELASTICSEARCH_PASS'),
//            'method' => 'POST',
//        ];
        $hosts = [getenv('ELASTICSEARCH_HOST')];
//        $logger = new Logger('Elasticsearch-Logger');
//        $logger->pushHandler(new StreamHandler('var/log/elasticsearch.log', Logger::WARNING));
        $this->client = ClientBuilder::create()
            ->setHosts($hosts)
            ->setRetries(2)
//            ->setLogger($logger)
            ->build();
        $this->setCacheService($cacheService);
        $this->setLogger($logger);
    }

    /*
     * Gets a document
     */
    public function get($index, $type, $id)
    {
        $getParams = [
            'index' => $index,
            'type'  => $type,
            'id'    => $id
        ];
        return $this->client->get($getParams);
    }

    /*
     * Indexes document are updates documents
     */
    public function indexOrUpdate($index, $type, $id, $body)
    {
        $documentExists = $this->exists($index, $type, $id);
        if ($documentExists) {
            $response = $this->update($index, $type, $id, $body);
            $this->logger->info('Updated search index:', [
                'index' => $index,
                'type'  => $type,
                'id'    => $id,
                'body'  => $body,
                'response' => $response
            ]);
            return $response;
        }
        $response = $this->index($index, $type, $id, $body);
        $this->logger->info('Created search index:', [
            'index' => $index,
            'type'  => $type,
            'id'    => $id,
            'body'  => $body,
            'response' => $response
        ]);
        return $response;
    }

    /*
     * Indexes documents (creates index if doesn't exist)
     */
    public function index($index, $type, $id, $body)
    {
        $indexParams = [
            'index' => $index,
            'type'  => $type,
            'id'    => $id,
            'body'  => $body
        ];
        $response = $this->client->index($indexParams);
        return $response;
    }

    /*
     * Updates documents
     */
    public function update($index, $type, $id, $body)
    {
        $updateParams = [
            'index' => $index,
            'type'  => $type,
            'id'    => $id,
            'body'  => [
                'doc' => $body
            ]
        ];
//        var_dump($updateParams);
        return $this->client->update($updateParams);
    }

    /*
     * Deletes a document
     */
    public function delete($index, $type, $id)
    {
        $deleteParams = [
            'index' => $index,
            'type'  => $type,
            'id'    => $id
        ];
        return $this->client->delete($deleteParams);
    }

    /*
     * Deletes an index
     */
    public function deleteIndex($index)
    {
        $deleteParams = [
            'index' => $index
        ];
        return $this->client->indices()->delete($deleteParams);
    }

    /*
     * Searches for a document.
     */
    public function search($index, $type, $matchField = null, $matchValue = null, $limit = self::DEFAULT_LIMIT)
    {
        $this->logger->info('Search request', [
            'index' => $index,
            'type'  => $type,
            'matchField' => json_encode($matchField, true),
            'matchValue' => json_encode($matchValue, true)
        ]);
        $cacheKey = new SearchCacheKey($index, $type, $matchField, $matchValue);
        try {
            $results = $this->cacheService->get($cacheKey);
            return $results;
        } catch (CacheNotEnabledException | CacheNotFoundException $exception) {
            $this->logger->warning('Cache unavailable', ['cacheKey' => $cacheKey, 'message' => $exception->getMessage()]);
            // continue
        }
        if (empty($matchValue)) {
//            $index =  'article';
//            $type =  'article';
//            dd([$index, $type, $limit]);
            $results = $this->all($index, $type, $limit);
        } else {
            if (is_array($matchField)) {
                $matchQuery = [
                    $matchField[0] => $matchValue[0],
//                    $matchField[1] => $matchValue[1]
                ];
            } else {
                $matchQuery = [
                    $matchField => $matchValue,
                ];
            }
            $searchParams = [
                'index' => $index,
                'type' => $type,
                'size' => $limit,
                'body' => [
                    'query' => [
                        'match' => $matchQuery
                    ]
//                    'query' => [
//                        'bool' => [
//                            'should' => [
//                                'match' => [
//                                    'classification' => 'Casino',
//                                    'status' => 'New'
//                                ],
//                            ]
//                        ]
//                    ]
                ],
            ];
            $response = $this->client->search($searchParams);
            $results = new Results($type, $response);
            if (is_array($matchField)) {
//                $results->filter('name', 'Sea Bingo');
                $filterMatchFieldCount = count($matchField) - 1;
                for( $filterIndex = 1; $filterIndex <= $filterMatchFieldCount; $filterIndex++ ) {
                    $results->filter($matchField[$filterIndex], $matchValue[$filterIndex]);
                }
                // dd($results);
            }
        }
        if ($results->hasErrors()) {
            $this->logger->error('Failed to build the following search results:', $results->getErrors());
        }
        $this->cacheService->set($cacheKey, $results);
        return $results;
    }


    /*
     * Searches for all document.
     */
    public function all($index, $type, $limit = self::DEFAULT_LIMIT)
    {
        $searchParams = [
            'index' => $index,
            'type' => $type,
            'size' => $limit,
        ];
        $response = $this->client->search($searchParams);
        $results = new Results($type, $response);
        if ($results->hasErrors()) {
            $this->logger->error('Failed to build the following search results:', $results->getErrors());
        }
        return $results;
    }

    /*
     * Checks to see if a document exists.
     */
    public function exists(string $index, string $type, string $id): bool
    {
        $existsParams = [
            'index' => $index,
            'type'  => $type,
            'id'    => $id
        ];
        return $this->client->exists($existsParams);
    }
}
