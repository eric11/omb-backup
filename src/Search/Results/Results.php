<?php

namespace OhMyBingo\Search\Results;

use OhMyBingo\Search\Exception\NotSearchableException;
use OhMyBingo\Search\Entry\EntryBuilder;
use ErrorException;
use Exception;

class Results
{
    private $took;
    private $count;
    private $type;
    private $entries = [];
    private $errors = [];

    public function __construct(string $type, array $response)
    {
        $this->type  = $type;
        $this->took  = $response['took'];
        // $this->count = count($response['hits']['hits']);
        foreach ($response['hits']['hits'] as $hit) {
            try {
                $entry = EntryBuilder::build($hit['_source'], $this->type);
                array_push($this->entries, $entry);
            } catch (NotSearchableException | ErrorException $exception) {
                $this->onError($hit['_source'], $exception);
                continue;
            }
        }
        $this->count = count($this->entries);
    }

    public function onError($itemReference, Exception $exception)
    {
        array_push($this->errors, [
            'type'          => $this->type,
            'ref'           => $itemReference,
            'message'       => $exception->getMessage(),
        ]);
    }

    public function hasErrors()
    {
        return (!empty($this->errors)) ? true : false;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getTook()
    {
        return $this->took;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function getEntries()
    {
        return $this->entries;
    }

    public function filter(string $filterField, $filterValue)
    {
        $filteredEntries = [];
        foreach ($this->entries as $entry) {
            if($entry->getData()[$filterField] == $filterValue) {
                array_push($filteredEntries, $entry);
            }
        }
        $this->entries = $filteredEntries;
        $this->count = count($this->entries);
    }
}

/*
    Array
    (
        [took] => 1
        [timed_out] =>
        [_shards] => Array
            (
                [total] => 5
                [successful] => 5
                [failed] => 0
            )

        [hits] => Array
            (
                [total] => 1
                [max_score] => 0.30685282
                [hits] => Array
                    (
                        [0] => Array
                            (
                                [_index] => my_index
                                [_type] => my_type
                                [_id] => my_id
                                [_score] => 0.30685282
                                [_source] => Array
                                    (
                                        [testField] => abc
                                    )
                            )
                    )
            )
    )
 */