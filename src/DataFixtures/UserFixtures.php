<?php

namespace OhMyBingo\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use OhMyBingo\Entity\User;

class UserFixtures extends Fixture
{
    const DEFAULT_ADMIN_PASSWORD = '123';

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('omb');
        $roles[] = 'ROLE_USER';
        $roles[] = 'ROLE_ADMIN';
        $user->setRoles($roles);
        $password = self::DEFAULT_ADMIN_PASSWORD;
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            $password
        ));
        $manager->persist($user);
        $manager->flush();
    }
}
