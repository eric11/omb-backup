<?php

namespace OhMyBingo\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use OhMyBingo\Entity\Comment;
use DateTime;

class CommentFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // BingoPort
        $comment = new Comment();
        $comment->setName('Bryony');
        $datetime = new DateTime("now");
        $comment->setDatetime($datetime);
        $message = 'BingoPort is my favourite bingo site in the world';
        $comment->setMessage($message);
        $comment->setSiteId('3FRYEYjjU7axLbSK0GR7j6'); // BingoPort
        $manager->persist($comment);
        $manager->flush();
    }
}
