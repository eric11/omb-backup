<?php

namespace OhMyBingo\ContentPreview;

use http\Exception\RuntimeException;
use OhMyBingo\Content\ContentService;
use OhMyBingo\Content\Query\Query;

class ContentPreviewService
{
    private $contentService;

    public function __construct(ContentService $contentService)
    {
        $this->contentService = $contentService;
    }

    public function getContentPreview(string $contentType, string $entrySlug)
    {
        $componentType = 'sitesOfTheMonth';

        $contentEntry = $this->contentService->getEntry(Query::get($componentType, $entrySlug));

        if (!$contentEntry) {
            throw new RuntimeException('Content entry not found matching slug:', $entrySlug);
        }
        return $contentEntry;
//        dd($contentEntry);
//        $siteListingEntries = $this->searchService->search('site', 'site', 'slug', $entrySlug);
//        return $siteListingEntries->getEntries()[0]->getAffiliateLink();
    }
}
