#!/usr/bin/env bash

git clean -fd
git reset --hard HEAD
git pull origin staging
git lfs pull origin staging
#GIT_LFS_SKIP_SMUDGE=1 git pull
composer install --optimize-autoloader
contentful space export --config config/contentful.json
#composer build
#composer content:backup
/usr/local/bin/contentful space export --config config/contentful.json
#composer db:create
composer db:migrate
#composer start

yarn install
yarn build

composer cached:clear
composer search:index
# source .env
