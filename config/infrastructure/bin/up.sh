#!/usr/bin/env bash

composer install
yarn install
docker-compose up -f  ../docker-compose.yml -d
composer cached:clear
#composer content:backup
#composer search:index
composer dev
