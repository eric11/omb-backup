#!/usr/bin/env bash

# Install essentials
apt-get install -y g++ make openssl git nano tree

# Install latest node
sudo npm cache clean -f
sudo npm install -g n
sudo n stable


yarn global add contentful-cli

