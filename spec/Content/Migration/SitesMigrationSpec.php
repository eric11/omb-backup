<?php

namespace Spec\OhMyBingo\Content\Migration;

use OhMyBingo\Content\ContentService;
use OhMyBingo\Content\ContentManagementService;
use Psr\Log\NullLogger;
use Symfony\Component\Dotenv\Dotenv;
use OhMyBingo\Content\Migration\SitesMigration;

class SitesMigrationSpec extends \PhpSpec\ObjectBehavior
{
    protected $contentService;

    function let()
    {
        $dotenv = new Dotenv();
        $dotenv->load('.env');
        $logger = new NullLogger();
        $contentManagementService = new ContentManagementService();
        $contentService = new ContentService();
        $this->beConstructedWith($contentManagementService, $contentService, $logger);
        $this->shouldHaveType(SitesMigration::class);
    }

    function it_fetches_all_sites()
    {
        $result = $this->run();
//        $this->beConstructedWith($this->exampleNumericMonth);
//        $this->days()->shouldBeEqualTo($this->specDaysInMonth($this->exampleNumericMonth));
    }

}