#!/usr/bin/env bash

# Git
sudo yum install git -y

# Git LFS
git-lfs
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | sudo bash

# Docker
sudo yum install docker -y
sudo service docker start
sudo usermod -a -G docker ec2-user


# Docker Compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Copy database
# scp -i "~/.ssh/OhMyBingo-Staging.pem" ./docker/db/files/localhost.sql ec2-user@18.221.25.96:/home/ec2-user/oh-my-bingo/docker/db/files/