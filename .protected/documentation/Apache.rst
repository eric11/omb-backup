Apache
======

We use Symfony Apache pack to configure the web server:

$ composer require symfony/apache-pack

Add vhost to sites-available:

Enable the site:

sudo a2ensite staging.ohmybingo.com
apachectl configtest


# Disable default site
a2dissite default


# List available hosts
apache2ctl -S


/etc/init.d/apache2 restart
