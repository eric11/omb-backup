
https://www.modernjsforphpdevs.com/react-symfony-4-starter-repo/

----\}}


https://github.com/tesonet/react-js-twig
https://github.com/phpv8/v8js/blob/master/README.Linux.md
https://mathieuhays.co.uk/how-to-install-v8js-for-php-on-mac-os-x/

==

https://coolestguidesontheplanet.com/installing-pear-on-osx-10-11-el-capitan/

==

/etc/php.ini

extension=v8js.so
brew install php73 --with-pear


====

PROPER:
https://andygrunwald.com/blog/migrate-your-local-php-7.2-setup-to-homebrew-v1.5./

brew install php@7.3
php --version, php --ini and pear config-show | grep php.ini

pecl install xdebug
pecl install redis
pecl install apcu
pecl install memcached
pecl install imagick

pecl install v8js-beta
 vi /etc/php.ini
extension=v8js.so
php -i | grep v8js

===

APACHE
https://tecadmin.net/install-apache-macos-homebrew/

sudo apachectl stop
sudo launchctl unload -w /System/Library/LaunchDaemons/org.apache.httpd.plist

brew install httpd
sudo brew services start httpd

sudo vi /private/etc/apache2/httpd.conf

Uncomment this line:

LoadModule php5_module libexec/httpd/libphp5.so
Add these lines in the httpd.conf to teach apache to handle .php requests:

<IfModule mod_php5.c>
    # If php is turned on, we respect .php and .phps files.
    AddType application/x-httpd-php .php
    AddType application/x-httpd-php-source .phps

    # Since most users will want index.php to work we
    # also automatically enable index.php
    <IfModule mod_dir.c>
        DirectoryIndex index.html index.php
    </IfModule>
</IfModule>
Save the file, restart Apache:

$ sudo apachectl graceful
or just start it if it isn't running:

$ sudo apachectl start



====


export PATH=/usr/local/Cellar/php/7.3.3/bin:$PATH

====

Mac Mojave Pear fix - use this: https://objects-us-east-1.dream.io/kbfiles/pear/go-pear.phar

