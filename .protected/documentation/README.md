Oh My Bingo
===========

Development
-----------

You can run the application locally using Symfony's web server:

  $ php bin/console server:run
  
List all routes:

  $ php bin/console debug:router

Debug templates:
  $ php bin/console debug:twig
  
Docker
------

Docker is used to provision local and staging development environment.

First create the network:

  $ docker network create firstleads_ohmybingo

Now run docker-compose up.

  $ docker-compose up db
  $ docker-compose up site
  
Add the following hosts:
  
  0.0.0.0 ohmybingo.local  

You should now be able to browse to:

  http://ohmybingo.local
  
  
Staging
-------

A staging server is available.

IP: 18.221.25.96
SSH Key: OhMyBingo-Staging.pem

To connect:

  $ ssh -i "~/.ssh/OhMyBingo-Staging.pem" ec2-user@18.221.25.96

Encore
------

- https://symfony.com/doc/current/frontend/encore/simple-example.html#requiring-javascript-modules

Encore is used to compile assets:

**Compile assets once:**

  $ yarn encore dev

**Watch and recompile assets:**

  $ yarn encore dev --watch

**Create a production build:**

  $ yarn encore production
  
Encore is configured with HRM (Hot Module Replacement):

  - https://symfony.com/doc/current/frontend/encore/dev-server.html
  
**HRM:**
$ composer dev
$ composer hot
 
