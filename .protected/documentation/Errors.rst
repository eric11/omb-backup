======
Errors
======

Custom error pages  implemented via config/twig.yaml: https://symfony.com/doc/current/controller/error_pages.html

Test Error pages:

http://127.0.0.1:8000/index.php/_error/404
http://127.0.0.1:8000/index.php/_error/404.json
