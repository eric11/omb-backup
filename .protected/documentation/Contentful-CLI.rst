==============
Contentful CLI
==============

$ yarn global add contentful-cli

$ contentful-config.json


Export contentful content
=========================

$ contentful space export --config config/contentful-config.json
