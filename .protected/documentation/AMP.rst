===
AMP
===

AMP Test: https://search.google.com/test/amp

Documentation: https://amp.dev/documentation/examples/components/amp-carousel/?referrer=ampbyexample.com

Provide AMP Urls and use the AMP API: https://amp.dev/documentation/guides-and-tutorials/integrate/integrate-with-apps?format=websites

Convert HTML to AMP:  https://amp.dev/documentation/guides-and-tutorials/start/converting/


AMP Validator
-------------

https://amp.dev/documentation/guides-and-tutorials/start/converting/building-page?format=websites