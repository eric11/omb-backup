brew services stop httpd

====
DocumentRoot is /usr/local/var/www.

The default ports have been set in /usr/local/etc/httpd/httpd.conf to 8080 and in
/usr/local/etc/httpd/extra/httpd-ssl.conf to 8443 so that httpd can run without sudo.

To have launchd start httpd now and restart at login:
  brew services start httpd
Or, if you don't want/need a background service you can just run:
  apachectl start

==


To enable PHP in Apache add the following to httpd.conf and restart Apache:
    LoadModule php7_module /usr/local/opt/php/lib/httpd/modules/libphp7.so

    <FilesMatch \.php$>
        SetHandler application/x-httpd-php
    </FilesMatch>

Finally, check DirectoryIndex includes index.php
    DirectoryIndex index.php index.html

The php.ini and php-fpm.ini file can be found in:
    /usr/local/etc/php/7.3/

To have launchd start php now and restart at login:
  brew services start php
Or, if you don't want/need a background service you can just run:
  php-fpm

/usr/local/opt/v8/
==

https://gist.github.com/fernandoaleman/868b64cd60ab2d51ab24e7bf384da1ca

brew tap homebrew/versions
brew install v8-315

sudo gem install libv8 -v '3.16.14.13' -- --with-system-v8
sudo gem install therubyracer -- --with-v8-dir=/usr/local/opt/v8-315

bundle install
