Google Rich Snippet
===================

Review with ratings
-------------------

https://developers.google.com/search/docs/data-types/review-snippet

The format we use is: Review with Ratings (json-ld)

===========================

**Whichbingo version:**

<script type="application/ld+json">
    {
        "@context": "http://schema.org/",
        "@type": "Review",
        "datePublished": "2018-05-14 18:50:51",
        "dateModified": "2019-03-01 14:11:19",
        "itemReviewed": {
            "@type": "Thing",
            "name": "Robin Hood Bingo",
            "image": "https://www.whichbingo.co.uk/wp-content/uploads/2018/05/robin-hood-bingo-logo.png"
        },
        "author": {
            "@type": "Organization",
            "name": "https://www.whichbingo.co.uk"
        },
        "reviewRating": {
            "@type": "Rating",
            "bestRating": "5",
            "worstRating": "0",
            "ratingValue": "4"
        }
    }
</script>

=========================

**OMB version:**
<script type="application/ld+json">
    {
        "@context": "http://schema.org/",
        "@type": "Review",
        "datePublished": "2018-05-14 18:50:51",
        "dateModified": "2019-03-01 14:11:19",
        "itemReviewed": {
            "@type": "Thing",
            "name": "Robin Hood Bingo",
            "image": "https://www.whichbingo.co.uk/wp-content/uploads/2018/05/robin-hood-bingo-logo.png"
        },
        "author": {
            "@type": "Organization",
            "name": "https://www.whichbingo.co.uk"
        },
        "reviewRating": {
            "@type": "Rating",
            "bestRating": "5",
            "worstRating": "0",
            "ratingValue": "4"
        }
    }
</script>

