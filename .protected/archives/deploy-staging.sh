#!/usr/bin/env bash

# Git LFS Pull

# In production we enter username and password:
#cd to directory
# git init

# git pull origin staging --allow-unrelated-histories
git pull
git lfs pull

# Install composer dependencies
composer install --no-dev --optimize-autoloader

# Install npm dependencies
yarn install
yarn build

# Check Elasticsearch is up


# Start Elasticsearch automatically
# https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-elasticsearch-on-ubuntu-16-04
# sudo systemctl enable elasticsearch.service

# Configure Apache: https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-ubuntu-18-04-quickstart

sudo ufw app list
