#!/usr/bin/env bash

# Connect to staging
#ssh -i ".protected/secret/OhMyBingo-Staging.pem" ec2-user@18.221.25.96
ssh -i "~/.ssh/OhMyBingo-Staging.pem" ec2-user@18.221.25.96

# Start Symfony web server
composer install
yarn install
yarn encore production
php bin/console server:run *:8080

# You can now browse to: 18.221.25.96:8080 or http://ohmybingo.staging:8080 if added to /etc/hosts
