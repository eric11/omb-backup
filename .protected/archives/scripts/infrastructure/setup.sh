#!/usr/bin/env bash

# https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/install-LAMP.html

# Update
sudo yum update -y

# Install Git
sudo yum install git

# Remove existing PHP
sudo yum remove php* httpd*
sudo yum clean all
sudo yum update -y


# Install PHP 7.2
sudo yum install -y httpd24 php72 mysql56-server php72-mysqlnd
sudo yum install php72-mysqli php72-pdo php72-pdo_mysql php72-mbstring php72-shmop php72-calendar php72-ctype php72-exif php72-fileinfo php72-gettext

# Start Apache
#sudo service httpd start
#sudo chkconfig httpd on
#chkconfig --list httpd

# Install Composer for Local User
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
php -r "unlink('composer-setup.php');"

# Install NVM
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
# Exit terminal and enter again

# Install Node
nvm install node
node --version
# v11.12.0

# Install Yarn
curl -o- -L https://yarnpkg.com/install.sh | bash
# Exit terminal and enter again

# Install Yarn
# https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-elasticsearch-on-ubuntu-16-04

