const $ = require('jquery');

export class Header {
    constructor() {
        this.selector = {
            navigation: '#navigation',
        };
        this.currentOpenedMenu = null;
        // this.showingSearchBarMobile = false;
        this.button = {
            menu: $("#menu--mobile"),
            search: $("#search--mobile"),
        };
        this.navigation = $(this.selector.navigation);
    }
    init() {
        if(!$(this.selector.navigation).length){
            console.warn('Not Found', this.constructor.name);
            return false;
        }
        const that = this;
        // this.header.toggle();
        console.debug('Header initialised');
        /* Show search bar when user clicks the search icon */
        this.button.menu.click(function(){
            console.debug('showing the mobile menu');
            // Change class of button to active
            that.button.menu.toggleClass('active');
            that.navigation.toggle();
        });
        /* Show search bar when user clicks the search icon */
        this.button.search.click(function(){
            console.debug('toggling the state');
            $("#header__search__container--mobile").toggle();
            $("#search__form__query--mobile").focus();
        });

    }
}
