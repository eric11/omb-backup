import {Pagination} from "../Pagination/Pagination";

const $ = require('jquery');

export class SiteListings {
    constructor(container, loaderPagination) {
        this.container  = container;
        this.pagination = new Pagination(container, loaderPagination);
    }
    init() {
        if(!$(this.container).length){
            console.warn('Not Found', this.constructor.name);
            return false;
        };
        console.debug(this.constructor.name, 'initialised');
        this.pagination.init();
    }
}
