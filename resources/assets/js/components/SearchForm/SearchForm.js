const $ = require('jquery');

/*
 * Search Form
 *
 */
export class SearchForm {

    constructor(form, inputField) {
        this.form = form;
        this.inputField = inputField;
    }
    init() {
        if(!$(this.form).length){
            console.warn('Not Found', this.constructor.name);
            return false;
        }
        console.debug('Search Form initialised');
        const that = this;
        $(this.inputField).keyup(function (e) {
            if (e.which == 13) {
                $(that.form).submit();
                return false;
            }
        });

        /* On mouseenter - slide open menu */
        $(this.form).mouseleave(function(event){
            // $('#search__form__query').hide();
            const closeAutocomplete = function () {
                const elmnt = 'search__form__query';
                const x = document.getElementsByClassName("autocomplete__items");
                for (let i = 0; i < x.length; i++) {
                    // if (elmnt != x[i] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                    // }
                }
            };
            closeAutocomplete();
            console.log('User left searchbox');
        });
    }
}
