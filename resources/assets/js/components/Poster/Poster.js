const $ = require('jquery');
require('webpack-jquery-ui');

export class Poster {
    constructor(container) {
        this.container = container;
    }
    init() {
        if(!$(this.container).length){
            console.warn('Not Found', this.constructor.name);
            return false;
        }
        console.debug('Poster initialised');
        console.debug('Poster found');
        /* On mouseenter - hide open menu */
        // $(this.container).mouseenter(function () {
        //     console.log('entered poster');
        //     $(this).addClass('stick');
        // });
        let poster      = document.getElementById("poster"),
            // -60 so it won't be jumpy
            stop      = poster.offsetTop + 300,// - 300,
            docBody   = document.documentElement || document.body.parentNode || document.body,
            hasOffset = window.pageYOffset !== undefined,
            scrollTop;

        window.onscroll = function (e) {
            // Mobile
            if ($(window).width() <= 800) {
                console.log('mobile poster scroll skip');
            } else {
                console.log('window scroll');
                // cross-browser compatible scrollTop.
                scrollTop = hasOffset ? window.pageYOffset : docBody.scrollTop;

                // if user scrolls to 60px from the top of the left div
                if (scrollTop >= stop) {
                    // stick the div
                    poster.className = 'stick';
                    console.log('set stick poster class name', scrollTop, stop);
                } else {
                    // release the div
                    poster.className = 'no-stick';
                    console.log('removed stick poster class name - no-stick');
                }
            }
        };

        console.log('Added stick class to poster');
    }
}
