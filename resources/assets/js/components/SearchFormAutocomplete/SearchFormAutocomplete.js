const $ = require('jquery');
import autocomplete from './autocomplete';

/*
 * Search Form Autocomplete
 *
 * @link https://api.jqueryui.com/autocomplete
 * @link https://github.com/scottgonzalez/jquery-ui-extensions/blob/master/demo/autocomplete/html.html
 */
export class SearchFormAutocomplete {

    constructor(form, input) {
        this.autoSuggestions = null;
        this.form = form;
        this.input = input;
        // this.source = [
        //     {
        //         value: "Free Bingo Sites",
        //         category: "*category"
        //     },
        //     {
        //         value: "Foxy Bingo",
        //         category: "*category"
        //     },
        // ];
    }

    init() {
        console.debug('Autocomplete initialised');

        const that = this;

        $.ajax({
            method: "GET",
            url: "/api/auto-suggestions"
        })
        // $.ajax({
        //     method: "GET",
        //     url: "api/auto-suggestions",
        //     dataType: "json",
        //     // headers: { "apiKey": "123" }
        // })
        .done(function( autoSuggestions ) {
            that.autoSuggestions = autoSuggestions;
            console.log( "Autosuggestions retrieved: ", that.autoSuggestions);
            autocomplete(document.getElementById(that.form),  document.getElementById(that.input), that.autoSuggestions);
        });

    }
}
