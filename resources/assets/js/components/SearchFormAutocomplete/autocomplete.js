function autocomplete(form,  inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) {
            return false;
        }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "--autocomplete-list");
        a.setAttribute("class", "autocomplete__items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        let currentItem = {
            value: null,
            category: null,
        };
        let labelDiv = null;
        let categoryDiv = null;
        let count = 0;
        let ticker = 0;
        for (i = 0; i < arr.length; i++) {
            /* First get the label */
            currentItem.value = arr[i].value;
            currentItem.category = arr[i].category;

            /*check if the item starts with the same letters as the text field value:*/
            if (currentItem.value.substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                b.setAttribute('class', 'autocomplete__item');
                /*make the matching letters bold:*/

                /* Create the label */
                labelDiv = document.createElement("DIV");
                labelDiv.setAttribute('class', 'autocomplete__item__label');
                labelDiv.innerHTML = currentItem.value.substr(0, val.length);
                labelDiv.innerHTML += currentItem.value.substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                labelDiv.innerHTML += "<input type='hidden' value='" + currentItem.value + "'>";
                // hiddenElement.innerHTML += "<span data-ref-search-autocomplete-value='hidden'>" + currentItem.value + "</span>";
                // b.appendChild(hiddenElement);
                b.appendChild(labelDiv);

                /* Create the category */
                categoryDiv = document.createElement("DIV");
                categoryDiv.setAttribute('class', 'autocomplete__item__category');
                categoryDiv.innerHTML = currentItem.category;
                b.appendChild(categoryDiv);

                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    form.submit();
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);

                /* Set bg color depending on odd or even (ticker = 0/1) */
                if (ticker === 0) {
                    b.classList.add("odd");
                    ticker = 1;
                } else {
                    b.classList.add("even");
                    ticker = 0;
                }

                /* If the first in the list then activate */
                count ++;
                if (count === 1) {
                    b.classList.add("autocomplete__item--active");
                }
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "--autocomplete-list");
        if (x) x = x.getElementsByClassName("autocomplete__item");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });

    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        if (x[currentFocus] === undefined) {
            console.error('Unable to add active as current focus is undefined')
        } else {
            x[currentFocus].classList.add("autocomplete__item--active");
        }

    }

    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete__item--active");
        }
    }

    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete__items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
}

export default autocomplete;
