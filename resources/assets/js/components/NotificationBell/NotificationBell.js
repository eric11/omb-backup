const $ = require('jquery');
require('webpack-jquery-ui');

export class NotificationBell {
    constructor(container, popup) {
        this.open = false;
        this.opening = false;
        this.container = container;
        this.popup = popup;
        this.selector = {
            container: container,
            popup: popup,
        };
    }
    init() {
        if(!$(this.container).length){
            console.warn('Not Found', this.constructor.name);
            return false;
        }
        console.debug('Notifications initialised');
        const that = this;

        // add class notification-bell__icon
        // function explode(){
        //     console.log('shaking notification bell');
        //     // $('#notification-bell__icon').removeClass('animated').addClass('animated');
        // }
        // console.log('shaking notification bell');
        // $('#notification-bell__icon').removeClass('animated').addClass('animated');
        // setTimeout(explode, 200);

        /* Close the notifications tab when user clicks outside */
        if (this.open) {
            window.onclick = function(event) {
                const targetClassName = event.target.className;
                console.log(targetClassName);
                if (!targetClassName.includes("notification-bell__icon")) {
                    $(that.popup).hide(0, function () {
                        that.open = false;
                    });
                }
            };
        }

        /* Notification Bell */
        $(this.container).click(function(){
            console.debug('Notification clicked');
            if (!that.open) {
                if (!that.opening) {
                    console.debug('Opened Notifications');
                    this.opening = true;
                    $(that.popup)
                        .stop(true, true)
                        .fadeIn({ duration: 400, queue: false })
                        .css('display', 'none')
                        .slideDown({
                            duration: 600,
                            easing: "easeOutQuint",
                            complete: function () {
                                console.log('Finished opening');
                                that.opening = false;
                                that.open = true;
                            }
                        });
                        // .slideDown(1500, function () {
                        //     console.log('Finished opening');
                        //     that.opening = false;
                        //     that.open = true;
                        // });
                    // $("#notification-bell .notification-bell__popup").show("slide", {
                    //     direction: "down",
                    //     easing: "easeOutQuint",
                    // }, 1500, function () {
                    //     console.log('Finished up');
                    //     that.opening = false;
                    //     that.open = true;
                    // });
                }
            } else {
                console.debug('Closed Notifications');
                $(that.popup).hide(0, function () {
                    console.debug('Finished closing');
                    that.open = false;
                });
                // $("#notification-bell .notification-bell__popup")
                //     .stop(true, true)
                //     .css('display', 'none')
                //     .slideDown(1500);
                that.open = false;
            }
        });
    }
}
