const $ = require('jquery');

/*
 * Comments Form
 *
 */
export class CommentForm {

    constructor() {
        this.selector = {
            form: '#comments__form',
            response: '#comments__response',
            nameField: '#comment-name',
            nameValidation: '#comment-name__validation',
            messageField: '#comment-message',
            messageValidation: '#comment-message__validation',
        };
    }
    init() {
        console.debug('Comment Form initialised');
        const that = this;
        // $(this.selector.inputField).keyup(function (e) {
        //     if (e.which == 13) {
        //         $(that.selector.form).submit();
        //         return false;
        //     }
        // });
        $(this.selector.form).submit(function( event ) {
            event.preventDefault();
            const siteId = $(that.selector.form).data('site-id');
            const nameValue = $(that.selector.nameField).val();
            const messageValue = $(that.selector.messageField).val();
            let nameError = null;
            let messageError = null;
            let serverError = null;
            if (!nameValue) {
                nameError = 'Please enter your name';

            }
            if (!messageValue) {
                messageError = 'Please enter your comment';
            }
            if (nameError) {
                $(that.selector.nameValidation).text(nameError).css('display', 'inline-block');
            } else {
                $(that.selector.nameValidation).text(nameError).css('display', 'none');
            }
            if (messageError) {
                $(that.selector.messageValidation).text(messageError).css('display', 'inline-block');
            } else {
                $(that.selector.messageValidation).text(messageError).css('display', 'none');
            }
            if (nameError || messageError) {
                return false;
            } else {
                // submit the comment to api
                $.ajax({
                    method: "POST",
                    url: "api/comments",
                    data: {
                        name: nameValue,
                        message: messageValue,
                        siteId: siteId
                    }
                })
                .done(function( response ) {
                    console.debug( "Comment saved: ", response);
                    if (response.status === 'ok') {
                        console.debug('Comment submit response:', response);
                        $(that.selector.form).hide();
                        $(that.selector.response).show();
                        $(that.selector.response).html("<p><strong>Thank You</strong></p><p>Your comment has been submitted.</p>");
                    }
                });
            }
            // alert( "Handler for .submit() called." );

        });
    }
}
