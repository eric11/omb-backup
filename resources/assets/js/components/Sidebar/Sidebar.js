const $ = require('jquery');

export class Sidebar {
    constructor(container) {
        this.container = container;
        this.button = {
            bingo: $("#sidebar__button--bingo"),
            slots: $("#sidebar__button--slots"),
            news:  $("#sidebar__button--news"),
            help:  $("#sidebar__button--help"),
        };
        this.container = {
            bingo: $("#sidebar__editors-choice--bingo"),
            slots: $("#sidebar__editors-choice--slots"),
            news:  $("#sidebar__news-and-help--news"),
            help:  $("#sidebar__news-and-help--help"),
        };
        this.selected = {
            bingo: true,
            slots: false,
            news:  true,
            help:  false,
        };
    }
    init() {
        if(!$(this.container).length){
            console.warn('Not Found', this.constructor.name);
            return false;
        }
        console.debug('Sidebar menu initialised');
        /* Bingo */
        const that = this;
        this.button.bingo.click(function(){
            if (!that.selected.bingo) {
                console.debug('You clicked the bingo button');
                that.selected.bingo = true;
                that.selected.slots = false;
                that.container.slots.hide();
                that.container.bingo.show();
                that.button.bingo.removeClass('unselected');
                that.button.bingo.addClass('selected');
                that.button.slots.removeClass('selected');
                that.button.slots.addClass('unselected');
            }
        });
        /* Slots */
        this.button.slots.click(function(){
            if (!that.selected.slots) {
                console.debug('You clicked the slots button');
                that.selected.bingo = false;
                that.selected.slots = true;
                that.container.bingo.hide();
                that.container.slots.show();
                that.button.bingo.removeClass('selected');
                that.button.bingo.addClass('unselected');
                that.button.slots.removeClass('unselected');
                that.button.slots.addClass('selected');
            }
        });
        /* News */
        this.button.news.click(function(){
            if (!that.selected.news) {
                console.debug('You clicked the news button');
                that.selected.news = true;
                that.selected.help = false;
                that.container.news.show();
                that.container.help.hide();
                that.button.news.removeClass('unselected');
                that.button.news.addClass('selected');
                that.button.help.removeClass('selected');
                that.button.help.addClass('unselected');
            }
        });
        /* Help */
        this.button.help.click(function(){
            if (!that.selected.help) {
                console.debug('You clicked the help button');
                that.selected.news = false;
                that.selected.help = true;
                that.container.news.hide();
                that.container.help.show();
                that.button.news.removeClass('selected');
                that.button.news.addClass('unselected');
                that.button.help.removeClass('unselected');
                that.button.help.addClass('selected');
            }
        });
    }
}
