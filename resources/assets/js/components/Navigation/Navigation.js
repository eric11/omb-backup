const $ = require('jquery');
const empty = require('is-empty');


// const { MobileNavigationMenu } = require('./MobileNavigationMenu');

export class Navigation {
    constructor() {
        this.container = '#navigation';
        this.desktopMenu = null;
        this.activeDesktopMenu = null;
        this.mobileMenu = null;
        this.activeMobileMenu = null;
        this.mobileBreakpoint = 812; // Cover iphone X - horizontal

        // this.mobileNavigationMenu = new MobileNavigationMenu();
    }
    init() {
        if(!$(this.container).length){
            console.warn('Not Found', this.constructor.name);
            return false;
        }
        console.debug('Navigation menu initialised');
        const that = this;

        /* On click - slide open menu */
        $("#navigation > ul > li.navigation__menu__item").click(function(event){
            // Mobile
            if ($(window).width() <= that.mobileBreakpoint) {

                that.mobileMenu = $(this).find("> div");
                // that.mobileNavigationMenu.onClick(event, that);

                that.mobileMenu = $(this).find("> div");
                if (event.target.className !== 'navigation__item__link') {
                    const targetHref = $(event.target).attr("href");
                    if(!empty(targetHref)) {
                        window.location.href = $(event.target).attr("href");
                        event.preventDefault();
                        return false;
                    }
                } else {
                    // set parent selected
                }

                // Link Ref
                const parent = $(event.target).parent();
                const parentClass = parent.attr('class');
                console.info('parentClass', parentClass);
                // if (parentClass === 'navigation__menu__item') {
                    console.info('highlyt case');
                // }
                // const linkHref = '';
                // console.log('Link href', linkHref);

                // Mobile
                const menuRef = $(this).data("ref");
                const menuChildren = $(this).data("children");
                if (!menuChildren) {
                    window.location.href = $(event.target).attr("href");
                    event.preventDefault();
                    return false;
                }

                const dataSelected = $(this).data("selected");

                // Open
                if (that.activeMobileMenu === menuRef) {
                    if (!dataSelected) {
                        parent.removeClass('selected');
                    }
                    that.mobileMenu.hide();
                    that.activeMobileMenu = null;
                    event.preventDefault();
                    return false;
                    // Close
                } else {
                    if (!dataSelected) {
                        parent.addClass('selected');
                    }
                    that.mobileMenu.show();
                    that.activeMobileMenu = menuRef;
                    event.preventDefault();
                    return false;
                }
            } else {
                // Desktop
                // console.debug('Desktop menu  click');
            }
        });

        // $("#navigation > ul > li a.navigation__item__link").click(function(event){
        //     if ($(window).width() <= 800) {
        //         // Mobile
        //         console.debug('clicked mob link');
        //     }
        // });
        /* On click - slide open menu */
        // $("#navigation > li a.navigation__item__link").click(function(event){
        //     console.log('click nav item');
        //     // Mobile
        //     if ($(window).width() <= 800) {
        //     } else {
        //         // Desktop
        //         // console.log('Desktop menu a click');
        //         // event.preventDefault();
        //         // window.location.href = $(this).find("> a:first-child").attr("href");
        //         // return false;
        //     }
        // });


        /* On click - slide open menu */
        // $("#navigation > ul > .navigation__menu__link").click(function(event){
        //     console.log('Desktop submenu a click');
        // });

        /* On mouseenter - slide open menu */
        $("#navigation > ul > li").mouseenter(function(event){
            // console.log('hover on menu')
            // Mobile
            if ($(window).width() <= that.mobileBreakpoint) {
                // Mobile
                // console.debug('mouseover mob item');
            } else {
                // console.debug('mouseover desktop over');
                // Desktop
                const desktopMenu = $(this).find("> div");
                const menuRef = $(this).data("ref");
                // Hide if already open
                if (that.activeDesktopMenu === menuRef) {
                    // console.debug('hid already open menu:' + menuRef);
                    that.desktopMenu.hide();
                    that.desktopMenu = null;
                    that.activeDesktopMenu = null;
                    event.preventDefault();
                }
                // Show if first time
                if (that.desktopMenu === null) {
                    // console.debug('Showing menu for first time:' + menuRef);
                    that.desktopMenu = $(this).find("> div");
                    // that.desktopMenu.show();
                    that.desktopMenu.css("display", "flex");
                    that.activeDesktopMenu = menuRef;
                    event.preventDefault();
                    // Hide others and show
                } else {
                    // console.debug('Showing menu for nth time:' + menuRef);
                    that.desktopMenu.hide();
                    that.desktopMenu = $(this).find("> div");
                    // that.desktopMenu.show();
                    that.desktopMenu.css("display", "flex");
                    that.activeDesktopMenu = menuRef;
                    event.preventDefault();
                }
            }
        });


        /* On mouseenter - show open menu */
        $(".navigation__menu").mouseenter(function () {
            if (that.desktopMenu) {
                that.desktopMenu.show();
            }
        });

        /* On mouseleave - hide open menu */
        $(".navigation__menu").mouseleave(function () {
            // console.log('Left meny submenu', that.desktopMenu);
            // $(this).hide();
            if (that.desktopMenu) {
                that.desktopMenu.hide();
                // that.desktopMenu = null;
                // that.activeDesktopMenu = null;
            }
        });
        $(".navigation__menu--double").mouseleave(function () {
            console.log('Left meny submenu  double', that.desktopMenu);
            // $(this).hide();
            if (that.desktopMenu) {
                that.desktopMenu.hide();
                // that.desktopMenu = null;
                // that.activeDesktopMenu = null;
            }
        });
        /* Set timeout to close window */
    }

}
