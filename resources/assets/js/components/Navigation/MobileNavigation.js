const $ = require('jquery');

export class MobileNavigation {
    constructor() {

    }

    init() {
    }

    onClick(event, navigation) {
        console.log(event, navigation);
        console.log(navigation.mobileMenu);
        navigation.mobileMenu = $(this).find("> div");
        if (event.target.className !== 'navigation__item__link') {
            window.location.href = $(event.target).attr("href");
            event.preventDefault();
            return false;
        }

        // Mobile
        const menuRef = $(this).data("ref");

        // Open
        if (navigation.activeMobileMenu === menuRef) {
            navigation.mobileMenu.hide();
            navigation.activeMobileMenu = null;
            event.preventDefault();
            return false;
            // Close
        } else {
            navigation.mobileMenu.show();
            navigation.activeMobileMenu = menuRef;
            event.preventDefault();
            return false;
        }
    }
}
