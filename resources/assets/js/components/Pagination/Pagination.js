const $ = require('jquery');
const InfiniteScroll = require('infinite-scroll'); // https://www.npmjs.com/package/infinite-scroll

/*
 * global.jQuery = $;
 * global.$ = $;
 * require('jquery-lazy');
 */

export class Pagination {
    constructor(container, loader) {
        this.container = container;
        this.loader    = loader;
    }
    init() {
        if(!$(this.container).length){
            console.warn('Not Found', this.constructor.name);
            return false;
        };
        this.totalPages = $(this.container).data("pagination-total-pages"),
            this.currentPage = $(this.container).data("pagination-current-page"),
            this.nextPage = $(this.container).data("pagination-next-page")
        ;
        console.info({
            tp: this.totalPages,
            cp: this.currentPage,
            np: this.nextPage,
        });
        if (!this.nextPage) {
            this.hideLoader();
            return false;
        }
        const that = this;
        const infiniteScroll = new InfiniteScroll( this.container, {
            path: function() {
                const targetPageNumber = this.loadCount + 2;
                const totalPages = $(that.container).data("pagination-total-pages"),
                    currentPage = $(that.container).data("pagination-current-page"),
                    nextPage = $(that.container).data("pagination-next-page")
                ;
                if (targetPageNumber > totalPages) {
                    return false;
                }
                console.log('Trying to get targetPageNumber:' + targetPageNumber, totalPages);
                if (targetPageNumber > totalPages) {
                    console.debug('No more pages!');
                    that.hideLoader();
                }

                return Pagination.getPath(targetPageNumber);
            },
            append: '.listing',
            checkLastPage: true,
            prefill: false,
            responseType: 'document',
            outlayer: false,
            scrollThreshold: 1000, // 400
            elementScroll: false,
            loadOnScroll: true,
            history: 'replace',
            historyTitle: true,
            onInit: function() {
            },
            debug: false,
        });
        infiniteScroll.on( 'scrollThreshold', function () {
            console.debug('reached threshold');
            that.showLoader();
        });
        infiniteScroll.on( 'request',  function (path) {
            that.showLoader();
            console.debug('making request:', path);
        });
        infiniteScroll.on( 'load',  function (response, path) {
            console.debug('loaded path:', path);
            console.debug('loaded response:', response);
            that.hideLoader();
        });
        infiniteScroll.on( 'append',  function (response, path, items) {
            console.debug('appended response:', response);
            console.debug('appended items:', items);
            console.debug('appended path:', path);
        });
        infiniteScroll.on( 'error',  function (error, path) {
            that.hideLoader();
            console.debug('error:', error);
            console.debug('error path:', path);
        });
        infiniteScroll.on( 'last',  function (response, path) {
            console.debug('last response:', response);
            console.debug('last path:', path);
        });
        infiniteScroll.on( 'history',  function (title, path) {
            console.debug( 'History changed to: ' + path );
        });
    }
    static getPath(pageNumber) {
        const path = '?page=' + ( ( pageNumber));
        console.debug('returning path:', path);
        return path;
    }
    hideLoader() {
        $(this.loader).hide();
    }
    showLoader() {
        $(this.loader).show();
    }
}
