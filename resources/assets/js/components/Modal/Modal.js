const $ = require('jquery');
require('webpack-jquery-ui');

export class Modal {
    constructor() {
        this.selector = {
            container: "#modal-site-collection",
            button: ".modal__button",
            overlay: ".modal__overlay",
            cancelButton: ".modal__button-cancel",
            closeButton: ".modal__content__close-button",
        };
    }
    init() {
        const that = this;
        if(!$(this.selector.container).length){
            console.warn('Modal not found');
            return;
        }
        $(this.selector.button).on("click", function(event) {
            console.log('opened popup');
            $(that.selector.container).show();
            // $( that.selector.container ).dialog({
            //     resizable: false,
            //     // height: "auto",
            //     // width: 400,
            //     modal: true,
            //     // buttons: {
            //     //     "Delete all items": function() {
            //     //         $( this ).dialog( "close" );
            //     //     },
            //     //     Cancel: function() {
            //     //         $( this ).dialog( "close" );
            //     //     }
            //     // }
            // });
            event.preventDefault();
            return false;
        });
        $(this.selector.closeButton).on("click", function(event) {
            console.debug('Closed popup via closeButton');
            $(that.selector.container).hide();
            event.preventDefault();
            return false;
        });
        $(this.selector.cancelButton).on("click", function(event) {
            console.debug('Closed popup via cancelButton');
            $(that.selector.container).hide();
            event.preventDefault();
            return false;
        });
        $(this.selector.overlay).on("click", function(event) {
            console.debug('Closed popup via overlay');
            $(that.selector.container).hide();
        });
        $(this.selector.cancel).on("click", function(event) {
            console.debug('Closed popup via cancel');
            $(that.selector.container).hide();
        });
        console.debug('Modal initialised');
    }
    // open() {
    //     const that = this;
    //     $( this.selector.container ).dialog({
    //         resizable: false,
    //         height: "auto",
    //         width: 400,
    //         modal: true,
    //         open: false
    //         // buttons: {
    //         //     "Delete all items": function() {
    //         //         $( this ).dialog( "close" );
    //         //     },
    //         //     Cancel: function() {
    //         //         $( this ).dialog( "close" );
    //         //     }
    //         // }
    //     });
    // }
}
