/* Main JavaScript */

// Require Sass
require('../css/app.scss');

// Require jQuery
const $ = require('jquery');
window.$ = $;
require('bootstrap');

// global.$ = global.jQuery = $;
// global.$.lazy = global.jQuery = $;
// require('./lib/jquery/jquery.ui.autocomplete.html.js');

// Require Components
const { Header } = require('./components/Header/Header');
const { Navigation } = require('./components/Navigation/Navigation');
const { Sidebar } = require('./components/Sidebar/Sidebar');
// const { Sidebar } = require('./components/Sidebar/Sidebar');
const { SearchFormAutocomplete } = require('./components/SearchFormAutocomplete/SearchFormAutocomplete');
const { SearchForm } = require('./components/SearchForm/SearchForm');
const { NotificationBell } = require('./components/NotificationBell/NotificationBell');
const { Modal } = require('./components/Modal/Modal');
const { SiteListings } = require('./components/SiteListings/SiteListings');

// On Document Ready
$(document).ready(function() {
    console.debug('Document ready!');

    /* Header */
    const header = new Header();
    header.init();

    /* Navigation */
    const navigation = new Navigation('#navigation');
    navigation.init();

    /* Sidebar */
    const sidebar = new Sidebar('#sidebar');
    sidebar.init();

    /* Notification Bell */
    const notificationBell = new NotificationBell('#notification-bell .notification-bell__container', '#notification-bell .notification-bell__popup');
    notificationBell.init();

    /* Modal */
    const modal = new Modal();
    modal.init();

    /* Search Form Autocomplete */
    const searchFormAutocomplete = new SearchFormAutocomplete('search__form--header', 'search__form__query');
    searchFormAutocomplete.init();

    /* Search Form */
    const searchForm = new SearchForm('#search__form--header', '#search__form__query');
    searchForm.init();

    /* Site Listings */
    const siteListings = new SiteListings('#listings', '#listings-pagination-loader');
    siteListings.init();

});
