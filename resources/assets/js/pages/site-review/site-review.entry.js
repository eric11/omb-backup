// require('../../../css/components/forms/comment-form.scss');

const $ = require('jquery');
require('bootstrap');

const { CommentForm } = require('../../components/CommentForm/CommentForm');
const { Poster } = require('../../components/Poster/Poster');


/* Poster*/
const poster = new Poster('#poster');

// On Document Ready
$(document).ready(function() {
    console.debug('Site review entry page is ready!');

    /* Site Listings */
    // $('.lazy').lazy();

    /* CommentForm */
    const commentForm = new CommentForm();
    commentForm.init();

    /* Poster*/
    const poster = new Poster('#poster');
    poster.init();
});
