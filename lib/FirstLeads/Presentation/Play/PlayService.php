<?php

namespace FirstLeads\Presentation\Play;

use FirstLeads\Infrastructure\Logger\LoggerTrait;
use OhMyBingo\Search\SearchService;
use Psr\Log\LoggerInterface as Logger;
use Spatie\Url\Url;

class PlayService
{
    use LoggerTrait;

    private $searchService;

    public function __construct(SearchService $searchService, Logger $logger)
    {
        $this->searchService = $searchService;
        $this->setLogger($logger);
    }

    public function getRedirectUrl(string $componentSlug, string $siteSlug): ?string
    {
        $siteListingEntries = $this->searchService->search('site', 'site', 'slug', $siteSlug);
        if (empty($siteListingEntries->getEntries())) {
            $this->logger->error(sprintf('Failed to retrieve reference slug: %s', $siteSlug));
            return null;
        }
        $siteEntry = $siteListingEntries->getEntries()[0];
        $url = Url::fromString($siteEntry->getAffiliateLink());
        $affiliateUrl = $url->withScheme('https')->__toString();
        $this->logger->info(sprintf('Redirecting user to affiliate url: %s', $affiliateUrl));
        return $affiliateUrl;
    }

    public function getLink(string $componentSlug,  string $siteSlug)
    {
        $link = '/play/'.$componentSlug.'/'.$siteSlug;
        return $link;
    }
}
