<?php

namespace FirstLeads\Presentation\Pagination;

class Pagination
{
    private $items;
    private $totalPages;
    private $currentPage;
    private $nextPage;

    public function __construct(
        array $items,
        int $totalPages,
        int $currentPage,
        ?int $nextPage
    ) {
        $this->setItems($items);
        $this->setTotalPages($totalPages);
        $this->setCurrentPage($currentPage);
        $this->setNextPage($nextPage);
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    /**
     * @return int
     */
    public function getTotalPages(): int
    {
        return $this->totalPages;
    }

    /**
     * @param int $totalPages
     */
    public function setTotalPages(int $totalPages): void
    {
        $this->totalPages = $totalPages;
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @param int $currentPage
     */
    public function setCurrentPage(int $currentPage): void
    {
        $this->currentPage = $currentPage;
    }

    /**
     * @return int|null
     */
    public function getNextPage(): ?int
    {
        return $this->nextPage;
    }

    /**
     * @param int|null $nextPage
     */
    public function setNextPage(?int $nextPage): void
    {
        $this->nextPage = $nextPage;
    }
}
