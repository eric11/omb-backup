<?php

namespace FirstLeads\Presentation\Pagination;

use Psr\Log\LoggerInterface as Logger;
use FirstLeads\Infrastructure\Logger\LoggerTrait;
use RuntimeException;

class PaginationService
{
    const DEFAULT_PAGE_LIMIT = 10;

    use LoggerTrait;

    public function __construct(Logger $logger)
    {
        $this->setLogger($logger);
    }

    public function getPagination(array $items, int $currentPageNumber, int $pageLimit = self::DEFAULT_PAGE_LIMIT): Pagination
    {
        $itemCount = count($items);
        $totalPages = ceil($itemCount / $pageLimit);
        $pageStart = ($currentPageNumber * $pageLimit) - ($pageLimit);
        if ($pageStart > $itemCount) {
            throw new RuntimeException(sprintf(
                'Page not found - there are %s items and %s show per page, there are %s pages in total, you tried to access page %s',
                $itemCount, $pageLimit, $totalPages, $currentPageNumber)
            );
        }
        $paginatedItems = array_slice($items, $pageStart, $pageLimit);
        $nextPage = $currentPageNumber + 1;
        if ($nextPage > $totalPages) {
            $nextPage = null;
        }
        $pagination = new Pagination($paginatedItems, $totalPages, $currentPageNumber, $nextPage);
        return $pagination;
    }
}
