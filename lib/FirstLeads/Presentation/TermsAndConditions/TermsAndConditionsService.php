<?php

namespace FirstLeads\Presentation\TermsAndConditions;

use Psr\Log\LoggerInterface as Logger;
use FirstLeads\Infrastructure\Logger\LoggerTrait;
use OhMyBingo\Search\SearchService;
use Spatie\Url\Url;

class TermsAndConditionsService
{
    use LoggerTrait;

    private $searchService;

    public function __construct(SearchService $searchService, Logger $logger)
    {
        $this->searchService = $searchService;
        $this->setLogger($logger);
    }

    public function getRedirectUrl(string $componentSlug, string $siteSlug)
    {
        $siteListingEntries = $this->searchService->search('site', 'site', 'slug', $siteSlug);
        if (empty($siteListingEntries->getEntries())) {
            $this->logger->error(sprintf('Failed to retrieve reference slug: %s', $siteSlug));
            return null;
        }
        $siteEntry = $siteListingEntries->getEntries()[0];
        $url = Url::fromString($siteEntry->getTermsAndConditionsUrl());
        $termsAndConditionsUrl = $url->withScheme('https')->__toString();
        $this->logger->info(sprintf('Redirecting user to terms and conditions URL: %s', $termsAndConditionsUrl));
        return $termsAndConditionsUrl;
    }

    public function getLink(string $componentSlug,  string $siteSlug)
    {
        $link = '/terms/'.$componentSlug.'/'.$siteSlug;
        return $link;
    }
}
