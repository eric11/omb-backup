<?php

namespace FirstLeads\Presentation\Navigation;

class NavigationService
{
    public function __construct()
    {

    }

    public function getSelected(string $navigationItemSlug,  string $pageSlug, array $navigationItems = []): bool
    {
        if ($navigationItemSlug === $pageSlug) {
            return true;
        } else {
            foreach ($navigationItems as $navigationMenu) {
                if (!empty($navigationMenu->getChildren()))  {
                    foreach ($navigationMenu->getChildren() as $navigationMenuChild) {
                        if (($navigationItemSlug === $navigationMenu->getSlug()) && ($navigationMenuChild->getSlug() === $pageSlug)) {
                            return true;
                        }
                        if (!empty($navigationMenuChild->getChildren()))  {
                            foreach ($navigationMenuChild->getChildren() as $navigationMenuGrandChild) {
                                if (($navigationItemSlug === $navigationMenu->getSlug()) && ($navigationMenuGrandChild->getSlug() === $pageSlug)) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

//    public function getLinks()
//    {
//        return $this->links;
//    }
}
