<?php

namespace FirstLeads\Infrastructure\Logger;

use Psr\Log\LoggerInterface as BaseLogger;

class Logger
{
    protected $logger;

    public function __construct(BaseLogger $logger)
    {
        $this->logger = $logger;
    }

    protected function emergency($message, array $context = array())
    {
        $this->logger->emergency($message, $context);
    }

    protected function alert($message, array $context = array())
    {
        $this->logger->alert($message, $context);
    }

    protected function critical($message, array $context = array())
    {
        $this->logger->critical($message, $context);
    }

    protected function error($message, array $context = array())
    {
        $this->logger->error($message, $context);
    }

    protected function warning($message, array $context = array())
    {
        $this->logger->warning($message, $context);
    }

    protected function notice($message, array $context = array())
    {
        $this->logger->notice($message, $context);
    }

    protected function info($message, array $context = array())
    {
        $this->logger->info($message, $context);
    }

    protected function debug($message, array $context = array())
    {
        $this->logger->debug($message, $context);
    }

}