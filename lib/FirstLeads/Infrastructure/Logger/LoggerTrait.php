<?php

namespace FirstLeads\Infrastructure\Logger;

use Psr\Log\LoggerInterface as Logger;

trait LoggerTrait
{
    /** @var Logger */
    protected $logger;

    protected function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }
}