<?php

namespace FirstLeads\Infrastructure\Cache;

use Predis\Client;
use Predis\Connection\ConnectionException;
use Psr\Log\LoggerInterface as Logger;
use FirstLeads\Infrastructure\Cache\Exception\CacheNotFoundException;

interface CacheServiceInterface
{
    public function set(CacheKey $key, $value): void;

    public function get(CacheKey $key);

    public function all();

    public function flushAll();
}
