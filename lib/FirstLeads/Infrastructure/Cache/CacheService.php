<?php

namespace FirstLeads\Infrastructure\Cache;

use FirstLeads\Infrastructure\Cache\CacheKey\CacheKey;
use FirstLeads\Infrastructure\Cache\Exception\CacheNotEnabledException;
use Predis\Client;
use Predis\Connection\ConnectionException;
use Psr\Log\LoggerInterface as Logger;
use FirstLeads\Infrastructure\Cache\Exception\CacheNotFoundException;

class CacheService
{
    /** @var Client Client client. */
    private $client;
    /** @var Logger Logger. */
    private $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
        $parameters = [
            'scheme' => getenv('REDIS_SCHEME'),
            'host'   => getenv('REDIS_HOST'),
            'port'   => getenv('REDIS_PORT'),
        ];
        $options = [
            //'exceptions' => true
        ];
        try {
            $this->client = new Client($parameters, $options);
            $this->client->connect();
        } catch (ConnectionException $e) {
            $this->logger->error('Failed to connect to Redis');
        }
    }

    public function set(CacheKey $key, $value): void
    {
        if (getenv('CACHE_ENABLED') != 1) {
            throw new CacheNotEnabledException(sprintf('Cache not enabled: %s', get_class($this)));
        }
        $this->logger->info('Setting value:', ['key' => $key->get(), 'value' => $value]);
        $this->client->set($key->get(), serialize($value));
        $this->logger->info('Set value:', ['key' => $key->get(), 'value' => $value]);
    }

    /*
     * @throws CacheNotFoundException
     */
    public function get(CacheKey $key)
    {
        if (getenv('CACHE_ENABLED') != 1) {
            throw new CacheNotEnabledException(sprintf('Cache not enabled: %s', get_class($this)));
        }
        $value = $this->client->get($key->get());
        if (!$value) {
            $this->logger->info('No value found', [
                'key' => $key->get()
            ]);
            throw new CacheNotFoundException('Value not found for key' . $key->get());
        }
        $this->logger->info('Found value:', ['key' => $key->get(), 'value' => $value]);
        return unserialize($value);
    }

    public function all()
    {
//        $value = $this->client->get();
//        return $value;
    }

    public function flushAll()
    {
        try {
            $this->client->flushAll();
            $this->logger->info('Redis cache cleared.');
        } catch (\Exception $e) {
            $this->logger->info('Failed to clear Redis cache.');
        }
    }
}
