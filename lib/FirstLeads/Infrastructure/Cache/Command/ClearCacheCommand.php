<?php

namespace FirstLeads\Infrastructure\Cache\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use FirstLeads\Infrastructure\Cache\CacheService;

class ClearCacheCommand extends Command
{
    protected static $defaultName = 'cached:clear';
    protected $cacheService;

    public function __construct(CacheService $cacheService)
    {
        parent::__construct();
        $this->cacheService = $cacheService;
    }

    protected function configure()
    {
        $this
            ->setDescription('Clears Redis cache.')
            ->setHelp('Clears redis cache...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '======================',
            'Cache - Clear',
            '======================',
            '',
        ]);
        $output->writeln('Clearing cache...');
        $this->cacheService->flushAll();
        $output->writeln('Cache cleared!');
    }
}