<?php

namespace FirstLeads\Infrastructure\Cache;

trait CacheTrait
{
    protected $cacheService;

    protected function setCacheService(CacheService $cacheService)
    {
        $this->cacheService = $cacheService;
    }
}