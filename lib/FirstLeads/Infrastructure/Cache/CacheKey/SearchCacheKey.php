<?php

namespace FirstLeads\Infrastructure\Cache\CacheKey;;

class SearchCacheKey extends CacheKey
{
    public function __construct(string $index, string $type, $matchFields, $matchValues)
    {
        $cacheKeyMatchFields = (is_array($matchFields) ? json_encode($matchFields, true) : $matchFields);
        $cacheKeyMatchValues = (is_array($matchValues) ? json_encode($matchValues, true) : $matchValues);
        $cacheKeyString      = sprintf('SearchCache.%s.%s.%s.%s', $index, $type, $cacheKeyMatchFields, $cacheKeyMatchValues);
        $cacheKeyString      = str_replace('"', '', $cacheKeyString);
        parent::__construct($cacheKeyString);
    }
}
