<?php

namespace FirstLeads\Infrastructure\Cache\CacheKey;;

class CacheKey
{
    /** @var object Key value. */
    protected $value;

    public function __construct(string $prefix, string $suffix = null)
    {
        $this->value = ($suffix) ? $prefix.'-'.$suffix : $prefix;
    }

    public function get(): string
    {
        return $this->value;
    }
}
