<?php

namespace FirstLeads\Infrastructure\Cache\CacheKey;;

class PageCacheKey extends CacheKey
{
    public function __construct(string $contentType, string $path)
    {
        $cacheKeyString      = sprintf('PageCache.%s.%s', $contentType, $path);
        parent::__construct($cacheKeyString);
    }
}
