<?php

namespace FirstLeads\Infrastructure\Routing\Contentful;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class ContentfulLoader extends Loader
{
    private $isLoaded = false;

    public function load($resource, $type = null)
    {
        if (true === $this->isLoaded) {
            throw new \RuntimeException('Do not add the "contenful" loader twice');
        }
        $routes = new RouteCollection();
        $path = '/about2';
        $defaults = [
            '_controller' => 'OhMyBingo\Controller\AboutController::index',
        ];
        $requirements = [
            'parameter' => '\d+',
        ];
        $route = new Route($path, $defaults, $requirements);
        $routeName = "aboutRoute";
        $routes->add($routeName, $route);
        $this->isLoaded = true;
//         dd($routes);
        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return 'contentful' === $type;
    }
}